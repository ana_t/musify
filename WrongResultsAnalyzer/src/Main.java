import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class Main {

	public static void main(String[] args) throws IOException 
	{
		if(args.length == 0)
		{
			System.out.println("Report file not found. Please define path as 1st argument");
			return;
		}
		
		File resultsFileToAnalyze = new File (args[0]);
		
		if(!resultsFileToAnalyze.exists() )
		{
			System.out.println("Report file not found. Please define path as 1st argument");
			return;
			
		}
		
		readFile(resultsFileToAnalyze.getAbsolutePath());
	
	}
	
	
	public static void readFile(String resultFilePath) throws FileNotFoundException, IOException
	{
		String lineWithSampleNameStart = " >>  A amostra escolhida para reconhecimento é :";
		String lineWithSuccessfulResult = ">Música Reconhecida :";
		String lineWithUnsuccessfulResult = "A identificação não foi conseguida...";

		String currentSample = "";
		int not_found_in_database = 0, wrong_result_but_same_genre = 0, wrong_result_other_genre = 0;

		try(BufferedReader br = new BufferedReader(new FileReader(resultFilePath))) 
		{
		    String line = br.readLine();
		    //while not EOF
		    while (line != null) 
		    {
		    	//is a identification block?
		    	if(line.toLowerCase().contains(lineWithSampleNameStart.toLowerCase()))
		    	{
		    		//obter nome da amostra
		    		// >>  A amostra escolhida aleatoriamente para reconhecimento é :/Users/Ana/Documents/Samples_Per_Genre/Jazz/jazz-00000-pc3-Y.wav**

		    	}

		        line = br.readLine();
		    }
		}
	
		
		
		
	}
	
	
	
	

}
