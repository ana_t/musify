#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QProcess>

//c++ includes
#include <stdio.h>
#include <iostream>
#include <fstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_choose_sample_button_clicked();

    void on_Identify_button_clicked();

    void on_choose_original_button_clicked();

    void updateOutput(void);



private:
    Ui::MainWindow *ui;
    QString sampleFilePath;
    QString originalFilePath;
    QProcess *process;

};

#endif // MAINWINDOW_H
