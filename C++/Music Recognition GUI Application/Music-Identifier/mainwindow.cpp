#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_choose_sample_button_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    QStringList fileNames;
    dialog.setNameFilter(tr("Audio Wave Files (*.wav)"));


    if (dialog.exec())
        fileNames = dialog.selectedFiles();

    sampleFilePath = fileNames.first();
    ui->Sample_Path_Label->setText(sampleFilePath);
}

void MainWindow::updateOutput()
{
    QByteArray bytes = process->readAllStandardOutput();
    ui->output_text_Browser->insertPlainText(bytes);
}

void MainWindow::on_Identify_button_clicked()
{
    //read input from textBoxes
    QString frameSize = ui->frameSize_textField->toPlainText();
    QString hopSize = ui->hopSize_textField->toPlainText();
    QString numberOfPeaksPerFrame = ui->numberOfPeaksPerFrame_textField->toPlainText();
    QString minFreq = ui->minFreq_textField->toPlainText();
    QString maxFreq = ui->maxFreq_textField->toPlainText();


    process = new QProcess(this);
    process->setReadChannel(QProcess::StandardOutput);
    ui->output_text_Browser->insertPlainText(" Running this Program with the following parameters: \n * Original File = " + originalFilePath +" \n * Sample File = " + sampleFilePath + "\n * Frame Size =  " + frameSize + "\n * Hop Size =  "+ hopSize + "\n * #Peaks per frame =  "+ numberOfPeaksPerFrame + "\n * Minimum Frequency =  "+ minFreq + "\n * Maximum Frequency = "+ maxFreq + "\n\n");

    QString commandToRunMusicIdentifier = "/Users/ana/Teses/Ana/Codigo/C++/Essentia/Build/Products/Debug/MusicIdentifier " + originalFilePath +" " + frameSize +" "+ hopSize +" "+ numberOfPeaksPerFrame +" "+ minFreq +" "+ maxFreq +" "+ sampleFilePath;
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(updateOutput()));
    process->start(commandToRunMusicIdentifier);



}

void MainWindow::on_choose_original_button_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    QStringList fileNames;
    dialog.setNameFilter(tr("Audio Wave Files (*.wav)"));


    if (dialog.exec())
        fileNames = dialog.selectedFiles();

    originalFilePath = fileNames.first();
    ui->Original_Path_Label->setText(originalFilePath);
}
