//
//  Parameter.h
//  MachineReportAnalyzer
//
//  Created by Ana on 27/04/15.
//  Copyright (c) 2015 Ana. All rights reserved.
//

#ifndef __MachineReportAnalyzer__Parameter__
#define __MachineReportAnalyzer__Parameter__
#include <iostream>
#include <stdio.h>
#endif /* defined(__MachineReportAnalyzer__Parameter__) */

using namespace std;
class Parameter{
    
    public:
        Parameter(int type, int value);
        void add(double success);
        void calculateAverage();
        int getType();
        int getValue();
        double getAverage();
    
    
    bool operator<(Parameter other) const
    {   
        return success > other.getAverage();
    }
    
    private:
        int counter;
        double success;
        int type;
        int myValue;
    
    
    
};