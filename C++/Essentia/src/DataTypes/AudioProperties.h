//
//  AudioProperties.h
//  Spectrogram
//
//  Created by Ana on 08/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__AudioProperties__
#define __Spectrogram__AudioProperties__

#include <stdio.h>
#include <essentia/pool.h>
#include "Peak.h"


#endif /* defined(__Spectrogram__AudioProperties__) */

using namespace std;
using namespace essentia;


struct fingerprint{

    string fingerprint;
    essentia::Real frequency1;
    essentia::Real frequency2;
    essentia::Real time1;
    essentia::Real time2;

};


class AudioProperties {

    public:
    
        string getFilename();
        string getFilePath();
        string getFileType();
        essentia::Pool getPool();
        AudioProperties(string filePath);
        void setPool(essentia::Pool );
        void setFrameNumber(int size);
        void setHopSize(int hop);
        int getFrameNumber();
        int getHopSize();
        vector<Peak*> getPeaks();
        void deletePeaks();
        ~AudioProperties();
        bool checkForPeakByFingerprint(string  fingerprint);
        essentia::Real _rSampleRate = 44100.0 ;
        int getNumberOfFingerprints();
        vector< string > getFingerprints();
        fingerprint getFingerprintStructFromKey(string fingerprint);


    

    private:
    
    essentia::Pool _poolPool;
    std::string _strFilePath;
    std::string _strFileName;
    std::string _strFileType; //ORIGINAL vs TYPE OF NOISE
    void setFileName(string filepath);
    int _iFrame_number;
    int _iHopSize;
    vector<Peak*> peaks;
    vector< string > fingerprintsForSample;
    map<string, fingerprint> fingerprint_to_fingerprintStruct;

    void savePeaksFromPoolInVector(essentia::Pool pool);
    map< string, AudioProperties*> fingerprints;

    void calculateFingerprintsForSong(vector<vector<essentia::Real>> frequencies_for_all_frames,vector<vector<essentia::Real>> magnitudes_for_all_frames );


};