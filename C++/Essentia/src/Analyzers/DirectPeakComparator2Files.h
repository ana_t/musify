//
//  DirectPeakComparator2Files.h
//  Spectrogram
//
//  Created by Ana on 15/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__DirectPeakComparator2Files__
#define __Spectrogram__DirectPeakComparator2Files__

#include <stdio.h>
#include "essentia.h"
#include <essentia/pool.h>
#include "AudioProperties.h"
#include "StatsMaker.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include "MachineReportWriter.h"

#include <mysql.h>


using namespace essentia;
using namespace std;




class DirectPeakComparator2Files{


    public:
    static bool comparePeaks(ofstream* os_reportFile, AudioProperties* audio1, AudioProperties* audio2, int hopSize,int sampleRate, int frameSize, int number_of_peaks_per_frame, int min_frequency_of_peaks, int max_frequency_of_peaks );
    
    
    

    //static vector<essentia::Real> times_with_more_peaks;
    //static vector<essentia::Real> times_with_less_peaks;


};
#endif /* defined(__Spectrogram__DirectPeakComparator2Files__) */
