//
//  StatsMaker.cpp
//  Spectrogram
//
//  Created by Ana on 15/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "StatsMaker.h"




/*
 * Method that calculates the frequency distribution in an vector of essentia::Real in some interval.
 */
vector<double> StatsMaker::getFrequencyDistribution(vector<essentia::Real> data_array, int hop_size, essentia::Real start_from, essentia::Real end_at, int total_of_samples ){
    
    int size = (end_at - start_from)/hop_size;
    vector<double> vectorD_frequencyDistributionPerClassInPc(size);
    //for(int pos = 0 ; pos < size ; pos ++)
      //  vectorD_frequencyDistributionPerClassInPc.at(pos) = 0;
    
    int counter = 1;
    for( vector<essentia::Real>::iterator dataIterator = data_array.begin(); dataIterator < data_array.end() ; dataIterator++){
        
        
        
        essentia::Real value = *dataIterator;
        int _class = value/hop_size;
        
        //cout <<"Digo que  " << (double) *dataIterator << " está na classe : " << _class << " -> [" << start_from + hop_size* _class << "," << start_from + hop_size* _class +hop_size << "] -- nºpicos = "<< data_array.size() <<  " , valor antigo de hits em _class= " << vectorD_frequencyDistributionPerClassInPc.at(_class)  << " ,iteração = " << counter << endl;

        int counterOfClass = vectorD_frequencyDistributionPerClassInPc.at(_class) +1;
        

        
        
        vectorD_frequencyDistributionPerClassInPc.at(_class)= counterOfClass;
        counter++;

        
    
    }
    
    //puts the distribution % instead of the hit number for each classe.
    
    for (vector<double>::iterator hit_iterator = vectorD_frequencyDistributionPerClassInPc.begin(); hit_iterator != vectorD_frequencyDistributionPerClassInPc.end(); hit_iterator++){
        
        *hit_iterator = (*hit_iterator)/total_of_samples;

    }
    
    
    //<< "   end_at = " << end_at << " start_from ="


    return vectorD_frequencyDistributionPerClassInPc;
    
    
}
