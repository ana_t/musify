//
//  StatsMaker.h
//  Spectrogram
//
//  Created by Ana on 15/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__StatsMaker__
#define __Spectrogram__StatsMaker__

#include <stdio.h>
#include "essentia.h"

#endif /* defined(__Spectrogram__StatsMaker__) */
using namespace std;

class StatsMaker {


    public:
    static vector<double> getFrequencyDistribution(vector<essentia::Real> data_array, int hop_size, essentia::Real start_from, essentia::Real end_at, int total_of_samples  );


};