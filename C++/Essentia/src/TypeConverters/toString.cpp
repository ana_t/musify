//
//  toString.cpp
//  Spectrogram
//
//  Created by Ana on 15/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "toString.h"


string toString::intToString (int i)
{
    ostringstream temp;
    temp<<i;
    return temp.str();
}

string toString::doubleToString (double dbl)
{
    std::ostringstream strs;
    strs << dbl;
    std::string str = strs.str();
    return str;
}
