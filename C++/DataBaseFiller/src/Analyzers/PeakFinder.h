//
//  PeakFinder.h
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __DataBaseFiller__PeakFinder__
#define __DataBaseFiller__PeakFinder__

#include <stdio.h>
#include <essentia/pool.h>
#include "AudioProperties.h"
#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include <essentia/scheduler/network.h>
#include <essentia/streaming/algorithms/poolstorage.h>

using namespace essentia;
using namespace essentia::streaming;
using namespace essentia::scheduler;



class PeakFinder{

    public:
    static Pool find(string frequency_range , string audio_path, int frameSize, int hopSize, essentia::Real sampleRate, int max_Frequency, int min_Frequency, int number_of_peaks_per_frame);

};


#endif /* defined(__DataBaseFiller__PeakFinder__) */
