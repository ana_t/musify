//
//  PeakFinder.cpp
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "PeakFinder.h"



Pool PeakFinder::find(string frequency_range , string audio_path, int frameSize, int hopSize, essentia::Real sampleRate, int max_Frequency, int min_Frequency, int number_of_peaks_per_frame){
    
    //To accomplish the Speactral peaks, one needs to do the following algorithms:
    // audioloader (in case of stereo sound) and Monoloader in case of an mono output  -> framecutter -> Windowing -> Spectrum -> Spectral Peaks
    
    AlgorithmFactory& factory = essentia::streaming::AlgorithmFactory::instance();
    
    Algorithm * algAudio = factory.create("MonoLoader", "filename", audio_path,"sampleRate", sampleRate);
    
    Algorithm * algFrameCutter = factory.create("FrameCutter","frameSize", frameSize,"hopSize", hopSize, "startFromZero", true);
    Algorithm * algWindowing = factory.create("Windowing", "type", "blackmanharris92");
    Algorithm* algSpectrum  = factory.create("Spectrum");
    
    Algorithm *algSpectralPeaks = factory.create("SpectralPeaks", "magnitudeThreshold", -1E300   , "maxFrequency", max_Frequency, "maxPeaks", number_of_peaks_per_frame, "minFrequency", min_Frequency, "orderBy", "magnitude");
    
    // This algorithms will be used together, as a pipeline, to apply the changes in the wanted order. The order and the changes chosen earlier will give as the Spectral Peaks. The following code will be encharge of the scheduling of the functions. First the MonoLoader will pass the information to the frameCutter, and so on...
    
    cout << "- connecting the algorithms" << endl;
    
    // Sending Audio -> FrameCutter
    algAudio -> output("audio") >> algFrameCutter->input("signal");
    // FrameCutter -> Windowing
    algFrameCutter->output("frame")  >>  algWindowing->input("frame");
    
    //Windowing -> Spectrum
    algWindowing->output("frame") >>  algSpectrum->input("frame");
    // Spectrum -> SpectralPeaks
    algSpectrum->output("spectrum")  >>  algSpectralPeaks->input("spectrum");
    
    
    // defining the save of the output from the algSpectralPeaks to the pool under the name of:
    Pool pool;
    algSpectralPeaks->output("frequencies") >> PC(pool, frequency_range);
    algSpectralPeaks->output("magnitudes") >> PC(pool, "SpectralPeaksMagnitudes");
    
    //algSpectrum->output("spectrum") >> PC(spectrum_pool, "Spectrum");
    
    // The network/pipeline creation
    Network pipeline(algAudio);
    
    // start the operations on the pipeline
    pipeline.run();
    
    cout << "- Cleaning resources from this iteration" << endl;
    
    pipeline.clear();
    return pool;
    
}