//
//  main.cpp
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include <stdio.h>
#include "essentia.h"
#include "taglib.h"
#include "fftw3.h"
#include <essentia/algorithmfactory.h>
#include <essentia/essentiamath.h>
#include <essentia/pool.h>
#include <essentia/scheduler/network.h>
#include <essentia/streaming/algorithms/poolstorage.h>
#include <iostream>
#include <pthread.h>
#include <thread>
#include <ctime>



//custom classes/interfaces
#include "AudioProperties.h"
#include "Analyzers/PeakFinder.h"
#include "Services/DatabaseService.h"
#include "Services/FingerprintInserter.h"
#include "Writter.h"
#include "FingerprintMetaData.h"


#define MUSIC_INPUT 2


using namespace std;
using namespace essentia::streaming;
using namespace essentia::scheduler;
using namespace codes;

/////// PARAMS //////////////

essentia::Real sampleRate = 44100.0 ;

DatabaseService database;
unsigned short int curr_song_id = 1;
map<string,map<unsigned short int, FingerprintMetaData*>> fingerprints;
map<unsigned short int,string> songs;


/*
 *  Checks if two strings are equal. In this case, compares if a command matches another
 *
 */
bool isCommand (string const attempt, string const command){

    return attempt.compare(command) == 0;

}


void manual(){
    
    cout << "\t********************************* MAN ***********************************"<< endl ;
    cout << "\tThe accepted commands for this module are the following:\n "<< endl ;
    cout << "\t\thelp (of course): shows a manual for this module, including the accepted commands, their functions, and how to use them.\n"<< endl ;
    cout << "\t\tadd <path to song> : Adds the song of the path given to the database. By using this command one also creates the unique ids of songs, to allow their identification through samples\n WARNING: the song must respect the following format: <Song Name>-<Artist>-<Album>-<Year>.wav or else IT WON'T WORK \n  Also, there can be no spaces in path!"<< endl ;
    cout << "\t\tradd <path to song> : Adds the song of the path given to the database. By using this command one also creates the unique ids of songs, to allow their identification through samples\n"<< endl ;
    cout << "\t\tadd <path to song> : Adds the song of the path given to RAM instead of to the database. By using this command one also creates the unique ids of songs, to allow their identification through samples\n"<< endl ;

    cout << "\t\tdelete <path to song> (in the future) : Deletes the song of the path given to the database. By using this command one also deletes the unique ids of songs, making impossible the identification of the deleted song through samples\n"<< endl ;
    
    cout << "\t\tidentify <path to sample> (in the future) : Tries to identify the music present in the recording given \n"<< endl ;
    cout << "\t\tridentify <path to sample> (in the future) : Tries to identify the music present in the recording given \n"<< endl ;

    cout << "\t\tstatus : shows the database numbers, such as number of songs \n"<< endl ;

    cout << "\t\tdeleteDB : deletes the database contents \n"<< endl ;

    cout << "\t******************************* MAN END *********************************"<< endl ;
    cout << "Please write your command."<< endl ;

}

void getParametersForAudioProperties(string filepath , string &_strSongName, string &_strArtistName, string &_strAlbumName, unsigned short int  &_iAlbumYear){
    
    string original_file_path = filepath;
    size_t aux_position_of_slash =  filepath.find("/");
    
    string aux_file_name = filepath;
    while(aux_position_of_slash != std::string::npos){
        aux_file_name = aux_file_name.substr(aux_position_of_slash+1);
        aux_position_of_slash =  aux_file_name.find("/");
        
    }
    string aux = aux_file_name;
    //defines every other property of audio based on the format of the file name. - song information
    size_t aux_position_of_hifen =  aux.find("-");
    _strSongName = aux.substr(0, aux_position_of_hifen);
    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find("-");
    _strArtistName = aux.substr(0,aux_position_of_hifen);
    
    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find("-");
    _strAlbumName = aux.substr(0, aux_position_of_hifen);
    
    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find(".");
    string yearString = aux.substr(0, aux_position_of_hifen);
    _iAlbumYear = stoi(yearString);
}

void errorHandler(int error){

    if(error == SONG_EXISTS)
        cout << "\tadd > ERROR: The song you requested can not be added because already exists in the database." << endl;
    if (error == CANT_DELETE_DATABASE)
        cout << "\tdeleteDB > ERROR: The database couldn't be cleared." << endl;

}

/*
AudioProperties getAudioPropertiesFromSound(string song_path){


 string _strSongName,_strArtistName, _strAlbumName;
 int _iAlbumYear;
 getParametersForAudioProperties(song_path , _strSongName, _strArtistName, _strAlbumName, _iAlbumYear);



}
*/
int identifySong(string arguments){

    string song_path = arguments;
    
    // To register the algorithms in the factory (ies)
    essentia::init();
    //calculating the peaks for each file - *TODO* this part will be running in multithreading to improve results in case of two files
    cout << " ** Start of Sample analysis " <<  song_path << " **" <<endl;
    cout << "Feature Extraction " << endl;
    time_t start_time = time(NULL);
    //TODO: mudar para number_mmdfkdfkmdfm
    Pool pool = PeakFinder::find("RANDOM", song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  MAX_FREQUENCY,  MIN_FREQUENCY,  PEAK_PER_FRAME_SAMPLE);
    essentia::shutdown();
    const vector<vector<essentia::Real>> frequencies = pool.value<vector<vector<essentia::Real>>>("SpectralPeaksFrequencies");
    cout << "Time of feature extraction (elapsed) : " << time(NULL) - start_time <<" seconds \n" <<endl;
    
    //já tem os picos, falta fazer fingerprints e começar a procurar na BD.
    
    //1) criar fingerprints e compará-los com a base de dados.
    //FingerprintInserter::calculateFingerprintsForSample(frequencies, (int)frequencies.size(), HOP_SIZE, sampleRate, database);

    

    return SUCCESS;
}


int identifySongRAM(string arguments){
    
    string song_path = arguments;
    
    // To register the algorithms in the factory (ies)
    essentia::init();
    //calculating the peaks for each file - *TODO* this part will be running in multithreading to improve results in case of two files
    cout << " ** Start of Sample analysis " <<  song_path << " **" <<endl;
    cout << "Feature Extraction " << endl;
    time_t start_time = time(NULL);
    //TODO: mudar para number_mmdfkdfkmdfm
    Pool pool_bass1 = PeakFinder::find(BASS1,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS1_MAX,  FREQUENCY_THRESHOLD_BASS1_MIN,  PEAK_PER_FRAME_DB);
    
    
    Pool pool_bass2 = PeakFinder::find(BASS2,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS2_MAX,  FREQUENCY_THRESHOLD_BASS2_MIN,  PEAK_PER_FRAME_DB);
    
    Pool pool_bass3 = PeakFinder::find(BASS3,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS3_MAX,  FREQUENCY_THRESHOLD_BASS3_MIN,  PEAK_PER_FRAME_DB);
    
    Pool pool_bass4 = PeakFinder::find(BASS4, song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS4_MAX,  FREQUENCY_THRESHOLD_BASS4_MIN,  PEAK_PER_FRAME_DB);
    
    //Pool pool_midrange1 = PeakFinder::find(MID_RANGE1,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_MIDRANGE1_MAX,  FREQUENCY_THRESHOLD_MIDRANGE1_MIN,  PEAK_PER_FRAME_DB);
    
    //Pool pool_midrange2 = PeakFinder::find(MID_RANGE2,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_MIDRANGE2_MAX,  FREQUENCY_THRESHOLD_MIDRANGE2_MIN,  PEAK_PER_FRAME_DB);
    
    
    
    essentia::shutdown();
    
    const vector<vector<essentia::Real>> bass1_frequencies = pool_bass1.value<vector<vector<essentia::Real>>>(BASS1);
    const vector<vector<essentia::Real>> bass2_frequencies = pool_bass2.value<vector<vector<essentia::Real>>>(BASS2);
    const vector<vector<essentia::Real>> bass3_frequencies = pool_bass3.value<vector<vector<essentia::Real>>>(BASS3);
    const vector<vector<essentia::Real>> bass4_frequencies = pool_bass4.value<vector<vector<essentia::Real>>>(BASS4);
    
    //const vector<vector<essentia::Real>> mid_range1_frequencies = pool_midrange1.value<vector<vector<essentia::Real>>>(MID_RANGE1);
    
    //const vector<vector<essentia::Real>> mid_range2_frequencies = pool_midrange2.value<vector<vector<essentia::Real>>>(MID_RANGE2);
    
    
    vector<const vector<vector<essentia::Real>> * > all_range_frequencies;
    
    all_range_frequencies.push_back(&bass1_frequencies);
    all_range_frequencies.push_back(&bass2_frequencies);
    all_range_frequencies.push_back(&bass3_frequencies);
    all_range_frequencies.push_back(&bass4_frequencies);
    cout << "Time of feature extraction (elapsed) : " << time(NULL) - start_time <<" seconds \n" <<endl;
    
    //já tem os picos, falta fazer fingerprints e começar a procurar na BD.
    
    //1) criar fingerprints e compará-los com a base de dados.
    FingerprintInserter::calculateFingerprintsForSample(all_range_frequencies, (int)bass1_frequencies.size(), HOP_SIZE, sampleRate, database, fingerprints);
    
    //Writter::writeMapToFile(fingerprints.begin(), fingerprints.end(), "fingerprintsSampleRam.dat");
    
    return SUCCESS;
}


int addSongRAM(string arguments){
    //
    
    string song_path = arguments;
    
    string _strSongName,_strArtistName, _strAlbumName;
    unsigned short int  _iAlbumYear;
    getParametersForAudioProperties(song_path , _strSongName, _strArtistName, _strAlbumName, _iAlbumYear);
    
    AudioProperties *audio = new AudioProperties(song_path , _strSongName, _strArtistName, _strAlbumName, _iAlbumYear);
    audio->setHopSize(HOP_SIZE);
    
    // To register the algorithms in the factory (ies)
    essentia::init();
    //calculating the peaks for each file - *TODO* this part will be running in multithreading to improve results in case of two files
    cout << " ** Started the processing of " <<  song_path << "**" <<endl;
    cout << "Feature Extraction and its storage " << endl;
    time_t start_time = time(NULL);
    
    Pool pool_bass1 = PeakFinder::find(BASS1,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS1_MAX,  FREQUENCY_THRESHOLD_BASS1_MIN,  PEAK_PER_FRAME_DB);
    
    const vector<vector<essentia::Real>> magnitudes = pool_bass1.value<vector<vector<essentia::Real>>>("SpectralPeaksMagnitudes");

    cout <<"   MAGNITUDES" << endl;
    cout << magnitudes << endl;
    cout <<"   MAGNITUDES" << endl;


    Pool pool_bass2 = PeakFinder::find(BASS2,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS2_MAX,  FREQUENCY_THRESHOLD_BASS2_MIN,  PEAK_PER_FRAME_DB);

    Pool pool_bass3 = PeakFinder::find(BASS3,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS3_MAX,  FREQUENCY_THRESHOLD_BASS3_MIN,  PEAK_PER_FRAME_DB);
    
    Pool pool_bass4 = PeakFinder::find(BASS4, song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_BASS4_MAX,  FREQUENCY_THRESHOLD_BASS4_MIN,  PEAK_PER_FRAME_DB);
    
   // Pool pool_midrange1 = PeakFinder::find(MID_RANGE1,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_MIDRANGE1_MAX,  FREQUENCY_THRESHOLD_MIDRANGE1_MIN,  PEAK_PER_FRAME_DB);
    
  //  Pool pool_midrange2 = PeakFinder::find(MID_RANGE2,song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  FREQUENCY_THRESHOLD_MIDRANGE2_MAX,  FREQUENCY_THRESHOLD_MIDRANGE2_MIN,  PEAK_PER_FRAME_DB);



    essentia::shutdown();
    
     vector<vector<essentia::Real>> bass1_frequencies = pool_bass1.value<vector<vector<essentia::Real>>>(BASS1);
     vector<vector<essentia::Real>> bass2_frequencies = pool_bass2.value<vector<vector<essentia::Real>>>(BASS2);
     vector<vector<essentia::Real>> bass3_frequencies = pool_bass3.value<vector<vector<essentia::Real>>>(BASS3);
     vector<vector<essentia::Real>> bass4_frequencies = pool_bass4.value<vector<vector<essentia::Real>>>(BASS4);

    //const vector<vector<essentia::Real>> mid_range1_frequencies = pool_midrange1.value<vector<vector<essentia::Real>>>(MID_RANGE1);

      //  const vector<vector<essentia::Real>> mid_range2_frequencies = pool_midrange2.value<vector<vector<essentia::Real>>>(MID_RANGE2);
    
    

  //  all_range_frequencies.push_back(&mid_range1_frequencies);
  //  all_range_frequencies.push_back(&mid_range2_frequencies);

    cout << "Bass 1 test  " << bass1_frequencies << "\n\n"<< endl;
    
    cout << "Bass 2 test  " << bass2_frequencies << "\n\n"<< endl;

    
    cout << "Bass 3 test  " << bass3_frequencies << "\n\n"<< endl;

    
    cout << "Bass 4 test  " << bass4_frequencies << "\n\n"<< endl;

    
    //TODO: verificar se é preciso
    audio->setFrameNumber((unsigned short int )bass1_frequencies.size());

    songs[curr_song_id] = _strSongName;
    FingerprintInserter::calculateFingerprintsForSong(bass1_frequencies ,bass2_frequencies, bass3_frequencies, bass4_frequencies, (unsigned short int )bass1_frequencies.size(), HOP_SIZE, sampleRate, database, fingerprints, curr_song_id, _strSongName);
    curr_song_id++;
    
    
    audio->setFrameNumber((unsigned short int )bass1_frequencies.size());
    cout << "Time of feature extraction (elapsed) : " << time(NULL) - start_time <<" seconds \n" <<endl;
    
    delete audio;
    return SUCCESS;
}


int addSong(string arguments){
    //
    
    string song_path = arguments;

    string _strSongName,_strArtistName, _strAlbumName;
    unsigned short int  _iAlbumYear;
    getParametersForAudioProperties(song_path , _strSongName, _strArtistName, _strAlbumName, _iAlbumYear);
    
    if(database.existsInDatabase( _strSongName, _strArtistName, _strAlbumName, _iAlbumYear) == SONG_EXISTS)
        return SONG_EXISTS;
    
    AudioProperties *audio = new AudioProperties(song_path , _strSongName, _strArtistName, _strAlbumName, _iAlbumYear);
    audio->setHopSize(HOP_SIZE);
    
    // To register the algorithms in the factory (ies)
    essentia::init();
    //calculating the peaks for each file - *TODO* this part will be running in multithreading to improve results in case of two files
    cout << " ** Started the processing of " <<  song_path << "**" <<endl;
    cout << "Feature Extraction and its storage " << endl;
    time_t start_time = time(NULL);

    Pool pool = PeakFinder::find("RANDOM", song_path, FRAME_SIZE, HOP_SIZE, sampleRate,  MAX_FREQUENCY,  MIN_FREQUENCY,  PEAK_PER_FRAME_DB);
    essentia::shutdown();

    
    int songID = database.insertSongInDatabase(audio);
    if (songID == SONG_EXISTS)
        return songID;
    const vector<vector<essentia::Real>> frequencies = pool.value<vector<vector<essentia::Real>>>("SpectralPeaksFrequencies");
    //cout << magnitudes << endl;
    //TODO: verificar se é preciso
    audio->setFrameNumber((int)frequencies.size());
    //cout <<"Real number of fingerprints = " << audio->calculateFingerprintsForSong(frequencies) << endl;
    //exit(1);
    //FingerprintInserter::calculateFingerprintsForSong(frequencies, (int)frequencies.size(), HOP_SIZE, sampleRate, database, songID);

    audio->setFrameNumber((int)frequencies.size());
    cout << "Time of feature extraction (elapsed) : " << time(NULL) - start_time << " seconds \n" <<endl;
    
    delete audio;
    return SUCCESS;
}


/*
 * Tries to do the strFull command. It has to search for a match and then does the corresponding command.
 
 *
 */
void doCommand(string strCommand, string arguments){
    
    if (isCommand(strCommand, "help"))
        manual();
    
    else if (isCommand(strCommand, "add")){
        long start = time(NULL);
        int status = addSong(arguments);
        if(status==SONG_EXISTS){
            cout << "Main>>addSong"<<endl;
            errorHandler(status);
        }
        else{
            cout << "Song added successfully. It took " << (time(NULL) - start)  << " seconds.\n" <<endl;
        }
    }
    else if (isCommand(strCommand, "radd")){
        long start = time(NULL);
        int status = addSongRAM(arguments);
        if(status==SONG_EXISTS){
            cout << "Main>>addSongRAM"<<endl;
            errorHandler(status);
        }
        else{
            cout << "Song added successfully. It took " << (time(NULL) - start)  << " seconds.\n" <<endl;
        }
    }

    else if (isCommand(strCommand, "status")){
        cout << database.getStatus();
    }
    else if (isCommand(strCommand, "deleteDB")){
        long start = time(NULL);
        int status = database.clearDatabase();
        if(status == CANT_DELETE_DATABASE){
            cout << "Main>>addSong"<<endl;
            errorHandler(status);
        }
        else cout << "Database cleared successfully. It took " << (time(NULL) - start) << " seconds.\n" <<endl;
    }
    
    else if (isCommand(strCommand, "quit")){
        cout << " Bye!" << endl;
        exit(0);
    }
    
    else if (isCommand(strCommand, "identify")){
        cout << "-> Starting the identifing process.... " <<endl;
        long start = time(NULL);
        identifySong(arguments);
        cout << "Identification done. It took " << (time(NULL) - start) << " seconds.\n" <<endl;
    }
    else if (isCommand(strCommand, "ridentify")){
        cout << "-> Starting the identifing process.... " <<endl;
        long start = time(NULL);
        identifySongRAM(arguments);
        cout << "Identification done. It took " << (time(NULL) - start) << " seconds.\n" <<endl;
    }

    else cout <<"\nCommand not recognized: " << strCommand <<endl;
}



/**************************************************************************************************************************
 ***************************************************************************************************************************
 ***********************************************         MAIN        *******************************************************
 ***************************************************************************************************************************
 ***************************************************************************************************************************/




int main(int argc, const char * argv[]) {
    
    cout << "Welcome to Database Filler!" << endl;
    cout << "\n >>> WARNING :  For every song add/delete the song must respect the following format: <Song Name>-<Artist>-<Album>-<Year>.wav or else IT WON'T WORK.\n Also, there can be no spaces in path!\n"<<endl;
    
    database.connectToDatabase();
    bool isRunning = true;
    string strFullCommand = "";
    cout << "Please write your command. If you need to get the manual, please type 'help' + enter. " << endl;
    
    while (isRunning) {
        string line;
        string strCommand;
        getline(cin, line);
        size_t aux_position_of_space =  line.find(" ");
        
        //waits for command
        strCommand =line.substr(0,aux_position_of_space);
        
        string arguments = line;
        aux_position_of_space =  arguments.find(" ");
        if(aux_position_of_space != string::npos)
            arguments = arguments.substr(aux_position_of_space+1);
        
        doCommand(strCommand, arguments);
        cout << "Insert new command: " << endl;
    }
    
}