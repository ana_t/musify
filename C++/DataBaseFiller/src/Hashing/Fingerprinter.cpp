//
//  Fingerprinter.cpp
//  Spectrogram
//
//  Created by Ana on 04/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "Fingerprinter.h"


 string Fingerprinter::makeFingerprint(int frame_diff, essentia::Real frequency_peak_1 ,essentia::Real frequency_peak_2 ){
     
     const float  PI_F=3.14159265358979f;
     
  //   assert(frequency_peak_1 != frequency_peak_2);
     
/*
     essentia::Real result1 = frequency_peak_1  * ((time_peak_1 * PI_F - time_peak_2) * PI_F);
     essentia::Real result2 = frequency_peak_2  * ((time_peak_1 * PI_F - time_peak_2) * PI_F*2);
     unsigned int result5 = result1 * result2;
     unsigned int result6 = frequency_peak_1 * PI_F*2;

        // 150 : D -> (essentia::Real)(Fingerprinter::MurmurHash2(&result5, sizeof(result5), 7463 )
     // 65 -> (essentia::Real)(Fingerprinter::MurmurHash2(&result5, sizeof(result5), 7463 ) ^ Fingerprinter::MurmurHash2(&result6, sizeof(result6), 7460 ) ^ Fingerprinter::MurmurHash2(&result7, sizeof(result7), 8765 ))
     //return std::to_string(Fingerprinter::MurmurHash2(&result5, sizeof(result5), 7463)  ^ Fingerprinter::MurmurHash2(&result6, sizeof(result6), 7460 )) + "-" + std::to_string(frequency_peak_1)  + "-"+  std::to_string(frequency_peak_2) + "-" + std::to_string((time_peak_2-time_peak_1)) ;
     
 */
     int iFrequencyPeak1 = frequency_peak_1;
     int iFrequencyPeak2 = frequency_peak_2;

     return std::to_string(iFrequencyPeak1)  + "-"+  std::to_string(iFrequencyPeak2) + "-" + std::to_string(frame_diff) ;



}

float Fingerprinter::makeFingerprint(essentia::Real time_peak_1, essentia::Real frequency_peak_1 ){
    
    essentia::Real result = frequency_peak_1* time_peak_1;
    return Fingerprinter::MurmurHash2(&result, sizeof(result), 7463 );
    
    
}



unsigned int Fingerprinter::MurmurHash2 ( const void * key, int len, unsigned int seed )
{
    // 'm' and 'r' are mixing constants generated offline.
    // They're not really 'magic', they just happen to work well.
    
    const unsigned int m = 0x5bd1e995;
    const int r = 24;
    
    // Initialize the hash to a 'random' value
    
    unsigned int h = seed ^ len;
    
    // Mix 4 bytes at a time into the hash
    
    const unsigned char * data = (const unsigned char *)key;
    
    while(len >= 4)
    {
        unsigned int k = *(unsigned int *)data;
        
        k *= m;
        k ^= k >> r;
        k *= m;
        
        h *= m;
        h ^= k;
        
        data += 4;
        len -= 4;
    }
    
    // Handle the last few bytes of the input array
    
    switch(len)
    {
        case 3: h ^= data[2] << 16;
        case 2: h ^= data[1] << 8;
        case 1: h ^= data[0];
            h *= m;
    };
    
    // Do a few final mixes of the hash to ensure the last few
    // bytes are well-incorporated.
    
    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;
    
    return h;
}