//
//  Fingerprinter.h
//  Spectrogram
//
//  Created by Ana on 04/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__Fingerprinter__
#define __Spectrogram__Fingerprinter__

#include <stdio.h>
#include <string>

//#include "Peak.h"
//#include "MurmurHash2.cpp"
#include "essentia.h"

using namespace essentia;
using namespace std;
#endif /* defined(__Spectrogram__Fingerprinter__) */

class Fingerprinter{

    public :
            static  string makeFingerprint(int frame_diff, essentia::Real frequency_peak_1 ,essentia::Real frequency_peak_2 );
            static float makeFingerprint(essentia::Real time_peak_1, essentia::Real frequency_peak_1 );
    private:
        static unsigned int MurmurHash2 ( const void * key, int len, unsigned int seed );



};