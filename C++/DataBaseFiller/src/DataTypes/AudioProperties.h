//
//  AudioProperties.h
//  Spectrogram
//
//  Created by Ana on 08/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __Spectrogram__AudioProperties__
#define __Spectrogram__AudioProperties__

#include "Music.h"
//#include "../Services/DatabaseService.h"
#include <stdio.h>
#include <essentia/pool.h>
#include "Peak.h"
#include <stdlib.h>
#include "Fingerprinter.h"
#include <assert.h>     /* assert */


using namespace std;
using namespace essentia;


struct fingerprint{

    string fingerprint;
    essentia::Real frequency1;
    essentia::Real frequency2;
    essentia::Real time1;
    essentia::Real time2;

};


class AudioProperties {

    public:
    
        //get Song Information
        string getSongName();
        string getFilePath();
        string getAlbum();
        string getArtist();
        Music* getMusic();
        int getYear();
    
        AudioProperties(Music* music);
        AudioProperties(string const filepath , string const _strSongName, string const _strArtistName, string const _strAlbumName, unsigned int const _iAlbumYear);
        AudioProperties(string filePath);
        ~AudioProperties();
        void setFrameNumber(int size);
        void setHopSize(int hop);
        int getFrameNumber();
        int getHopSize();
        void insertFingerprint(string fingerprint);
        //vector<Peak*> getPeaks();

        set<string> getFingerprints();
        //void deletePeaks();
        //bool checkForPeakByFingerprint(string  fingerprint);
        essentia::Real _rSampleRate = 44100.0 ;
        size_t getNumberOfFingerprints();
        //fingerprint getFingerprintStructFromKey(string fingerprint);
        void savePeaksFromPoolInVector(essentia::Pool pool);
        set<string> calculateFingerprintsForSong(vector<vector<essentia::Real>> frequencies_for_all_frames);


    private:
        
    //std::string _strFilePath;
    Music* _music;
    
    //Meta-Data on Song
    int _iFrame_number;
    int _iHopSize;
    
    //vector<Peak*> peaks;
    set<string> _setFingerprints;
    //map<string, fingerprint> fingerprint_to_fingerprintStruct;
    //map< string, AudioProperties*> fingerprints;

    void setFileName(string filepath, Music* &music);


};
#endif /* defined(__Spectrogram__AudioProperties__) */
