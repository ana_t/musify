//
//  Peak.cpp
//  Spectrogram
//
//  Created by Ana on 04/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "Peak.h"
#include "Fingerprinter.h"

Peak::Peak(essentia::Real freq, essentia::Real mag, essentia::Real t, int f){
    
    frequency = new essentia::Real(freq);
    magnitude = new essentia::Real(mag);
    time = new essentia::Real(t);
    frame = new int(f);
    
}

Peak::Peak(const Peak &peak){

    frequency = new essentia::Real(* peak.frequency);
    magnitude = new essentia::Real(* peak.magnitude);
    time = new essentia::Real(* peak.time);
    frame = new int(* peak.frame);
}

Peak::~Peak(){

    delete frequency;
    delete magnitude;
    delete time;
    delete frame;
}


essentia::Real* Peak::getFrequency(){
    return frequency;
}
essentia::Real* Peak::getMagnitude(){
    return magnitude;

}

essentia::Real* Peak::getTime(){
    //std::cout << "getTime Address = " << time << std::endl;
    return time;
}
int* Peak::getFrame(){
    return frame;

}



bool Peak::equals(Peak* p){

    return (*frequency == *(p->frequency));

}

std::string Peak::toString(){

    return "* Peak info:  Frequency = " + std::to_string(*frequency)  + " Magnitude = " + std::to_string(*magnitude) + " Frame = " + std::to_string(*frame) + " Time = " + std::to_string(*time) +" *";

}
