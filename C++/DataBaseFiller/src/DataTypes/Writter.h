//
//  Writter.h
//  DataBaseFiller
//
//  Created by Ana on 19/06/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __DataBaseFiller__Writter__
#define __DataBaseFiller__Writter__

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <set>
#include <map>
#include <fstream>

using namespace std;
class Writter{
public :

    static void writeSetToFile(set<string>::iterator pos, set<string>::iterator end, string filename );
    static void writeMapToFile(map<string, int>::iterator pos, map<string,int>::iterator end, string filename );

};
#endif /* defined(__DataBaseFiller__Writter__) */
