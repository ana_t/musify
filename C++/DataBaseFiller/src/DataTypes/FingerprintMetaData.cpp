//
//  FingerprintMetaData.cpp
//  DataBaseFiller
//
//  Created by Ana on 27/07/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "FingerprintMetaData.h"

FingerprintMetaData::FingerprintMetaData(unsigned short int songID){

    _songID = songID;

}

void FingerprintMetaData::addTime(unsigned short int time)
{

    _times.insert(time);
    
}

unsigned short int FingerprintMetaData::getSongID()
{

    return _songID;

}

set<unsigned short int> FingerprintMetaData::getTimes()
{

    return _times;
}

set<unsigned short int>::iterator FingerprintMetaData::endTime()
{
    return _times.end();

}

FingerprintMetaData::~FingerprintMetaData()
{

}



