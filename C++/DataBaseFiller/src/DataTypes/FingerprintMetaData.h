//
//  FingerprintMetaData.h
//  DataBaseFiller
//
//  Created by Ana on 27/07/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __DataBaseFiller__FingerprintMetaData__
#define __DataBaseFiller__FingerprintMetaData__

#include <stdio.h>
#include <set>

using namespace std;

class FingerprintMetaData
{
    
public:
    set<unsigned short int> getTimes();
    unsigned short int getSongID();
    void addTime(unsigned short int time);
    FingerprintMetaData(unsigned short int songID);
    set<unsigned short int>::iterator endTime();

    ~FingerprintMetaData();
    
    
    
private:
    unsigned short int _songID;
    set<unsigned short int> _times;
    
};



#endif /* defined(__DataBaseFiller__FingerprintMetaData__) */
