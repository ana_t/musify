//
//  AudioProperties.cpp
//  Spectrogram
//
//  Created by Ana on 08/04/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "AudioProperties.h"
#include "Writter.h"

using namespace std;

AudioProperties::AudioProperties(string filePath){
    //_music();
    setFileName(filePath, _music);
    assert("Music is null..." && _music!=NULL);
    assert ("Music is created with null properties! \n" && _music->getArtist() != "");
    
    _iFrame_number = 0;

}

AudioProperties::AudioProperties(Music *music){
    _music = music;
}




AudioProperties::AudioProperties(string const filepath , string const _strSongName, string const _strArtistName, string const _strAlbumName, unsigned int const _iAlbumYear){
    
    _iFrame_number = 0;
    
    _music = new Music(_strSongName+_strArtistName+_strAlbumName, _strSongName, _strAlbumName, _strArtistName , _iAlbumYear, filepath);
    assert("AudioProperties >> setFileName : Music is null..." && _music!=NULL);
    
}





void AudioProperties::insertFingerprint(string fingerprint){
    
    //cout << "Fingerprint = " << fingerprint <<endl;
    _setFingerprints.find(fingerprint);

    assert( " Duplicate Fingerprint in this song! \n " && _setFingerprints.find(fingerprint) == _setFingerprints.end());
    _setFingerprints.insert(fingerprint);
}

set<string> AudioProperties::getFingerprints(){
    return _setFingerprints;
}
size_t AudioProperties::getNumberOfFingerprints(){
    return _setFingerprints.size();


}

string AudioProperties::getSongName(){
    return _music->getSongName();

}
string AudioProperties::getAlbum(){
    return _music->getAlbum();
}
string AudioProperties::getArtist(){
    return _music->getArtist();

}
int AudioProperties::getYear(){
    return _music->getYear();
}

AudioProperties::~AudioProperties(){
    
    //peaks.clear();
    //fingerprints.clear();
    _setFingerprints.clear();
    delete _music;
}


void AudioProperties::setFileName(string filepath , Music* &music){
    
    string original_file_path = filepath;
    size_t aux_position_of_slash =  filepath.find("/");

    string aux_file_name = filepath;
    while(aux_position_of_slash != std::string::npos){
        aux_file_name = aux_file_name.substr(aux_position_of_slash+1);
        aux_position_of_slash =  aux_file_name.find("/");
        
    }
    string aux = aux_file_name;
    //defines every other property of audio based on the format of the file name. - song information
    size_t aux_position_of_hifen =  aux.find("-");
    string _strSongName = aux.substr(0, aux_position_of_hifen);
    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find("-");
    string _strArtistName = aux.substr(0,aux_position_of_hifen);

    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find("-");
    string _strAlbumName = aux.substr(0, aux_position_of_hifen);

    aux = aux.substr( aux_position_of_hifen+1);
    aux_position_of_hifen =  aux.find(".");
    string yearString = aux.substr(0, aux_position_of_hifen);
    int _iAlbumYear = stoi(yearString);

    //Music(string id, string name, string album, string artist, int year, string path);

    
    //cout << "SongName  = " << _strSongName << " Artist  = " << _strArtistName << " Album  = " << _strAlbumName << " Year  = " << _iAlbumYear << endl;
    
    music = new Music(_strSongName+_strArtistName+_strAlbumName, _strSongName, _strAlbumName, _strArtistName , _iAlbumYear, original_file_path);
    assert("AudioProperties >> setFileName : Music is null..." && music!=NULL);
    assert("It didn't change the values of music! " && _strSongName == music->getSongName());
    
    
    
}

void AudioProperties::setFrameNumber(int size){

    _iFrame_number = size;
}


string AudioProperties::getFilePath(){

    return _music->getFilePath();
}


int AudioProperties::getFrameNumber(){

    return _iFrame_number;
}
int AudioProperties::getHopSize(){
    
    return _iHopSize;
}


set<string> AudioProperties::calculateFingerprintsForSong(vector<vector<essentia::Real>> frequencies_for_all_frames ){

    int frame = 0;
    int counter = 0;
    
    //for every frame
    for(vector<vector<essentia::Real>>::iterator frequencies_iterator = frequencies_for_all_frames.begin() ; frequencies_iterator != frequencies_for_all_frames.end(); frequencies_iterator++){
        
        essentia::Real current_frame_time = frame * (_iHopSize/_rSampleRate);
        if(frame == _iFrame_number-1 )
            break;
        
        //for every frequency data on frame
        for (vector<essentia::Real>::iterator frequencies_of_current_frame = (*frequencies_iterator).begin(); frequencies_of_current_frame != (*frequencies_iterator).end(); frequencies_of_current_frame ++ ){
            essentia::Real current_frequency = *(frequencies_of_current_frame);

            
            //for each following 2 frames, make fingerprints with the combination of their peaks
            for (int nextFrame = 1 ; frequencies_iterator !=frequencies_for_all_frames.end() && nextFrame < 5; nextFrame ++ ){
                if(frame + nextFrame >= _iFrame_number)
                    continue;
                //for each peak in frame
                for (vector<essentia::Real>::iterator frequencies_of_next_frame = (*(frequencies_iterator + nextFrame)).begin(); frequencies_of_next_frame != (*(frequencies_iterator + nextFrame)).end(); frequencies_of_next_frame ++ ){
                    
                essentia::Real next_frequency = *(frequencies_of_next_frame);
                essentia::Real next_frame_time = (frame + nextFrame) * (_iHopSize/_rSampleRate);
                string fingerprint = Fingerprinter::makeFingerprint(nextFrame, current_frequency ,next_frequency);
                    
                    if(current_frequency == next_frequency)
                        continue;
                    counter ++;
                    //TODO: Meter caso se queira confirmar valores. Retirei para debug de número de fingerprints ser mais rápido
                    //insertFingerprint(fingerprint);
                    _setFingerprints.insert(fingerprint);
                    
                }
                
            }
            
            
        }
        ++frame;
        
    }
    //Writter::writeSetToFile(_setFingerprints.begin(), _setFingerprints.end(), "correcto.txt");
    return _setFingerprints;
    
}
/*
fingerprint AudioProperties::getFingerprintStructFromKey(string  fingerprint){

    return fingerprint_to_fingerprintStruct[fingerprint];

}
*/

void AudioProperties::savePeaksFromPoolInVector(essentia::Pool pool ){
    
    //gets data from pool for every frame - each vector in the vector is data from a single frame
    vector<vector<essentia::Real>> frequencies_for_all_frames = pool.value<vector<vector<essentia::Real>>>("SpectralPeaksFrequencies");
    //vector<vector<essentia::Real>> magnitudes_for_all_frames = pool.value<vector<vector<essentia::Real>>>("SpectralPeaksMagnitudes");
    calculateFingerprintsForSong(frequencies_for_all_frames);
    
   }
/*
vector<Peak*> AudioProperties::getPeaks(){
    //cout << "**************************************************\n I have " << peaks.size() << " peaks *****************************\n **************************************************" << endl;
    return peaks;

}
*/

void AudioProperties::setHopSize(int hop){
    _iHopSize = hop;
}

/*
bool AudioProperties::checkForPeakByFingerprint(string fingerprint){
    
    map<string , AudioProperties*>::iterator fingerprintsIterator = fingerprints.find(fingerprint);
        return fingerprintsIterator != fingerprints.end();

}
*/

Music* AudioProperties::getMusic(){
    return _music;

}




/*void AudioProperties::deletePeaks(){
    
    for (vector<Peak*>::iterator peakIterator = peaks.begin() ; peakIterator < peaks.end() ; peakIterator ++ ){
    
        delete *peakIterator;
    }
    peaks.clear();
}
*/
