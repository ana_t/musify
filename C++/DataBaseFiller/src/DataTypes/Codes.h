//
//  Codes.h
//  DataBaseFiller
//
//  Created by Ana on 04/06/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef DataBaseFiller_Codes_h
#define DataBaseFiller_Codes_h


namespace codes{

    unsigned short int const SUCCESS = 1;
    unsigned short int const DOESNT_EXIST = 65000;
    unsigned short int const INSUCCESS = 56;
    unsigned short int const SONG_EXISTS = 57;
    unsigned short int const CANT_DELETE_DATABASE= 58;
    
    //Matching process codes
    unsigned short int const CLOSE_HIT = 2;
    unsigned short int const SCORE_THRESHOLD=200;
    unsigned short int const FREQUENCY_MIN_INTERVAL=0;
    unsigned short int const HITS_MIN_THRESHOLD=40;

    
    unsigned short int const FREQUENCY_THRESHOLD_BASS1_MIN = 40;
    unsigned short int const FREQUENCY_THRESHOLD_BASS1_MAX = 80;
    unsigned short int const FREQUENCY_THRESHOLD_BASS2_MIN = 80;
    unsigned short int const FREQUENCY_THRESHOLD_BASS2_MAX = 120;
    unsigned short int const FREQUENCY_THRESHOLD_BASS3_MIN = 120;
    unsigned short int const FREQUENCY_THRESHOLD_BASS3_MAX = 180;
    unsigned short int const FREQUENCY_THRESHOLD_BASS4_MIN = 180;
    unsigned short int const FREQUENCY_THRESHOLD_BASS4_MAX = 300;
    
    unsigned short int const FREQUENCY_THRESHOLD_MIDRANGE1_MIN = 180;
    unsigned short int const FREQUENCY_THRESHOLD_MIDRANGE1_MAX = 300;
    unsigned short int const FREQUENCY_THRESHOLD_MIDRANGE2_MIN = 401;
    unsigned short int const FREQUENCY_THRESHOLD_MIDRANGE2_MAX = 600;

    static const string BASS1 = "BASS1";
    static const string BASS2 = "BASS2";
    static const string BASS3 = "BASS3";
    static const string BASS4 = "BASS4";
    static const string MID_RANGE1 = "MIDRANGE1";
    static const string MID_RANGE2 = "MIDRANGE2";

    
    
    unsigned short int const FRAME_THRESHOLD_LEVEL1 = 5;
    
    unsigned short int const SCORE_THRESHOLD_LEVEL1 = 3;
    unsigned short int const SCORE_THRESHOLD_LEVEL2 = 5;
    unsigned short int const SCORE_THRESHOLD_LEVEL3 = 7;

    
    unsigned short int const FRAME_THRESHOLD_CONSIDER_MUSIC_NOT_FOUND = 3500;


    unsigned short int const SAME_FRAME = 0;

    //Points
    unsigned short int const POINTS_10 = 10;
    unsigned short int const POINTS_9 = 9;
    unsigned short int const POINTS_5 = 5;
    unsigned short int const POINTS_4 = 4;
    unsigned short int const POINTS_3 = 3;
    unsigned short int const POINTS_8 = 8;
    unsigned short int const POINTS_1 = 1;
    unsigned short int const POINTS_2 = 2;

    //int const SCORE_MIN_THRESHOLD_REMOVE_SONG = -20;

    unsigned short int const REPEATED_HIT_ON_FRAME_POINTS=15;

    //Intervals
    
    signed short int const LOWER_INTERVAL = -1;
    signed short int const UPPER_INTERVAL = 1;

    
    //feature extraction code
    unsigned short int const AHEAD_FRAME_LIMIT =1;
    unsigned short int const PEAK_PER_FRAME_DB = 5;
    unsigned short int const PEAK_PER_FRAME_SAMPLE = 1;

    unsigned short int const FRAME_SIZE = 1024 *4 ;//5120; //4096; //1024; // 512
    unsigned short int const HOP_SIZE = FRAME_SIZE;///2;
    unsigned short int const MIN_FREQUENCY = 200;
    unsigned short int const MAX_FREQUENCY = 6000; //10000
}



#endif
