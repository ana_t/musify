//
//  Music.h
//  DataBaseFiller
//
//  Created by Ana on 25/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __DataBaseFiller__Music__
#define __DataBaseFiller__Music__

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>     /* assert */
#include <string>


using namespace std;

class Music{

    public:
    
        ~Music();
        //Music( Music& other );
        Music(string id, string name, string album, string artist, int year, string path);
        string getSongName();
        string getFilePath();
        string getAlbum();
        string getArtist();
        //void setID(string id);
        string getID();
        int getYear();

    private:

        std::string _strFilePath;
        std::string _strAlbumName;
        std::string _strSongName;
        std::string _strArtistName;
        int _iAlbumYear;
        string _iID;
    
    
};


#endif /* defined(__DataBaseFiller__Music__) */

