//
//  Writter.cpp
//  DataBaseFiller
//
//  Created by Ana on 19/06/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "Writter.h"



void Writter::writeSetToFile(set<string>::iterator pos, set<string>::iterator end, string filename ){

    ofstream file;
    file.open (filename);
    int counter = 0;
    for (pos; pos != end; pos ++){
        file << counter <<" "<< *pos << endl;
        counter ++;
    }
    file.close();


}


void Writter::writeMapToFile(map<string, int>::iterator pos, map<string,int>::iterator end, string filename ){
    
    ofstream file;
    file.open (filename);
    for (pos; pos != end; pos ++){
        file << pos->first << endl;//" "<<  pos->second<< endl;
    }
    file.close();
    
    
}
