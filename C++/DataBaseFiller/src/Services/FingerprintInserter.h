//
//  FingerprintInserter.h
//  DataBaseFiller
//
//  Created by Ana on 15/06/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __DataBaseFiller__FingerprintInserter__
#define __DataBaseFiller__FingerprintInserter__

#define PRINT_REMAINING_FINGERPRINTS 50000

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "DatabaseService.h"
#include <sqlite3.h>
#include "FingerprintMetaData.h"

using namespace std;
using namespace codes;

class FingerprintInserter{
    
public:
    static unsigned short int calculateFingerprintsForSong(vector<vector<essentia::Real>> frequencies_for_all_frames, unsigned short int  iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database, unsigned short int  songID );
    static unsigned short int calculateFingerprintsForSong(  vector<vector<essentia::Real>> bass1_frequencies_for_all_frames,  vector<vector<essentia::Real>> bass2_frequencies_for_all_frames ,  vector<vector<essentia::Real>> bass3_frequencies_for_all_frames,  vector<vector<essentia::Real>> bass4_frequencies_for_all_frames, unsigned short int  iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database, map<string,map<unsigned short int, FingerprintMetaData*>> &fingerprints, unsigned  short int  songID, string songName);

    static unsigned short int calculateFingerprintsForSample(vector<vector<essentia::Real>> frequencies_for_all_frames, unsigned short int  iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database);
    static unsigned short int calculateFingerprintsForSample(vector<const vector<vector<essentia::Real>> * > frequencies_for_all_frames, unsigned short int  iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database, map<string,map<unsigned short int, FingerprintMetaData*>> fingerprints);
        
private:
    static unsigned short int insertFingerprintIntoDB(string fingerprint, int songID, DatabaseService database);
    static unsigned short int searchForFingerprintInDB(string fingerprint, DatabaseService database);
    static void printDataToScreen( int counter,unsigned short int _iHopSize, float _rSampleRate , unsigned short int iFrame_number , int hits,  int total_in_cloud_hits);
    static unsigned short int pointsForProximity(unsigned short int lastHitFrame, unsigned short int currentFrame, unsigned short int currentScore);
    static void addToBlackList( unsigned short int songID);
    static void updateHitsOfSong(unsigned short int songID);
    static void addCurrentFrameFingerprints(vector<essentia::Real> current_frame_frequencies, unsigned short int songID, map<string,map<unsigned short int, FingerprintMetaData*>> &fingerprints, unsigned short int frame);
    static void updateCounters(map<unsigned short int,unsigned short int> &originalFrameToHits,map<unsigned short int,unsigned short int> &originalFrameToSong,
                               map<unsigned short int,pair<unsigned short int,unsigned short int>> &sampleFrameToOriginalFrame,
                               unsigned short int frame, FingerprintMetaData* frames , unsigned short int songID );
    static unsigned short int intersectionFinder(map<unsigned short int,unsigned short int> &originalFrameToHits);
    static unsigned short int getPartialFingerprint(unsigned short int frequency);
    
    static long makeFingerprint(long p1, long p2,long p3, long p4);


    
    static unsigned short int biggestScore;
    static bool result_reached;
    static unsigned short int songIDwithBiggestScore;
    static map<unsigned short int,unsigned short int> mapSongScore;
    static map<unsigned short int,unsigned short int> mapSongLastHitFrame;
    static map<unsigned short int,string> mapSongIDtoName;
    static set<unsigned short int> black_list;
    static map<unsigned short int,unsigned short int> mapSongIDHits;
    static bool interval_fingerprint_generation;

};


#endif /* defined(__DataBaseFiller__FingerprintInserter__) */