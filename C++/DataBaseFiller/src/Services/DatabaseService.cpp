//
//  DatabaseService.cpp
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//
#include "DatabaseService.h"

int current_song_id;
char* database_file = "/Users/ana/Teses/Ana/Music16.db";

/*
static int callbackToSearchForMusic(void *data, int argc, char **argv, char **azColName){
    
    
    
    fprintf(stderr, "%s: ", (const char*)data);
    
    int i;
    for(i=0; i<argc; i++){
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    cout <<  "callback>> curent_song_id = " << current_song_id << endl;
    return SUCCESS;
    
}
 */
bool DatabaseService::connectToDatabase(){
    
    bool connection_success = sqlite3_open(database_file, &db);
    
    if( connection_success ){
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        exit(0);
    }
    else{
        //sqlite3_exec(db, "PRAGMA synchronous = OFF", NULL, NULL, &sErrMsg);
        //sqlite3_exec(db, "PRAGMA journal_mode = MEMORY", NULL, NULL, &sErrMsg);
        //PRAGMA journal_mode = WAL
        //PRAGMA synchronous = NORMAL

        
        // char * sErrMsg;
        //sqlite3_exec(db, "PRAGMA synchronous = OFF", NULL, NULL, &sErrMsg);
        //sqlite3_exec(db, "PRAGMA journal_mode = MEMORY", NULL, NULL, &sErrMsg);


        cout << "Opened database successfully!" <<  endl;
    }
    
    
    //search statements
    sqlite3_prepare_v2(db, "SELECT artistID FROM Artists where name=?1;", -1, &checkArtistExistance, NULL);
    sqlite3_prepare_v2(db, "SELECT albumID FROM Albums where artistID=?1 AND year=?2 AND name=?3;", -1, &checkAlbumExistance, NULL);
    sqlite3_prepare_v2(db,"SELECT songID FROM music_artists natural inner join Music natural inner join music_album where artistID=?1 AND albumID=?2 AND name=?3 AND year=?4;",-1, &checkSongExistance,NULL);
    sqlite3_prepare_v2(db,"SELECT songID FROM Fingerprints where fingerprint=?1;",-1, &checkFingerprintExistance,NULL);
    sqlite3_prepare_v2(db,"SELECT Number FROM Counter where Type='Fingerprints';",-1, &countFingerprints,NULL);
    sqlite3_prepare_v2(db,"SELECT Number FROM Counter where Type='Songs';",-1, &countSongs,NULL);
    
    //insert statements
    sqlite3_prepare_v2(db, "INSERT into Albums (artistID,year,name)values (?1,?2,?3);", -1, &insertAlbumDB, NULL);
    sqlite3_prepare_v2(db, "INSERT into Artists (name) values (?1);", -1, &insertArtistDB, NULL);
    sqlite3_prepare_v2(db, "INSERT into Fingerprints (fingerprint,songID) values (?1,?2);", -1, &insertFingerprintDB, NULL);
    sqlite3_prepare_v2(db, "INSERT into Music (name,year) values (?1,?2);", -1, &insertSongDB, NULL);
    sqlite3_prepare_v2(db, "INSERT into music_album (songID,albumID) values (?1,?2);", -1, &insertMusic_AlbumDB, NULL);
    sqlite3_prepare_v2(db, "INSERT into music_artists (songID, artistID) values (?1,?2);", -1, &insertMusic_ArtistDB, NULL);
    
    //delete statements
    sqlite3_prepare_v2(db, "DELETE FROM Artists;", -1, &deleteArtists, NULL);
    sqlite3_prepare_v2(db, "DELETE FROM Albums;", -1, &deleteAlbums, NULL);
    sqlite3_prepare_v2(db, "DELETE FROM Music;", -1, &deleteSongs, NULL);
    sqlite3_prepare_v2(db, "DELETE FROM Fingerprints;", -1, &deleteFingerprints, NULL);
    sqlite3_prepare_v2(db, "DELETE FROM music_artists; ", -1, &deleteMusicArtists, NULL);
    sqlite3_prepare_v2(db, "DELETE FROM music_album;", -1, &deleteMusicAlbums, NULL);
    
    //return statements
    
    sqlite3_prepare_v2(db, "SELECT * FROM SQLITE_SEQUENCE where name=?1;", -1, &getLastSongID, NULL);
    
    return SUCCESS;
    
}





int DatabaseService::insertSongInDatabase(  AudioProperties *pAudioProperties){
    
    //size_t previous_number_of_songs_database = _mapMusicByID.size();
    
    startTransaction();
    int year = pAudioProperties->getYear();
    string song_name = pAudioProperties->getSongName();
    string artist_name = pAudioProperties->getArtist();
    string album_name = pAudioProperties->getAlbum();
    char * sErrMsg;
    sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &sErrMsg);

    int artistID = artistExists(artist_name);
    
    if( artistID == DOESNT_EXIST )
        artistID = insertArtist(artist_name);

    int albumID = albumExists(album_name, year, artistID);
    if( albumID == DOESNT_EXIST )
        albumID = insertAlbum( album_name,  year,  artistID);
    
    int songID = songExists( artistID,  albumID, year,song_name);
    
    if( songID == DOESNT_EXIST)
        songID = insertSongInDatabase(song_name, year,artistID, albumID);
    else return SONG_EXISTS;
    
    insertMusic_Artists(songID,  artistID);
    insertMusic_album( songID,  albumID);

    //cout << " artistID = " << artistID << " album "<< albumID << "song "<< songID <<endl ;

    //assert("DatabaseService::insertSongInDatabase >> SongID after insert is still -1." && songExists( artistID,  albumID, year,song_name) != DOESNT_EXIST);

    sqlite3_exec(db, "UPDATE Counter SET Number=Number +1 where Type='Songs' ", NULL, NULL, &sErrMsg);
    //sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);

    
    //cout << endl;
    //cout << "DatabaseService >> insertSongInDatabase : New song added to database! "<< endl;

    return songID;

}

static int callback(void *data, int argc, char **argv, char **azColName){
    
    current_song_id = atoi(argv[0]);
    return 0;
    /*
    fprintf(stderr, "%s: ", (const char*)data);

    int i;
    for(i=0; i<argc; i++){
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    cout <<  "callback>> curent_song_id = " << current_song_id << endl;
    */

}


int DatabaseService::insertSongInDatabase(string song_name, int year, int artistID, int albumID){


    //sqlite3_prepare_v2(db, "INSERT into Music (name,year) values (?1,?2);", -1, &insertSongDB, NULL);

    sqlite3_bind_text(insertSongDB, 1, song_name.c_str(), (int)song_name.size() , SQLITE_STATIC);
    sqlite3_bind_int( insertSongDB, 2, year); // prepared statement, parameter pos, value

    int query_status = sqlite3_step(insertSongDB);
    
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::insertSongInDatabase>> Problem inserting song "<< song_name << ", artistID=" << artistID<< endl;
    }
    
    char * getLastSongID = "SELECT seq FROM SQLITE_SEQUENCE where name='Music'";
    char * error = 0;
    const char* message = "Finding song id!";
    current_song_id = DOESNT_EXIST;
    
    /* Execute SQL statement */
    int rc = sqlite3_exec(db, getLastSongID, callback, (void*)message, &error);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "DatabaseService::insertSongInDatabase >> SQL error in getting the correct ID of the song: %s\n", error);
        sqlite3_free(error);
    }

    return current_song_id;
}

int DatabaseService::insertAlbum(string album_name, int year, int artistID){

    //sqlite3_prepare_v2(db, "INSERT into Albums (artistID,year,name)values (?1,?2,?3);", -1, &insertAlbumDB, NULL);

    sqlite3_bind_int( insertAlbumDB, 1, artistID); // prepared statement, parameter pos, value
    sqlite3_bind_int( insertAlbumDB, 2, year); // prepared statement, parameter pos, value
    sqlite3_bind_text(insertAlbumDB, 3, album_name.c_str(), (int)album_name.size() , SQLITE_STATIC);

    int query_status = sqlite3_step(insertAlbumDB);
    
    if(query_status !=  SQLITE_DONE){
        //artistID = sqlite3_column_int(insertSongDB,0);
        cout<< "DatabaseService::insertAlbum>> Problem inserting album: "<< album_name << endl;
    }
    sqlite3_reset(insertAlbumDB);

    return albumExists( album_name,  year,  artistID);
}

int DatabaseService::insertFingerprint(string fingerprint, int songID){

    // sqlite3_prepare_v2(db, "INSERT into Fingerprints (fingerprint,songID) values (?1,?2);", -1, &insertFingerprintDB, NULL);
    sqlite3_bind_text(insertFingerprintDB, 1, fingerprint.c_str(), (int)fingerprint.size() , SQLITE_STATIC);
    sqlite3_bind_int( insertFingerprintDB, 2, songID); // prepared statement, parameter pos, value
    int query_status = sqlite3_step(insertFingerprintDB);
    sqlite3_reset(insertFingerprintDB);
    sqlite3_clear_bindings(insertFingerprintDB);

    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::insertFingerprint>> "<< sqlite3_errmsg(db) <<" -> Problem inserting fingerprint: "<< fingerprint << endl;
        return INSUCCESS;
    }

    return SUCCESS;

}



int DatabaseService::artistExists(string artist_name)
{
    sqlite3_bind_text(checkArtistExistance, 1, artist_name.c_str(), (int)artist_name.size() , SQLITE_STATIC);
    int query_status = sqlite3_step(checkArtistExistance);
    int artistID = DOESNT_EXIST;

     if(query_status ==  SQLITE_ROW){
        artistID = sqlite3_column_int(checkArtistExistance,0);
         //cout<< "DatabaseService::artistExists>> Found artist! ID = "<< artistID << endl;
     }
    sqlite3_reset(checkArtistExistance);
    return artistID;

}

int DatabaseService::albumExists(string album_name, int year, int artistID){
    //    sqlite3_prepare_v2(db, "SELECT albumID FROM Albums where artistID=?1 AND year=?2 AND name=?3;", -1, &checkAlbumExistance, NULL);
    sqlite3_bind_int( checkAlbumExistance, 1, artistID); // prepared statement, parameter pos, value
    sqlite3_bind_int( checkAlbumExistance, 2, year); // prepared statement, parameter pos, value
    sqlite3_bind_text(checkAlbumExistance, 3, album_name.c_str(), (int)album_name.size() , SQLITE_STATIC);

    
    int query_status = sqlite3_step(checkAlbumExistance);
    int albumID = DOESNT_EXIST;
    
    if(query_status ==  SQLITE_ROW){
        albumID = sqlite3_column_int(checkAlbumExistance,0);
        //cout<< "DatabaseService::albumExists>> Found album! ID = "<< albumID << endl;
    }
    sqlite3_reset(checkAlbumExistance);

    return albumID;
}

int DatabaseService::songExists(int artistID, int albumID, int year, string name){
//"SELECT songID FROM music_artists natural inner join Music natural inner join music_album where artistID=?1 AND albumID=?2;"

    
    int songID = DOESNT_EXIST;
    sqlite3_bind_int( checkSongExistance, 1, artistID); // prepared statement, parameter pos, value
    sqlite3_bind_int( checkSongExistance, 2, albumID); // prepared statement, parameter pos, value
    sqlite3_bind_text(checkSongExistance, 3, name.c_str(), (int)name.size() , SQLITE_STATIC);
    sqlite3_bind_int( checkSongExistance, 4, year); // prepared statement, parameter pos, value
    //    sqlite3_prepare_v2(db,"SELECT songID FROM music_artists natural inner join Music natural inner join music_album where artistID=?1 AND albumID=?2 AND name=?3 AND year=?4;",-1, &checkSongExistance,NULL);

    int query_status = sqlite3_step(checkSongExistance);
    
    if(query_status ==  SQLITE_ROW){
        songID = sqlite3_column_int(checkSongExistance,0);
        //cout<< "DatabaseService::songExists>> Found song! ID = "<< songID << endl;
    }
    sqlite3_reset(checkSongExistance);
    return songID;
}

int DatabaseService::fingerprintExists(string fingerprint, int &songID){
    
    songID = DOESNT_EXIST;
    int status = DOESNT_EXIST;
    sqlite3_bind_text(checkFingerprintExistance, 1, fingerprint.c_str(), (int)fingerprint.size() , SQLITE_STATIC);

    int query_status = sqlite3_step(checkFingerprintExistance);
    if(query_status ==  SQLITE_ROW){
        cout << "DatabaseService::fingerprintExists>> HIT! " <<  endl;

        songID = sqlite3_column_int(checkSongExistance,0);

        status = SUCCESS;
        cout<< "DatabaseService::songExists>> Found Fingerprint!" << endl;
    }
    
    sqlite3_reset(checkFingerprintExistance);
    return status;


}


bool DatabaseService::closeDB(){

        return sqlite3_close(db);
}




int DatabaseService::insertArtist(string artist_name){

    sqlite3_bind_text(insertArtistDB, 1, artist_name.c_str(), (int)artist_name.size() , SQLITE_STATIC);
    
    int query_status = sqlite3_step(insertArtistDB);
    sqlite3_reset(insertArtistDB);

    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::insertArtist>> Problem inserting artist: "<< artist_name << endl;
        return INSUCCESS;
    }
    

    return artistExists( artist_name);



}

int DatabaseService::insertMusic_album(int songID, int albumID){

    

    //sqlite3_prepare_v2(db, "INSERT into music_album (songID,albumID) values (?1,?2);", -1, &insertMusic_AlbumDB, NULL);
    //sqlite3_prepare_v2(db, "INSERT into music_artists (songID, artistID) values (?1,?2);", -1, &insertMusic_ArtistDB, NULL);

    sqlite3_bind_int( insertMusic_AlbumDB, 1, songID); // prepared statement, parameter pos, value
    sqlite3_bind_int( insertMusic_AlbumDB, 2, albumID); // prepared statement, parameter pos, value

    int query_status = sqlite3_step(insertMusic_AlbumDB);
    sqlite3_reset(insertMusic_AlbumDB);

    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::insertMusic_album>> Problem inserting Music_album, songID = : "<< songID << " , albumID="<< albumID<<   endl;
        return INSUCCESS;
    }

    return SUCCESS;
}



int DatabaseService::insertMusic_Artists(int songID, int artistID){

    sqlite3_bind_int( insertMusic_ArtistDB, 1, songID); // prepared statement, parameter pos, value
    sqlite3_bind_int( insertMusic_ArtistDB, 2, artistID); // prepared statement, parameter pos, value
    
    int query_status = sqlite3_step(insertMusic_ArtistDB);
    sqlite3_reset(insertMusic_ArtistDB);
    
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::insertMusic_Artists>> Problem inserting Music_Artists, songID = : "<< songID << " , artistID="<< artistID<<   endl;
        return INSUCCESS;
    }
    
    return SUCCESS;

}









DatabaseService::~DatabaseService(){

}


int DatabaseService::existsInDatabase(const string _strSongName, const string _strArtistName, const string _strAlbumName, const int year){

    int artistID = artistExists( _strArtistName ) ;
    if (artistID == DOESNT_EXIST)
        return DOESNT_EXIST;
    
    int albumID = albumExists(_strAlbumName, year, artistID);
    
    if(albumID == DOESNT_EXIST)
        return DOESNT_EXIST;
    
    return songExists( artistID,  albumID,  year,  _strSongName);



}




/****TODO!!!!!!!!!!****/
/****TODO!!!!!!!!!!****/
/****TODO!!!!!!!!!!****/
/****TODO!!!!!!!!!!****/
/****TODO!!!!!!!!!!****/
/****TODO!!!!!!!!!!****/


bool DatabaseService::clearDatabase(){

    char *sErrMsg;
    sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &sErrMsg);

    int query_status = sqlite3_step(deleteAlbums);
    sqlite3_reset(deleteAlbums);
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::clearDatabase>> Problem deleting database... Albums table - "<< sqlite3_errmsg(db) <<    endl;
        sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);

        return INSUCCESS;
        
    }
    cout<< "DatabaseService::clearDatabase>> Deleted Albums! "<<   endl;

    query_status = sqlite3_step(deleteArtists);
    sqlite3_reset(deleteArtists);
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::clearDatabase>> Problem deleting database... Artists table"<<   endl;
        sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);

        return INSUCCESS;

    }
    cout<< "DatabaseService::clearDatabase>> Deleted Artists! "<<   endl;

    query_status = sqlite3_step(deleteFingerprints);
    sqlite3_reset(deleteFingerprints);
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::clearDatabase>> Problem deleting database... Fingerprints table - No fingerprints"<<   endl;
        //sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);
        //return INSUCCESS;

    }
    cout<< "DatabaseService::clearDatabase>> Deleted Fingerprints! "<<   endl;

    query_status = sqlite3_step(deleteMusicAlbums);
    sqlite3_reset(deleteMusicAlbums);
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::clearDatabase>> Problem deleting database... music_albums table"<<   endl;
        //sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);
        //return INSUCCESS;

    }
    cout<< "DatabaseService::clearDatabase>> Deleted music_albums! "<<   endl;


    query_status = sqlite3_step(deleteMusicArtists);
    sqlite3_reset(deleteMusicArtists);
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::clearDatabase>> Problem deleting database... music_artists table"<<   endl;
        //sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);
        //return INSUCCESS;

    }
    cout<< "DatabaseService::clearDatabase>> Deleted music_artists! "<<   endl;

    query_status = sqlite3_step(deleteSongs);
    sqlite3_reset(deleteSongs);
    if(query_status !=  SQLITE_DONE){
        cout<< "DatabaseService::clearDatabase>> Problem deleting database... Music table"<<   endl;
        //sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);
        //return INSUCCESS;

    }
    sqlite3_exec(db, "UPDATE Counter SET Number=0 where Type='Songs' ", NULL, NULL, &sErrMsg);
    sqlite3_exec(db, "UPDATE Counter SET Number=0 where Type='Fingerprints' ", NULL, NULL, &sErrMsg);

    sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);
    
    cout<< "DatabaseService::clearDatabase>> Deleted songs! "<<   endl;

    return SUCCESS;
    
}

void DatabaseService::incrementFingerprintNumber(int fingerprints){
    char *sErrMsg;
    string command_to_increment ="UPDATE Counter SET Number=Number +";
    command_to_increment += to_string(fingerprints);
    command_to_increment +=" where Type='Fingerprints' ";
    sqlite3_exec(db, command_to_increment.c_str(), NULL, NULL, &sErrMsg);
    
}

string DatabaseService::getStatus(){
    string result ="\t ********* Database Status ********* \n\t Number of songs = ";
    result += to_string(getNumberOfSongs());
    result += "\n\t Number of fingerprints = ";
    result += to_string(getNumberOfFingerprints()) +"\n";
    return result;

}



int DatabaseService::getNumberOfFingerprints(){
    int query_status = sqlite3_step(countFingerprints);
    int fingerprint_count;
    
    //if(query_status ==  SQLITE_ROW){
        fingerprint_count = sqlite3_column_int(countFingerprints,0);
        sqlite3_reset(countFingerprints);
        return fingerprint_count;
    //}
    //sqlite3_reset(countFingerprints);
    //cout<< "DatabaseService::getNumberOfFingerprints>> Problem getting fingerprint number " << endl;
    //return INSUCCESS;

}
unsigned long DatabaseService::getNumberOfSongs(){

    int query_status = sqlite3_step(countSongs);
    int song_count = 0;
    
    //if(query_status ==  SQLITE_ROW){
        song_count = sqlite3_column_int(countSongs,0);
        sqlite3_reset(countSongs);
        return song_count;
    //}
    //cout<< "DatabaseService::getNumberOfSongs>> Problem getting song number:  " << sqlite3_errmsg(db) << endl;
    //sqlite3_reset(countSongs);
    //return INSUCCESS;

}

void DatabaseService::startTransaction(){
    char *sErrMsg;
    sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, &sErrMsg);
    
    
}

void DatabaseService::endTransaction(){
    char *sErrMsg;
    sqlite3_exec(db, "END TRANSACTION", NULL, NULL, &sErrMsg);
    
    
}

void DatabaseService::commitTransction(){
    char *sErrMsg;
    sqlite3_exec(db, "COMMIT;", NULL, NULL, &sErrMsg);
    
}


