//
//  FingerprintInserter.cpp
//  DataBaseFiller
//
//  Created by Ana on 15/06/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#include "FingerprintInserter.h"
#include "Writter.h"

unsigned short int FingerprintInserter::biggestScore = 0; // dummy value
unsigned short int FingerprintInserter::songIDwithBiggestScore=DOESNT_EXIST;
bool FingerprintInserter::result_reached = false;
map<unsigned short int,unsigned short int> FingerprintInserter::mapSongScore;
map<unsigned short int,unsigned short int> FingerprintInserter::mapSongLastHitFrame;
map<unsigned short int,string> FingerprintInserter::mapSongIDtoName;
set<unsigned short int> FingerprintInserter::black_list;
map<unsigned short int,unsigned short int> FingerprintInserter::mapSongIDHits;
bool FingerprintInserter::interval_fingerprint_generation = false;





unsigned short int FingerprintInserter::insertFingerprintIntoDB(string fingerprint, int songID, DatabaseService database){
    int dummy;
    if(database.fingerprintExists(fingerprint, dummy) == DOESNT_EXIST){
        database.insertFingerprint(fingerprint, songID);
        return SUCCESS;
    }
    cout << "FingerprintInserter >> insertFingerprintIntoDB : Fingerprint exists!!! Fingerprint = "<<fingerprint << " Song : "<< songID <<  endl;
    return INSUCCESS;
    
}



unsigned short int FingerprintInserter::calculateFingerprintsForSong( vector<vector<essentia::Real>> frequencies_for_all_frames , unsigned short int  _iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database, unsigned short int  songID){
    
    int frame = 0;
    int counter = 0;
    
    // it is started in the DatabaseService insertSong
    //database.startTransaction();
    
    cout << "*Insertion in database has started.*"<< endl;

    //for every frame
    for(vector<vector<essentia::Real>>::iterator frequencies_iterator = frequencies_for_all_frames.begin() ; frequencies_iterator != frequencies_for_all_frames.end(); frequencies_iterator++){
        
        //essentia::Real current_frame_time = frame * (_iHopSize/_rSampleRate);
        if(frame == _iFrame_number-1 )
            break;
        
        //for every frequency data on frame
        for (vector<essentia::Real>::iterator frequencies_of_current_frame = (*frequencies_iterator).begin(); frequencies_of_current_frame != (*frequencies_iterator).end(); frequencies_of_current_frame ++ ){
            essentia::Real current_frequency = *(frequencies_of_current_frame);
            
            
            //for each following 5 frames, make fingerprints with the combination of their peaks
            for (int nextFrame = 1 ; frequencies_iterator !=frequencies_for_all_frames.end() && nextFrame < AHEAD_FRAME_LIMIT; nextFrame ++ ){
                if(frame + nextFrame >= _iFrame_number)
                    continue;
                //for each peak in frame
                for (vector<essentia::Real>::iterator frequencies_of_next_frame = (*(frequencies_iterator + nextFrame)).begin(); frequencies_of_next_frame != (*(frequencies_iterator + nextFrame)).end(); frequencies_of_next_frame ++ ){
                    
                    essentia::Real next_frequency = *(frequencies_of_next_frame);
                    
                    if(current_frequency == next_frequency)
                        continue;

                    //essentia::Real next_frame_time = (frame + nextFrame) * (_iHopSize/_rSampleRate);
                    string fingerprint = Fingerprinter::makeFingerprint(nextFrame, current_frequency ,next_frequency);

                    //insertion in Database
                    insertFingerprintIntoDB( fingerprint, songID, database);
                    counter++;
                    if(counter%50000 == 0){
                        cout << " +50000 done ! Counter = " << counter<<  endl;
                        database.commitTransction();
                        //database.endTransaction();
                        //database.startTransaction();
                        
                    }
                }// add /Users/ana/Teses/Ana/Original/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
                
            }//122319 - Cat
            //4749321 - Queen -> 755153
            //729333 - Rick Astley
            //6584542
            
        }
        ++frame;
        
    }
    database.incrementFingerprintNumber(counter);
    database.endTransaction();

    return SUCCESS;

}


unsigned short int FingerprintInserter::calculateFingerprintsForSample(vector<vector<essentia::Real> > frequencies_for_all_frames, unsigned short int  iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database)
{
    
    int frame = 0, counter = 0, hits = 0, songID = DOESNT_EXIST;
    
    set<pair<int,int>> setHitsPerSong;
    map<int,int> mapSongHits;

    cout << "* Matching has started. *"<< endl;
    database.startTransaction();

    //for every frame
    for(vector<vector<essentia::Real>>::iterator frequencies_iterator = frequencies_for_all_frames.begin() ; frequencies_iterator != frequencies_for_all_frames.end(); frequencies_iterator++){
        
        //essentia::Real current_frame_time = frame * (_iHopSize/_rSampleRate);
        if(frame == iFrame_number-1 )
            break;
        
        //for every frequency data on frame
        for (vector<essentia::Real>::iterator frequencies_of_current_frame = (*frequencies_iterator).begin(); frequencies_of_current_frame != (*frequencies_iterator).end(); frequencies_of_current_frame ++ ){
            essentia::Real current_frequency = *(frequencies_of_current_frame);
            
            
            //for each following 5 frames, make fingerprints with the combination of their peaks
            for (int nextFrame = 1 ; frequencies_iterator !=frequencies_for_all_frames.end() && nextFrame < AHEAD_FRAME_LIMIT; nextFrame ++ ){
                if(frame + nextFrame >= iFrame_number)
                    continue;
                //for each peak in frame
                for (vector<essentia::Real>::iterator frequencies_of_next_frame = (*(frequencies_iterator + nextFrame)).begin(); frequencies_of_next_frame != (*(frequencies_iterator + nextFrame)).end(); frequencies_of_next_frame ++ ){
                    
                    essentia::Real next_frequency = *(frequencies_of_next_frame);
                    
                    if(current_frequency == next_frequency)
                        continue;
                    
                    //essentia::Real next_frame_time = (frame + nextFrame) * (_iHopSize/_rSampleRate);
                    string fingerprint = Fingerprinter::makeFingerprint(nextFrame, current_frequency ,next_frequency);
                    //cout << "Before search" <<endl;
                    //search for particular fingerprint in database
                    database.fingerprintExists(fingerprint, songID);
                    //cout << "After search" <<endl;

                    //if there was a hit on the database
                    if(  songID != DOESNT_EXIST){
                        
                        cout << " HIT! " <<  endl;
                        //if is a hit on a song already identified (even if wrongly)
                        if(mapSongHits.find(songID) != mapSongHits.end())
                            mapSongHits[songID] = mapSongHits[songID]+1;
                        else
                            mapSongHits[songID] = 1;
                        cout << " HIT! " <<  endl;

                        hits++;
                    
                    }
                    counter++;
                    if(counter%500 == 0){
                        cout << " +500 done ! Counter = " << counter<<  endl;
                        //database.commitTransction();
                    }
                    
                }
                
            }
            
        }
        ++frame;
        
    }
    database.endTransaction();
    //print results
    cout <<" *** Result of matching sample fingerprints against database ***" << endl;
    
    cout <<"\t Number of frames analyzed : " << iFrame_number << endl;
    cout <<"\t Sample Seconds : " << (double) iFrame_number * (_iHopSize/_rSampleRate) << endl;

    cout <<"\t Number of generated fingerprints : " << counter << endl;
    cout <<"\t Number of hits : " << hits << endl;
    cout <<"\t Ratio hits/generated : " << (double)hits/counter << endl;
    cout <<"\t Ratio hits/frame : " << (double)hits/iFrame_number << endl;
    cout <<"\t Ratio hits/seconds : " << (double)hits/((double) iFrame_number * (_iHopSize/_rSampleRate)) << endl;

    cout <<"\n\t Results: (if any)" << endl;

    map<int,int>::iterator songHitIterator = mapSongHits.begin();
    
    for(;songHitIterator != mapSongHits.end(); songHitIterator++){
        
        cout << "SongID: "<< songHitIterator->first << " Hits: " << songHitIterator->second << endl;
    
    
    }
    
    cout << "*** End of sample matching analisys *** " << endl;
    
    return SUCCESS;

}







unsigned short int FingerprintInserter::calculateFingerprintsForSong(  vector<vector<essentia::Real>> bass1_frequencies_for_all_frames,  vector<vector<essentia::Real>> bass2_frequencies_for_all_frames ,  vector<vector<essentia::Real>> bass3_frequencies_for_all_frames,  vector<vector<essentia::Real>> bass4_frequencies_for_all_frames, unsigned short int  _iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database, map<string,map<unsigned short int, FingerprintMetaData*>> &fingerprints, unsigned short int songID, string songName){
    
    unsigned short int frame = 0, counter = 0;
    
    // it is started in the DatabaseService insertSong
    //database.startTransaction();
    mapSongIDtoName[songID] = songName;
    cout << "*Insertion in RAM has started.*"<< endl;
    
    

    // does a combination for every frequency in that frame
    unsigned short int bass1frequency,bass2frequency,bass3frequency,bass4frequency ;
    
    //[ [] ]
    
    vector<vector<essentia::Real>>::iterator  bass1_frame_iterator = bass1_frequencies_for_all_frames.begin();
    vector<essentia::Real>::iterator bass1_frequency_iterator = (*bass1_frame_iterator).begin();
    
    vector<vector<essentia::Real>>::iterator  bass2_frame_iterator = bass2_frequencies_for_all_frames.begin();
    vector<essentia::Real>::iterator bass2_frequency_iterator = (*bass2_frame_iterator).begin();
    
    vector<vector<essentia::Real>>::iterator  bass3_frame_iterator = bass3_frequencies_for_all_frames.begin();
    vector<essentia::Real>::iterator bass3_frequency_iterator = (*bass3_frame_iterator).begin();
    
    vector<vector<essentia::Real>>::iterator  bass4_frame_iterator = bass4_frequencies_for_all_frames.begin();
    vector<essentia::Real>::iterator bass4_frequency_iterator = (*bass4_frame_iterator).begin();
    
    while (frame < _iFrame_number)
    {
        //ordered fingerprints, crescent ordering bass1 -> bass2 -> bass3 -> bass4 -> mid1 -> mid2
        //subsets : all -> full score! , mid1 x mid2 , bass1 x bass2 , bass1 x bass3, bass1 x bass4, bass1 
        
        //TODO: TIRAR!
        if(frame ==   100)
            break;
        //cout << "\n Frame " << frame << endl;
        //gets frequency for every range in that frame
        
        if( (*bass4_frame_iterator).size() == 0 || (*bass1_frame_iterator).size() == 0 || (*bass2_frame_iterator).size() == 0 || (*bass3_frame_iterator).size() == 0)
        {
          //  cout << " > Jumped fingerprint because it has not enougth members!  1: "<<(*bass1_frame_iterator).size() <<" ; 2: " << (*bass2_frame_iterator).size() << " 3: "<< (*bass3_frame_iterator).size()<< " ; 4: " <<  (*bass4_frame_iterator).size()<< endl;
            frame++;
            bass1_frame_iterator++;
            bass2_frame_iterator++;
            bass3_frame_iterator++;
            bass4_frame_iterator++;
            
            bass1_frequency_iterator = (*bass1_frame_iterator).begin();
            bass2_frequency_iterator = (*bass2_frame_iterator).begin();
            bass3_frequency_iterator = (*bass3_frame_iterator).begin();
            bass4_frequency_iterator = (*bass4_frame_iterator).begin();


            continue;
        }
        
        bass1frequency = *bass1_frequency_iterator;
        bass2frequency = *bass2_frequency_iterator;
        bass3frequency = *bass3_frequency_iterator;
        bass4frequency = *bass4_frequency_iterator;




        

        /*
            if((*bass1_frame_it).size() != 0 )
                bass1frequency = (unsigned short int)*bass1_range_frequencies_peaks_iterator_on_frame;
            else bass1frequency = (FREQUENCY_THRESHOLD_BASS1_MIN + FREQUENCY_THRESHOLD_BASS1_MAX)/2;
            
        if((*bass1_frame_it).size() != 0 )
                bass2frequency = (FREQUENCY_THRESHOLD_BASS2_MIN + FREQUENCY_THRESHOLD_BASS2_MAX)/2;
            else bass2frequency = 0;
            
        if((*bass1_frame_it).size() != 0 )
                bass3frequency = (FREQUENCY_THRESHOLD_BASS3_MIN + FREQUENCY_THRESHOLD_BASS3_MAX)/2;
            else bass3frequency = 0;
            
        if((*bass1_frame_it).size() != 0 )
                bass4frequency = (FREQUENCY_THRESHOLD_BASS4_MIN + FREQUENCY_THRESHOLD_BASS4_MAX)/2;
            else bass4frequency = 0;
         
         
            unsigned short int mid_range1frequency = *mid_range1_frequencies_peaks_iterator_on_frame;
            unsigned short int mid_range2frequency = *mid_range2_frequencies_peaks_iterator_on_frame;
             */
            
            //makes fingerprints
        
            string fingerprint = to_string(getPartialFingerprint(bass1frequency) + getPartialFingerprint(bass2frequency) + getPartialFingerprint(bass3frequency) + getPartialFingerprint(bass4frequency)); // + getPartialFingerprint(mid_range1frequency) + getPartialFingerprint(mid_range2frequency)) ;


                cout << "\n Frame " << frame << "[ORIGINAL] Frequencies: " << bass1frequency << "-" << bass2frequency << "-"<< bass3frequency << "-" << bass4frequency << " Fingerprint1 = "  << fingerprint << " Fingerprint2 = "  << makeFingerprint((long)bass1frequency, (long)bass2frequency,(long) (long)bass3frequency, (long)bass4frequency) << endl;//"-" << mid_range1frequency << "-"<< mid_range2frequency<< " Fingerprint = " <<  fingerprint<< endl;
            

            /* ********    INSERTION      ******** */
            map<string,map<unsigned short int, FingerprintMetaData*>>::iterator fingerprint_structure_iterator = fingerprints.find(fingerprint);
            //if fingerprint already exists
            if(fingerprint_structure_iterator != fingerprints.end())
            {
                map<unsigned short int, FingerprintMetaData*>::iterator FPMetadata_per_song_iterator = fingerprint_structure_iterator->second.find(songID);
                //if there is a fingerprint for that song already
                if(FPMetadata_per_song_iterator != fingerprint_structure_iterator->second.end())
                {
                    //if already exists a FPMetadata for that fingerprint AND that song
                    FPMetadata_per_song_iterator->second->addTime(frame);
                
                }
                //if there is not, one shall be created
                else
                {
                    FingerprintMetaData* fpmd = new FingerprintMetaData(songID);
                    fpmd->addTime(frame);
                    fingerprints[fingerprint][songID] = fpmd;
                }
            
            
            }
        
            //if the fingerprint does not exist, then one shall be created
            else
            {
                FingerprintMetaData* fpmd = new FingerprintMetaData(songID);
                fpmd->addTime(frame);
                fingerprints[fingerprint][songID] = fpmd;
            }
        
        
        
            counter++;
            /*
            //updates iterators
            bass1_range_frequencies_peaks_iterator_on_frame++;
            bass2_range_frequencies_peaks_iterator_on_frame++;
            bass3_range_frequencies_peaks_iterator_on_frame++;
            bass4_range_frequencies_peaks_iterator_on_frame++;
     
            mid_range1_frequencies_peaks_iterator_on_frame++;
            mid_range1_frequencies_peaks_iterator_on_frame++;
        */
        
        bass1_frame_iterator++;
        bass2_frame_iterator++;
        bass3_frame_iterator++;
        bass4_frame_iterator++;
        
        bass1_frequency_iterator = (*bass1_frame_iterator).begin();
        bass2_frequency_iterator = (*bass2_frame_iterator).begin();
        bass3_frequency_iterator = (*bass3_frame_iterator).begin();
        bass4_frequency_iterator = (*bass4_frame_iterator).begin();
                                           /*
            mid_range1_frame_it++;
            mid_range2_frame_it++;
        */
            frame++;

        
    }
    
    
    return SUCCESS;
    
}


unsigned short int FingerprintInserter::calculateFingerprintsForSample(vector<const vector<vector<essentia::Real>> * > frequencies_for_all_frames, unsigned short int  iFrame_number, unsigned short int  _iHopSize, float _rSampleRate, DatabaseService database, map<string,map<unsigned short int, FingerprintMetaData*>> fingerprints)
{
    
    unsigned short int frame = 0, songID = DOESNT_EXIST, counter_of_hits_in_this_cloud = 0, aux_max_of_diferent_combinations_in_cloud = 0;
    int total_in_cloud_hits = 0, counter = 0, hits = 0, score = 0;
    signed short int upper_limit = 0;
    
    for(unsigned short int pos = 1 ; pos < PEAK_PER_FRAME_DB; pos++ ){
    
        aux_max_of_diferent_combinations_in_cloud += PEAK_PER_FRAME_DB - pos;
    }
    set<string> sampleHittedFingerprints;
    set <essentia::Real> frequencies_involved_in_cloud;
    map<unsigned short int,unsigned short int> songIDHits;
    map<unsigned short int,unsigned short int> originalFrameToHits; //local
    map<unsigned short int,unsigned short int> originalFrameToSong; //local
    map<unsigned short int,pair<unsigned short int,unsigned short int >> sampleFrameToOriginalFrame; //global


    cout << "* Matching has started. RAM has " << fingerprints.size() << " fingerprints *"<<endl;

    
    
    
    vector<const vector<vector<essentia::Real>> * >::iterator all_range_frequencies_peaks_iterator = frequencies_for_all_frames.begin();
    
    vector<vector<essentia::Real>> bass1 = **all_range_frequencies_peaks_iterator;
    vector<essentia::Real>::iterator bass1_range_frequencies_peaks_iterator = (*(bass1.begin())).begin();
    all_range_frequencies_peaks_iterator++;

    
    vector<vector<essentia::Real>> bass2 = **all_range_frequencies_peaks_iterator;
    vector<essentia::Real>::iterator bass2_range_frequencies_peaks_iterator = (*(bass2.begin())).begin();
    all_range_frequencies_peaks_iterator++;

    vector<vector<essentia::Real>> bass3 = **all_range_frequencies_peaks_iterator;
    vector<essentia::Real>::iterator bass3_range_frequencies_peaks_iterator = (*(bass3.begin())).begin();
    all_range_frequencies_peaks_iterator++;

    vector<vector<essentia::Real>> bass4 =**all_range_frequencies_peaks_iterator;
    vector<essentia::Real>::iterator bass4_range_frequencies_peaks_iterator = (*(bass4.begin())).begin();
    all_range_frequencies_peaks_iterator++;

    vector<vector<essentia::Real>> mid_range1 = **all_range_frequencies_peaks_iterator;
    vector<essentia::Real>::iterator mid_range1_frequencies_peaks_iterator = (*(mid_range1.begin())).begin();
    all_range_frequencies_peaks_iterator++;

    vector<vector<essentia::Real>> mid_range2 = **all_range_frequencies_peaks_iterator;
    vector<essentia::Real>::iterator mid_range2_frequencies_peaks_iterator = (*(mid_range2.begin())).begin();
    
    // does a combination for every frequency in that frame
    while (frame < iFrame_number)
    {
        //ordered fingerprints, crescent ordering bass1 -> bass2 -> bass3 -> bass4 -> mid1 -> mid2
        //subsets : all -> full score! , mid1 x mid2 , bass1 x bass2 , bass1 x bass3, bass1 x bass4, bass1
        
        //TODO: TIRAR!
        if(frame == 1000)
            continue;
        
        //gets frequency for every range
        while(bass1_range_frequencies_peaks_iterator != (*(bass1.begin())).end() ){
            
        unsigned short int bass1frequency = *bass1_range_frequencies_peaks_iterator;
        unsigned short int bass2frequency = *bass2_range_frequencies_peaks_iterator;
        unsigned short int bass3frequency = *bass3_range_frequencies_peaks_iterator;
        unsigned short int bass4frequency = *bass4_range_frequencies_peaks_iterator;
        unsigned short int mid_range1frequency = *mid_range1_frequencies_peaks_iterator;
        unsigned short int mid_range2frequency = *mid_range2_frequencies_peaks_iterator;
        
        //makes fingerprints
        
        string fingerprint = to_string(getPartialFingerprint(bass1frequency) + getPartialFingerprint(bass2frequency) + getPartialFingerprint(bass3frequency) + getPartialFingerprint(bass4frequency) + getPartialFingerprint(mid_range1frequency) + getPartialFingerprint(mid_range2frequency)) ;
        
        
        if(frame <= 15)
        {
            cout << "[SAMPLE] Frequencies: " << *bass1.begin() << "-" << *bass2.begin() << "-"<< *bass3.begin() << "-" << *bass4.begin() << "-" << *mid_range1.begin() << "-"<< *mid_range2.begin()<< " Fingerprint = " <<  fingerprint<< endl;
            
            
        }
        
        /* ********    IDENTIFICATION NEW      ******** */
        map<string,map<unsigned short int, FingerprintMetaData*>>::iterator hit = fingerprints.find(fingerprint);
        //if fingerprint already exists
        if(hit != fingerprints.end())
        {

            for(map<unsigned short int, FingerprintMetaData*>::iterator songIDIterator = hit->second.begin() ; songIDIterator != hit->second.end() ; songIDIterator ++ )
            {
                
                songID = songIDIterator->first ;
                mapSongIDHits[songID] = mapSongIDHits[songID] +1;

                cout << "[SAMPLE] Cloud hit! with  " << bass1frequency << "-" << bass2frequency << "-"<< bass2frequency << "-" << bass3frequency << "-" << bass3frequency << "-"<< bass4frequency<< "  = " <<  mid_range1frequency <<  "-" << mid_range1frequency << endl;
    
                updateCounters(originalFrameToHits,originalFrameToSong,sampleFrameToOriginalFrame,frame, songIDIterator->second,songID);
                
                counter_of_hits_in_this_cloud++;
                hits++;
            }
        
        
        }
        counter++;
            
            
            
            
        
        
        //updates iterators
        bass1_range_frequencies_peaks_iterator++;
        bass2_range_frequencies_peaks_iterator++;
        bass3_range_frequencies_peaks_iterator++;
        bass4_range_frequencies_peaks_iterator++;
        
        mid_range1_frequencies_peaks_iterator++;
        mid_range1_frequencies_peaks_iterator++;
            
        }
        
        /*
        // iterar originalFrameToHits à procura do que tem mais hits -> intersecções
        unsigned short int intersection_existance =  intersectionFinder(originalFrameToHits);
        if(intersection_existance != DOESNT_EXIST)
        {
            unsigned short int current_frame_score = 2;
            
            unsigned short int hits = originalFrameToHits[intersection_existance];
            cout << "* Intersection results > frame =  " << intersection_existance << " of song " << originalFrameToSong[intersection_existance] <<  " with  " <<hits << "hits" <<endl;
            
            map<unsigned short int, pair<unsigned short int,unsigned short int>>::iterator iterator_sampleToOriginal =  sampleFrameToOriginalFrame.find(originalFrameToSong[intersection_existance]);
            
            
            
            if (hits >= SCORE_THRESHOLD_LEVEL1 && hits < SCORE_THRESHOLD_LEVEL2)
            {
                current_frame_score = POINTS_3;
            }
            
            else if (hits >= SCORE_THRESHOLD_LEVEL2 && hits < SCORE_THRESHOLD_LEVEL3){
                
                current_frame_score = POINTS_5;
                
            }
            
            else if (hits >= SCORE_THRESHOLD_LEVEL3)
            {
                current_frame_score = POINTS_8;
            }
            else if (hits > SCORE_THRESHOLD_LEVEL3)
            {
                current_frame_score = POINTS_10;
            }
            
            
            //TODO: check if already existed!
            
            if(iterator_sampleToOriginal != sampleFrameToOriginalFrame.end() )
            {
                // if the difference is the same
                if(frame - iterator_sampleToOriginal->second.first == intersection_existance - iterator_sampleToOriginal->second.second )
                {
                    cout << " Result Confirmed. The intersection is corrected. EXTRA POINTS!" << endl;
                    current_frame_score += POINTS_2;
                    
                    
                }
                
            }
            songID = originalFrameToSong[intersection_existance] ;
            
            //updates or creates
            sampleFrameToOriginalFrame[songID] = pair<unsigned short int,unsigned short int> (frame, intersection_existance);
            //updates score and checks if it we have a match!
            score = mapSongScore[songID] + current_frame_score;
            
            if(score > biggestScore )
            {
                biggestScore = score;
                songIDwithBiggestScore = songID;
            }
            
            if(score > SCORE_THRESHOLD && frame > FRAME_THRESHOLD_LEVEL1){
                result_reached = true;
                printDataToScreen( counter, _iHopSize,  _rSampleRate ,  frame ,  hits ,total_in_cloud_hits);
                return SUCCESS;
                
                
            }
            mapSongScore[songID] =  score;
            
        }

        */
        
        ++frame;
        
        // TODO : RETIRAR!
        if (frame > 500){
            printDataToScreen( counter, _iHopSize,  _rSampleRate ,  frame ,  hits ,total_in_cloud_hits);
            return SUCCESS;
            
        }
        
        
    }
    
    
    
    
    /****************************   OLD     *************************************/
    /*
    
    
    //for every frame
    for(vector<vector<essentia::Real>>::iterator frequencies_iterator = frequencies_for_all_frames.begin() ; frequencies_iterator != frequencies_for_all_frames.end(); frequencies_iterator++)
    {
        if(frame == 1000)
            continue;

        //essentia::Real current_frame_time = frame * (_iHopSize/_rSampleRate);
        if(frame == iFrame_number-1 )
            break;
        
        if(frame <50)
        {
        cout << "\n [SAMPLE] Frequencies of frame " << frame <<  "/" << iFrame_number <<" are :";

        for (vector<essentia::Real>::iterator frequencies_of_current_frame = (*frequencies_iterator).begin(); frequencies_of_current_frame != (*frequencies_iterator).end(); frequencies_of_current_frame ++ )
        {
            essentia::Real current_frequency = *(frequencies_of_current_frame);
            cout << " " <<  current_frequency << " ";
        
        }
        cout << endl;
        }
        
        signed short int aux_add = 0;

        //for every frequency data on frame
        for (vector<essentia::Real>::iterator frequencies_of_current_frame = (*frequencies_iterator).begin(); frequencies_of_current_frame != (*frequencies_iterator).end(); frequencies_of_current_frame ++ )
        {
            
            essentia::Real current_frequency = *(frequencies_of_current_frame);
            essentia::Real real_current_frequency  = current_frequency;
            if(frame > FRAME_THRESHOLD_LEVEL1 && !interval_fingerprint_generation && total_in_cloud_hits < HITS_MIN_THRESHOLD)
            {
                cout << " \n ***** Started search with interval! ****** \n"  << endl;
                interval_fingerprint_generation = true;
                upper_limit = UPPER_INTERVAL;
                aux_add = LOWER_INTERVAL;
                
            }
            
            ***************************** CURRENT FRAME **********************************
            
            //searching for frequencies with current frames
            for(signed short int value_to_add_current_frequency = aux_add ; value_to_add_current_frequency <= upper_limit ; value_to_add_current_frequency ++)
            {
                for(signed short int  value_to_add_next_frequency = aux_add; value_to_add_next_frequency <= upper_limit ; value_to_add_next_frequency ++)
                {
                    
                    for(vector<essentia::Real>::iterator frequencies_left_in_frame = frequencies_of_current_frame +1; frequencies_left_in_frame != (*frequencies_iterator).end(); frequencies_left_in_frame ++)
                    {
                        essentia::Real next_frequency = *frequencies_left_in_frame;


                        if ( current_frequency + value_to_add_current_frequency > next_frequency + value_to_add_next_frequency)
                        {
                            current_frequency = next_frequency + value_to_add_next_frequency;
                            next_frequency = current_frequency + value_to_add_current_frequency;
                        }
                        else
                        {
                            current_frequency += value_to_add_current_frequency;
                            next_frequency += value_to_add_next_frequency;
                        }

                        string fingerprint = Fingerprinter::makeFingerprint(0, current_frequency ,next_frequency );
                        counter++;
                        //search in "DB" and give points for it if it there was a match
                        map<string,map<unsigned short int, FingerprintMetaData*>>::iterator hit;
                        //cout << "\t Fingerprint - ["  << current_frequency  << " , " << next_frequency << " ]"<< endl;

                        // for the real frequency
                        if ( (hit = fingerprints.find(fingerprint)) != fingerprints.end())
                        {
                            frequencies_involved_in_cloud.insert(current_frequency);
                            frequencies_involved_in_cloud.insert(next_frequency);
                    
                            for(map<unsigned short int, FingerprintMetaData*>::iterator songIDIterator = hit->second.begin() ; songIDIterator != hit->second.end() ; songIDIterator ++ )
                            {
                        
                                songID = songIDIterator->first ;
                                mapSongIDHits[songID] = mapSongIDHits[songID] +1;
                                cout << " [SongIDs ( cloud ?) = " << songID << "] HIT! with frequencies [" <<current_frequency  << "," << next_frequency << "] ,  frames = "<< songIDIterator->second->getTimes() <<  endl;
                        
                                updateCounters(originalFrameToHits,originalFrameToSong,sampleFrameToOriginalFrame,frame, songIDIterator->second,songID);
                                
                                counter_of_hits_in_this_cloud++;
                                hits++;
                            }
                        }
                        current_frequency =  real_current_frequency;
                    }
                }
                    
                counter++;
            }
            
        }
                    ***************************************************************

        if(counter_of_hits_in_this_cloud > 0){
            total_in_cloud_hits+=counter_of_hits_in_this_cloud;
            cout << "Cloud inspection for frame " << frame << ": " << endl;
            cout << "* Hits in cloud : " << counter_of_hits_in_this_cloud << " of a total of " << aux_max_of_diferent_combinations_in_cloud << " possibilities. "<< endl;

    
            // iterar originalFrameToHits à procura do que tem mais hits -> intersecções
            unsigned short int intersection_existance =  intersectionFinder(originalFrameToHits);
            if(intersection_existance != DOESNT_EXIST)
            {
                unsigned short int current_frame_score = 2;

                unsigned short int hits = originalFrameToHits[intersection_existance];
                cout << "* Intersection results > frame =  " << intersection_existance << " of song " << originalFrameToSong[intersection_existance] <<  " with  " <<hits << "hits" <<endl;
                
                map<unsigned short int, pair<unsigned short int,unsigned short int>>::iterator iterator_sampleToOriginal =  sampleFrameToOriginalFrame.find(originalFrameToSong[intersection_existance]);


                
                if (hits >= SCORE_THRESHOLD_LEVEL1 && hits < SCORE_THRESHOLD_LEVEL2)
                {
                    current_frame_score = POINTS_3;
                }
                
                else if (hits >= SCORE_THRESHOLD_LEVEL2 && hits < SCORE_THRESHOLD_LEVEL3){
                
                    current_frame_score = POINTS_5;
                
                }
                
                else if (hits >= SCORE_THRESHOLD_LEVEL3)
                {
                    current_frame_score = POINTS_8;
                }
                else if (hits > SCORE_THRESHOLD_LEVEL3)
                {
                    current_frame_score = POINTS_10;
                }
                
                
                //TODO: check if already existed!
                
                if(iterator_sampleToOriginal != sampleFrameToOriginalFrame.end() )
                {
                    // if the difference is the same
                    if(frame - iterator_sampleToOriginal->second.first == intersection_existance - iterator_sampleToOriginal->second.second )
                    {
                        cout << " Result Confirmed. The intersection is corrected. EXTRA POINTS!" << endl;
                        current_frame_score += POINTS_2;
                
                    
                    }
                        
                }
                songID = originalFrameToSong[intersection_existance] ;
                
                //updates or creates
                sampleFrameToOriginalFrame[songID] = pair<unsigned short int,unsigned short int> (frame, intersection_existance);
                //updates score and checks if it we have a match!
                score = mapSongScore[songID] + current_frame_score;
                
                if(score > biggestScore )
                {
                    biggestScore = score;
                    songIDwithBiggestScore = songID;
                }

                if(score > SCORE_THRESHOLD && frame > FRAME_THRESHOLD_LEVEL1){
                    result_reached = true;
                    printDataToScreen( counter, _iHopSize,  _rSampleRate ,  frame ,  hits ,total_in_cloud_hits);
                    return SUCCESS;
                    

                }
                mapSongScore[songID] =  score;
            
            }
            
            
            //iterar originalFrameToSong
            
            cout << "* Frequencies in cloud : " << frequencies_involved_in_cloud << endl;
            cout << endl;
            counter_of_hits_in_this_cloud = 0;
            frequencies_involved_in_cloud.clear();
            originalFrameToHits.clear(); //local
            originalFrameToSong.clear(); //local
        
        }
        
        ++frame;
        if( double(hits/frame) < 1.0 && frame >= FRAME_THRESHOLD_CONSIDER_MUSIC_NOT_FOUND){
            
            //printDataToScreen( counter, _iHopSize,  _rSampleRate ,  iFrame_number ,  hits );
            //return SUCCESS;
            
        }
        if (frame > 1000){
            printDataToScreen( counter, _iHopSize,  _rSampleRate ,  frame ,  hits , total_in_cloud_hits);
            return SUCCESS;

        }

        
    }
    */
    /****************************   OLD  END    *************************************/

    //Writter::writeMapToFile(sampleFingerprints.begin(), sampleFingerprints.end(), "sampleFingerprints.dat");
    //print results
    printDataToScreen( counter, _iHopSize,  _rSampleRate ,  iFrame_number ,  hits ,total_in_cloud_hits);
    return SUCCESS;
    
}


 void FingerprintInserter::printDataToScreen( int counter,unsigned short int _iHopSize, float _rSampleRate , unsigned short int iFrame_number , int hits , int total_in_cloud_hits){


    cout <<" *** Result of matching sample fingerprints against RAM ***" << endl;
    
    cout <<"\t Number of frames analyzed : " << iFrame_number << endl;
    cout <<"\t Sample Seconds : " << (double) iFrame_number * (_iHopSize/_rSampleRate) << endl;
    
    cout <<"\t Number of generated fingerprints : " << counter << endl;
    cout <<"\t Number of hits : " << hits << " [ "<< total_in_cloud_hits << " from Cloud hits]"<< endl;
    cout <<"\t Ratio hits/generated : " << (double)hits/counter << endl;
    cout <<"\t Ratio hits/frame : " << (double)hits/iFrame_number << endl;
    cout <<"\t Ratio hits/seconds : " << (double)hits/((double) iFrame_number * (_iHopSize/_rSampleRate)) << endl;
    
    
    cout <<"\n\t Results: (if any)" << endl;
    if(result_reached){
        cout << "Identification made! Result >> Song: "<< mapSongIDtoName[songIDwithBiggestScore] << endl;
        
    }
    else{
        cout << "It was not possible to make a reliable identification. More details: " << endl;
        
        if(mapSongScore.empty())
            cout << " Not a single match with our database" <<endl;
        
        else{
            if(songIDwithBiggestScore != DOESNT_EXIST)
                cout << "The most similar song found was: SongID = " << songIDwithBiggestScore << " with "<< biggestScore << " as seen in the following list of hits: " << endl;
            else
                cout << " There is no similar music in our database. " << endl;
        }
    }
         cout << "Full list of positives (false and true) songs:  " << endl;

         map<unsigned short int,unsigned short int>::iterator songHitIterator = mapSongScore.begin();
     
         for(;songHitIterator != mapSongScore.end(); songHitIterator++)
         {
             int aux_songID = songHitIterator->first;
             cout << mapSongIDtoName[aux_songID] <<" (SongID: "<< aux_songID <<  ") Score: " << songHitIterator->second <<  " Hits : " << mapSongIDHits[aux_songID] << endl;
         
         
        
         }
     set<unsigned short int>::iterator blackListIterator = black_list.begin();
     for(;blackListIterator != black_list.end(); blackListIterator++)
     {
         int aux_songID = *blackListIterator;
         cout << mapSongIDtoName[aux_songID] <<" (SongID: "<< aux_songID <<  " --  Blacklist) Hits : " << mapSongIDHits[aux_songID]  << endl;
         
         
         
     }
     
    cout << "*** End of sample matching analysis *** " << endl;
     mapSongScore.clear();
     mapSongLastHitFrame.clear();
     biggestScore = DOESNT_EXIST; // dummy value
     songIDwithBiggestScore=DOESNT_EXIST;
     result_reached = false;
     black_list.clear();
     mapSongIDHits.clear();
     interval_fingerprint_generation = false;




}

unsigned short int FingerprintInserter::pointsForProximity(unsigned short int lastHitFrame, unsigned short int currentFrame, unsigned short int currentScore)
{
    unsigned short int difference = currentFrame - lastHitFrame;
    // if there was already a hit with the same frame, then extra points will be added to the score
    //because it is unlikely to exist 2 or more hits with the same first frame if that song is not the correct one. (TODO: verify if it is enough to return a response!)
    //cout << "Frame difference = "<< difference << endl;
    return 1;
    /*
    if ( difference == SAME_FRAME)
        return REPEATED_HIT_ON_FRAME_POINTS;
    else if ( difference < FRAME_THRESHOLD_LEVEL1)
        return POINTS_9;
    else if ( difference < FRAME_THRESHOLD_LEVEL2)
        return POINTS_8;
    else if ( difference < FRAME_THRESHOLD_LEVEL3)
        return  POINTS_5;
    else if(difference < FRAME_THRESHOLD_LEVEL4)
        return POINTS_3;
    else if(difference >= FRAME_THRESHOLD_LEVEL4 && difference <= FRAME_THRESHOLD_LEVEL5){
        //if(currentScore > POINTS_5)
            return POINTS_2;
        //else return -(currentScore -1);
    }
    //removes points, if the probability of a false positive is high
    else {
        //if(currentScore > POINTS_8)
            return POINTS_1;
        //else return -(currentScore -1);

    
    }*/
}

void FingerprintInserter::updateHitsOfSong(unsigned short int songID)
{
    map<unsigned short int,unsigned short int>::iterator hitIterator = mapSongIDHits.find(songID);
    
    if(hitIterator != mapSongIDHits.end())
    {
        unsigned short int previous_hits = hitIterator->second;
        hitIterator->second = previous_hits+1;
        
    }
    else
        mapSongIDHits[songID] = 1;
    
}

void FingerprintInserter::addToBlackList( unsigned short int songID )
{
    black_list.insert(songID);
    mapSongScore.erase(songID);
    mapSongLastHitFrame.erase(songID);

}


void FingerprintInserter::addCurrentFrameFingerprints(vector<essentia::Real> current_frame_frequencies,unsigned short int songID, map<string,map<unsigned short int, FingerprintMetaData*>> &fingerprints, unsigned short int frame){

    // Has to make every combination (or the most meaningful) and add them to the database
    
    string fingerprint;
    essentia::Real freq1,freq2;
    
    vector<essentia::Real>::iterator aux_iterator_for_the_other_frequency;
    for (vector<essentia::Real>::iterator frequencies_iterator = current_frame_frequencies.begin(); frequencies_iterator != current_frame_frequencies.end(); frequencies_iterator ++ )
    {
        
        //TODO: MULTITHREADING ! - With next frame

        //TODO: MULTITHREADING ! - With current frame
        for (aux_iterator_for_the_other_frequency = frequencies_iterator; aux_iterator_for_the_other_frequency != current_frame_frequencies.end(); aux_iterator_for_the_other_frequency ++)
        {
            if(aux_iterator_for_the_other_frequency == frequencies_iterator)
                continue;
            
            freq1 = *frequencies_iterator;
            freq2 = *aux_iterator_for_the_other_frequency;
            unsigned short int diff = freq2 -freq1;
            if(freq2-freq1 < 0)
                diff*=-1;
            if(diff < FREQUENCY_MIN_INTERVAL)
                continue;
            
            //orders the frequencies to make half the frequencies in the DB
            //if(freq1 > freq2)
             //   fingerprint = Fingerprinter::makeFingerprint(0, freq2 ,freq1);
            //else
                fingerprint = Fingerprinter::makeFingerprint(0, freq1 ,freq2);
            
            map<string,map<unsigned short int, FingerprintMetaData*>>::iterator fingerprint_structure_iterator = fingerprints.find(fingerprint);
            //if fingerprint already exists
            if(fingerprint_structure_iterator != fingerprints.end())
            {
                map<unsigned short int, FingerprintMetaData*>::iterator FPMetadata_per_song_iterator = fingerprint_structure_iterator->second.find(songID);
                //if there is a fingerprint for that song already
                if(FPMetadata_per_song_iterator != fingerprint_structure_iterator->second.end())
                {
                    //if already exists a FPMetadata for that fingerprint AND that song
                    FPMetadata_per_song_iterator->second->addTime(frame);
                
                }
                //if there is not, one shall be created
                else
                {
                    FingerprintMetaData* fpmd = new FingerprintMetaData(songID);
                    fpmd->addTime(frame);
                    fingerprints[fingerprint][songID] = fpmd;
                }
                
                
            }
            
            //if the fingerprint does not exist, then one shall be created
            else
            {
                FingerprintMetaData* fpmd = new FingerprintMetaData(songID);
                fpmd->addTime(frame);
                fingerprints[fingerprint][songID] = fpmd;
            }
        }
    
    }

}

void FingerprintInserter::updateCounters(map<unsigned short int,unsigned short int> &originalFrameToHits,map<unsigned short int,unsigned short int> &originalFrameToSong,map<unsigned short int,pair<unsigned short int,unsigned short int>> &sampleFrameToOriginalFrame, unsigned short int frame, FingerprintMetaData* frames , unsigned short int songID )

{
    //cout << "FingerprintInserter::updateCounters>> frames of original : " <<  frames->getTimes() << endl;
    
    set<unsigned short int> frames_time = frames->getTimes();
    // for each hitted frame
    for (set<unsigned short int>::iterator original_times= frames_time.begin(); original_times != frames_time.end(); original_times++)
    {
        unsigned short int original_frame = *original_times;
        map<unsigned short int,unsigned short int>::iterator originalToHitIterator = originalFrameToHits.find(original_frame);
        if(originalToHitIterator != originalFrameToHits.end())
            originalFrameToHits[original_frame] = originalFrameToHits[original_frame] + 1;
        else
        {
            originalFrameToHits[original_frame] = 1;

        }
        originalFrameToSong[original_frame] = songID ; // adds to the music dictionary

    }
    
}


unsigned short int FingerprintInserter::intersectionFinder( map<unsigned short int,unsigned short int> &originalFrameToHits)
{
    unsigned short int max_hits = 1;
    unsigned short int frameWithMostHits = DOESNT_EXIST;
    
    
    for(map<unsigned short int,unsigned short int>::iterator hitsIterator = originalFrameToHits.begin(); hitsIterator != originalFrameToHits.end() ; hitsIterator++)
    
    {
        
        if(hitsIterator->second > max_hits)
        {
            frameWithMostHits = hitsIterator-> first;
            max_hits = hitsIterator -> second;
        }
    
    }
    
    if((max_hits < 3 && !interval_fingerprint_generation) || (max_hits < 10 && interval_fingerprint_generation))
        return DOESNT_EXIST;
    
    else return frameWithMostHits;
}


unsigned short int FingerprintInserter::getPartialFingerprint(unsigned short int frequency)
{
    
    
    
    unsigned short int lastDigit = frequency%10;
    
    if( lastDigit <= 2 && lastDigit >= 0)
       return frequency - lastDigit;
       
       else if ( lastDigit >= 5 && lastDigit <= 7 )
           return frequency - (5- lastDigit);
        else if ( lastDigit >= 8 && lastDigit < 10 )
            return frequency + (10 - lastDigit );
        else
            return frequency + (5- lastDigit);

}


long FingerprintInserter::makeFingerprint(long p1, long p2,long p3, long p4) {
  
    long FUZ_FACTOR = 2;
    return (p4 - (p4 % FUZ_FACTOR)) * 100000000 + (p3 - (p3 % FUZ_FACTOR))
    * 100000 + (p2 - (p2 % FUZ_FACTOR)) * 100
    + (p1 - (p1 % FUZ_FACTOR));
}



/*
 
radd /Users/ana/Teses/Ana/Original/1minuto/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
radd /Users/ana/Teses/Ana/Original/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
ridentify /Users/ana/Teses/Ana/Original/1minuto/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
ridentify /Users/ana/Copy/SAMPLes/10s/Boss-AC-Boa-vibe-10s-sample1.wav
ridentify /Users/ana/Copy/SAMPLes/boss-boa-vibe-sample2.wav
ridentify /Users/ana/Copy/SAMPLes/boa-vibe-menos-ruido.wav

 
// 90 Seconds Boa vibe com muito ruído
radd /Users/ana/Teses/Ana/Original/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
ridentify /Users/ana/Copy/SAMPLes/Full/wav/Boss-AC-Boa-vibe-full-sample.wav
 
 // 90 Seconds Boa vibe com menos ruído
radd /Users/ana/Teses/Ana/Original/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
ridentify /Users/ana/Copy/SAMPLes/boa-vibe-menos-ruido.wav

 

 
 
radd /Users/ana/Teses/Ana/Original/Never_Gonna_Give_You_Up-Rick_Astley-Whenever_You_Need_Somebody-1987.wav

ridentify /Users/ana/Teses/Ana/Codigo/Samples/Misturas_com_ruído/Rick_Astley/footsteps-1/Rick_Astley-Never_Gonna_Give_You_Up.wav

ridentify /Users/ana/Teses/Ana/Codigo/Samples/Misturas_com_ruído/Rick_Astley/WalkingInSnow/Rick_Astley-Never_Gonna_Give_You_Up.wav

 
 
 
    Ficheiros para a BD
add /Users/ana/Teses/Ana/Original/1minuto/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
add /Users/ana/Teses/Ana/Original/1minuto/You_Need_Me_I_Don't_Need_You-Ed_Sheeran-+-2011.wav
add /Users/ana/Teses/Ana/Original/1minuto/Skyfall-Adele-BondOST-2012.wav
 cout << "*Insertion in database has started.*"<< endl;

 
 
 Alguém_me_Ouviu-Boss_AC_featuring_Mariza-Preto_no_branco-2009.wav
 All_About_That_Bass-Meghan_Trainor-Title-2015.wav
 Beatsound_Loverboy-Slimmy-Beatsound_Loverboy-2007.wav
 Best Of Sade Tribute Soul Mix (Smooth Jazz Music Songs R&B Compilation Playlist By Eric The Tutor).wav
 Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
 Chandelier-Sia-1000Forms_of_Fear-2014.wav
 Counting_Stars-OneRepublic-Native-2013.wav
 Hideaway-Kiesza-Sound_of_a_Woman-2014 .wav
 I_Want_to_Break_Free-Queen-The_Works-1984.wav
 Lena_a_culpa_não_é_tua-Boss_AC-Rimar_Contra_A_Maré-2001.wav
 Mewow2-CAT-I_Hate_Mondays-1992.wav
 Mix.wav
 Never_Gonna_Give_You_Up-Rick_Astley-Whenever_You_Need_Somebody-1987.wav
 No_Ordinary_Love-Sade-Love_Deluxe-1992.wav
 Officially-Yours-Craig_David-Trust_Me-2007.wav
 Paradise-Coldplay-Mylo_Xyloto-2011.wav
 Piano.ff.A7.aiff
 Piano.mf.D5.aiff
 Piano.mf.G1.aiff
 Royals-Lorde-Pure_Heroine-2013.wav
 Skyfall-Adele-BondOST-2012.wav
 Smells_Like_Teen_Spirit-Nirvana-Nevermind-1991.wav
 Sugar-Maroon_5-V_2014 .wav
 System of a Down - Toxicity.wav
 Take_Me_To_Church-Hozier-Hozier-2014.wav
 Taylor Swift - Shake It Off.wav
 The_Look_Of_Love-Diana_Krall-The_Look_Of_Love-2001.wav
 The_Pretender-Foo_Fighters-Echoes,Silence,Patience&Grace-2007.wav
 Thinking_Out_Loud-Ed_Sheeran-x-2014.wav
 Thirty Seconds to Mars - This is War Lyrics.wav
 Tu_És_Mais_ForteBoss_AC_featuring_Shout-AC_Para_os_amigos-2012.wav
 You_Need_Me,I_Don't_Need_You-Ed_Sheeran-+-2011.wav
 
 
 3714362504-86.132812-419.301514-0.0058sql05
 1980109764-86.132812-2313.972168-0.005805
 
ridentify /Users/ana/Copy/SAMPLes/boa-vibe-menos-ruido.wav

 ********** 1m original 35 sample! ************
 
 
// 1m Original Sample 35 seconds Royals ID = 1
radd /Users/ana/Teses/Ana/Original/1minuto/Royals-Lorde-Pure_Heroine-2013.wav
ridentify /Users/ana/Copy/SAMPLes/35s/royals.wav
 
// 1m Original Sample 35 seconds Thinking out loud ID = 2
radd /Users/ana/Teses/Ana/Original/1minuto/Thinking_Out_Loud-Ed_Sheeran-x-2014.wav
ridentify /Users/ana/Copy/SAMPLes/35s/thinking-ed.wav
 
 
// 1m Original Sample 35 seconds Boa vibe ID = 3
radd /Users/ana/Teses/Ana/Original/1minuto/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
ridentify /Users/ana/Copy/SAMPLes/35s/Boss-AC-Boa-vibe.wav
ridentify /Users/ana/Copy/SAMPLes/ruido_injectado/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
 
 
// 1m Original Sample 35 seconds Kieza ID = 4
radd /Users/ana/Teses/Ana/Original/1minuto/Hideaway-Kiesza-Sound_of_a_Woman-2014 .wav
ridentify /Users/ana/Copy/SAMPLes/35s/kieza.wav
 
radd /Users/ana/Teses/Ana/Original/1minuto/You_Need_Me_I_Don't_Need_You-Ed_Sheeran-+-2011.wav
ridentify /Users/ana/Copy/SAMPLes/Full/wav/i-dont-need-ed.wav

 **********  ********** ************
 
radd /Users/ana/Teses/Ana/Original/1minuto/Magic-Flute-Patricia-2014.wav
ridentify /Users/ana/Copy/SAMPLes/35s/Patricia.wav

ridentify /Users/ana/Copy/SAMPLes/boa-vibe-amostra-sem-ruido.wav

ridentify /Users/ana/Teses/Ana/Original/1minuto/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
 
 **********  ********** ************ **********  ********** ************
 **********  ********** Specific Frequencies ************ **************
 **********  ********** ************ **********  ********** ************
radd /Users/ana/Teses/Ana/Original/highFreq-pureTone-Freqs-2015.wav
ridentify /Users/ana/Copy/SAMPLes/high-freq-with-noise.wav

radd /Users/ana/Teses/Ana/Original/lowFreq-pureTone-Freqs-2015.wav
ridentify /Users/ana/Copy/SAMPLes/low-freq-with-noise.wav
 
radd /Users/ana/Teses/Ana/Original/1minuto/Boa_vibe2-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
ridentify /Users/ana/Copy/SAMPLes/Boa_vibe_acompanha_original-Boss_AC-RAP-2005.wav
radd /Users/ana/Copy/SAMPLes/Boa_vibe_acompanha_original-Boss_AC-RAP-2005.wav

 */