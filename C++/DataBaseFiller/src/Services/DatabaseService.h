//
//  DatabaseService.h
//  DataBaseFiller
//
//  Created by Ana on 13/05/15.
//  Copyright (c) 2015 MTG. All rights reserved.
//

#ifndef __DataBaseFiller__DatabaseService__
#define __DataBaseFiller__DatabaseService__

#include <stdio.h>
#include "../DataTypes/AudioProperties.h"
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include "Codes.h"
#include <sqlite3.h>


using namespace std;
using namespace codes;

class DatabaseService{
    
public:
    ~DatabaseService();
    bool connectToDatabase();
    int insertSongInDatabase(AudioProperties *pAudioProperties);
    int insertSongInDatabase(string song_name, int year, int artistID, int albumID);
    bool clearDatabase();
    int existsInDatabase(string const _strSongName, string const _strArtistName, string const _strAlbumName,const int year);
    bool closeDB();
    unsigned long getNumberOfSongs();
    int getNumberOfFingerprints();
    string getStatus();
    int fingerprintExists(string fingerprint, int &songID);
    int insertFingerprint(string fingerprint, int songID);
    sqlite3* getDB();
    void startTransaction();
    void endTransaction();
    void commitTransction();
    void incrementFingerprintNumber(int fingerprints);
    
private:
    sqlite3 *db;
    
    //existance methods
    int artistExists(string artist_name);
    int songExists(int artistID, int albumID, int year, string name);
    int albumExists(string album_name, int year, int artistID);
    
    //insertion methods
    int insertAlbum(string album_name, int year, int artistID);
    int insertArtist(string artist_name);
    int insertMusic_album(int songID, int albumID);
    int insertMusic_Artists(int songID, int artistID);
    
    //prepared statements
    sqlite3_stmt *checkArtistExistance;
    sqlite3_stmt *checkAlbumExistance;
    sqlite3_stmt *checkSongExistance;
    sqlite3_stmt *checkFingerprintExistance;
    sqlite3_stmt *countFingerprints;
    sqlite3_stmt *countSongs;
    
    sqlite3_stmt *insertAlbumDB;
    sqlite3_stmt *insertArtistDB;
    sqlite3_stmt *insertSongDB;
    sqlite3_stmt *insertFingerprintDB;
    sqlite3_stmt *insertMusic_AlbumDB;
    sqlite3_stmt *insertMusic_ArtistDB;
    
    //sqlite3_stmt *deleteDatabase;
    sqlite3_stmt *deleteArtists;
    sqlite3_stmt *deleteAlbums;
    sqlite3_stmt *deleteSongs;
    sqlite3_stmt *deleteFingerprints;
    sqlite3_stmt *deleteMusicArtists;
    sqlite3_stmt *deleteMusicAlbums;
    
    //return statements
    sqlite3_stmt* getLastSongID;
    
    
    
    
    
    //TODO: delete operations
    //TODO: update operations
    
};




#endif /* defined(__DataBaseFiller__DatabaseService__) */