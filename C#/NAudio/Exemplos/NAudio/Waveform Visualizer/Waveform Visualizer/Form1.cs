﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Waveform_Visualizer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void openWAVFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //abre painel de Browse file
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Wave File (*.wav)| *.wav;";

            if (open.ShowDialog() != DialogResult.OK)
                return;

            waveViewer1.WaveStream = new NAudio.Wave.WaveFileReader(open.FileName);

        }
    }
}
