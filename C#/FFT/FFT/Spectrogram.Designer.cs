﻿namespace FFT
{
    partial class Spectrogram
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar15 = new System.Windows.Forms.ProgressBar();
            this.progressBar14 = new System.Windows.Forms.ProgressBar();
            this.progressBar13 = new System.Windows.Forms.ProgressBar();
            this.progressBar12 = new System.Windows.Forms.ProgressBar();
            this.progressBar11 = new System.Windows.Forms.ProgressBar();
            this.progressBar10 = new System.Windows.Forms.ProgressBar();
            this.progressBar9 = new System.Windows.Forms.ProgressBar();
            this.progressBar8 = new System.Windows.Forms.ProgressBar();
            this.progressBar7 = new System.Windows.Forms.ProgressBar();
            this.progressBar6 = new System.Windows.Forms.ProgressBar();
            this.progressBar5 = new System.Windows.Forms.ProgressBar();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.progressBar16 = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // progressBar15
            // 
            this.progressBar15.Location = new System.Drawing.Point(502, 3);
            this.progressBar15.Name = "progressBar15";
            this.progressBar15.Size = new System.Drawing.Size(29, 132);
            this.progressBar15.TabIndex = 11;
            // 
            // progressBar14
            // 
            this.progressBar14.Location = new System.Drawing.Point(467, 3);
            this.progressBar14.Name = "progressBar14";
            this.progressBar14.Size = new System.Drawing.Size(29, 132);
            this.progressBar14.TabIndex = 12;
            // 
            // progressBar13
            // 
            this.progressBar13.Location = new System.Drawing.Point(432, 3);
            this.progressBar13.Name = "progressBar13";
            this.progressBar13.Size = new System.Drawing.Size(29, 132);
            this.progressBar13.TabIndex = 13;
            // 
            // progressBar12
            // 
            this.progressBar12.Location = new System.Drawing.Point(397, 3);
            this.progressBar12.Name = "progressBar12";
            this.progressBar12.Size = new System.Drawing.Size(29, 132);
            this.progressBar12.TabIndex = 23;
            // 
            // progressBar11
            // 
            this.progressBar11.Location = new System.Drawing.Point(362, 3);
            this.progressBar11.Name = "progressBar11";
            this.progressBar11.Size = new System.Drawing.Size(29, 132);
            this.progressBar11.TabIndex = 22;
            // 
            // progressBar10
            // 
            this.progressBar10.Location = new System.Drawing.Point(327, 3);
            this.progressBar10.Name = "progressBar10";
            this.progressBar10.Size = new System.Drawing.Size(29, 132);
            this.progressBar10.TabIndex = 21;
            // 
            // progressBar9
            // 
            this.progressBar9.Location = new System.Drawing.Point(292, 3);
            this.progressBar9.Name = "progressBar9";
            this.progressBar9.Size = new System.Drawing.Size(29, 132);
            this.progressBar9.TabIndex = 20;
            // 
            // progressBar8
            // 
            this.progressBar8.Location = new System.Drawing.Point(257, 3);
            this.progressBar8.Name = "progressBar8";
            this.progressBar8.Size = new System.Drawing.Size(29, 132);
            this.progressBar8.TabIndex = 19;
            // 
            // progressBar7
            // 
            this.progressBar7.Location = new System.Drawing.Point(222, 3);
            this.progressBar7.Name = "progressBar7";
            this.progressBar7.Size = new System.Drawing.Size(29, 132);
            this.progressBar7.TabIndex = 14;
            // 
            // progressBar6
            // 
            this.progressBar6.Location = new System.Drawing.Point(187, 3);
            this.progressBar6.Name = "progressBar6";
            this.progressBar6.Size = new System.Drawing.Size(29, 132);
            this.progressBar6.TabIndex = 15;
            // 
            // progressBar5
            // 
            this.progressBar5.Location = new System.Drawing.Point(152, 3);
            this.progressBar5.Name = "progressBar5";
            this.progressBar5.Size = new System.Drawing.Size(29, 132);
            this.progressBar5.TabIndex = 16;
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(117, 3);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(29, 132);
            this.progressBar4.TabIndex = 18;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(82, 3);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(29, 132);
            this.progressBar3.TabIndex = 17;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(47, 3);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(29, 132);
            this.progressBar2.TabIndex = 10;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(12, 3);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(29, 132);
            this.progressBar1.TabIndex = 9;
            // 
            // progressBar16
            // 
            this.progressBar16.Location = new System.Drawing.Point(537, 3);
            this.progressBar16.Name = "progressBar16";
            this.progressBar16.Size = new System.Drawing.Size(29, 132);
            this.progressBar16.TabIndex = 24;
            // 
            // Spectrogram
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.progressBar16);
            this.Controls.Add(this.progressBar15);
            this.Controls.Add(this.progressBar14);
            this.Controls.Add(this.progressBar13);
            this.Controls.Add(this.progressBar12);
            this.Controls.Add(this.progressBar11);
            this.Controls.Add(this.progressBar10);
            this.Controls.Add(this.progressBar9);
            this.Controls.Add(this.progressBar8);
            this.Controls.Add(this.progressBar7);
            this.Controls.Add(this.progressBar6);
            this.Controls.Add(this.progressBar5);
            this.Controls.Add(this.progressBar4);
            this.Controls.Add(this.progressBar3);
            this.Controls.Add(this.progressBar2);
            this.Controls.Add(this.progressBar1);
            this.Name = "Spectrogram";
            this.Size = new System.Drawing.Size(575, 141);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar15;
        private System.Windows.Forms.ProgressBar progressBar14;
        private System.Windows.Forms.ProgressBar progressBar13;
        private System.Windows.Forms.ProgressBar progressBar12;
        private System.Windows.Forms.ProgressBar progressBar11;
        private System.Windows.Forms.ProgressBar progressBar10;
        private System.Windows.Forms.ProgressBar progressBar9;
        private System.Windows.Forms.ProgressBar progressBar8;
        private System.Windows.Forms.ProgressBar progressBar7;
        private System.Windows.Forms.ProgressBar progressBar6;
        private System.Windows.Forms.ProgressBar progressBar5;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.ProgressBar progressBar16;
    }
}
