package Repository;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.jtransforms.fft.DoubleFFT_1D;

import AudioTools.PeakFinder.PeakFinder;
import AudioTools.PeakFinder.SimplePeakFinder;
import AudioTools.Readers.WavFile;
import AudioTools.Readers.WavFileException;
import AudioTools.Windows.Hanning;
import Entities.FingerprintMetaData;
import Hashing.Fingerprinter;
import Music.Song;
import Utilities.Codes;
import Utilities.Complex;
import Utilities.MainUtilities;
import Utilities.OutputWritter;

public class MusicRepository implements Repository{

	private static Map<Integer, HashMap<Integer, FingerprintMetaData>> _fingerprints;
	private static Map<Integer, HashMap<Integer, FingerprintMetaData>> _fingerprintsHanning;

	private static Map<Integer, Song> _music;

	private static int _number_of_songs;
	//private static Matcher _matcher;
	private static Fingerprinter _fingerprinter;
	//private static int hits_for_frame_
	private static int _bit_depth;
	private static PeakFinder _peak_finder;
	int music_winner = Codes.NO_MATCH;
	
	
	public MusicRepository( Fingerprinter fingerprinter, int bit_depth,PeakFinder peakFinder) {

		_fingerprints = new HashMap<Integer, HashMap<Integer,FingerprintMetaData>>();	
		_fingerprintsHanning = new HashMap<Integer, HashMap<Integer,FingerprintMetaData>>();
		_music = new HashMap<Integer, Song>();
		//_matcher = matcher;
		_fingerprinter = fingerprinter;
		_bit_depth = bit_depth;
		_peak_finder = peakFinder;
		
		}
	
	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return _fingerprints.size();
	}
	
	private static byte[] getAudioBytes(String path_to_audio) throws IOException, UnsupportedAudioFileException{
		
		File audio_file = new File(path_to_audio);
		//lê ficheiro de áudio
		AudioInputStream audio_input_stream = AudioSystem
				.getAudioInputStream(audio_file);
		byte audio[] = new byte[(int) audio_file.length()];
		audio_input_stream.read(audio);

		return audio;
		
	}
	
	public int identify(String sample_path,String sample_name) throws IOException, UnsupportedAudioFileException, WavFileException
	{
		byte[]audio8 = null;
		int result = Codes.NO_MATCH;
		//original_results_file.createNewFile();
		FileWriter writer = new FileWriter(Codes.SAMPLE_DATA_FOLDER + sample_name +"-" + MainUtilities.getTimestampForFileName() + ".samp");
		FileWriter fp_debug_writer = new FileWriter(Codes.FINGERPRINTS_FOLDER + sample_name +"-" + MainUtilities.getTimestampForFileName() + ".fpss");
		_fingerprinter.setFileWriter(fp_debug_writer);
		
		if(_bit_depth == 8) 
		{
			audio8 = getAudioBytes(sample_path);
			result = getSamplePeaksPerFrame(audio8);	
		}
		else
		{
			WavFile wavFile = WavFile.openWavFile(new File(sample_path));
			
			//Para mostrar dados da amostra!
			//System.out.println("\nidentify>> Info da amostra: " + sample_path);
			//wavFile.display();
			System.out.println();
			
				double[] frame = new double[Codes.FRAME_SIZE];
				int samples_read = 1;
				int frame_counter=0;
				//System.out
				//.println("********* Vou inserir a original "+ song.getName() + " na BD *********\n");
				while(result == Codes.NO_MATCH && samples_read > 0)
				{

					
					samples_read = wavFile.readFrames(frame, Codes.FRAME_SIZE);
					//TODO:Manda ler os picos de cada frame
					result = get16bitPeaksPerFrame(frame,  writer,frame_counter,Codes.NO_MATCH,true);
					frame_counter++;
				

			}
		}
		//int audio_size = audio.length;
		
		//int number_of_frames = audio_size / Codes.FRAME_SIZE
		//		+ (audio_size - Codes.FRAME_SIZE*Codes.HOP_RATIO) / Codes.FRAME_SIZE; // hop size

		//System.out
		//.println("********* Vou procurar Sample na BD *********\n");
			
		
		if(result != Codes.NO_MATCH)
		{
			System.out.println(generateSuccessMessage());/*
			int sample_frame_recognition = _fingerprinter.getMatcher().getFrameHitted();
			int offset = _fingerprinter.getMatcher().getWinnerOffset();
			int original_first_frame = _fingerprinter.getMatcher().getFirstFrameOfOriginalCorrespondentToTheSample();
			double original_first_frame_time = convertFrameToSeconds(_fingerprinter.getMatcher().getFirstFrameOfOriginalCorrespondentToTheSample());
			double sample_time_needed_to_identification = convertFrameToSeconds(sample_frame_recognition);
			System.out.println(">Identificação feita!");
			System.out.println(">Música Reconhecida : " + _music.get(result).getName() + " na frame: "
					+ sample_frame_recognition + " com o offset " + offset );
			System.out.println(">A amostra começa na frame " + original_first_frame + " da música original. ("
					+ original_first_frame_time + " segundos) ");
			System.out.println(">Identificação feita com " +  sample_time_needed_to_identification + " segundos da amostra!");
			System.out.println("\n\t\t ***************************************************************\n");
*/
		}
		else 	
			System.out.println("A identificação não foi conseguida...");
		
		System.out.println("Resultado do matching final: ");
		//TODO: colocar de novo _fingerprinter.getMatcher().printToScreenLastHits();

		fp_debug_writer.flush();
		fp_debug_writer.close();
		_fingerprinter.resetVariables();

		return result;
		
	
		
	}
	
	
	
	private int getSamplePeaksPerFrame(byte[] audio) throws IOException
	{
		
		File results_file =new File("Sample_Results.txt");
		FileWriter writer = new FileWriter(results_file);

		int success = Codes.NO_MATCH;
		int number_of_frames = audio.length / Codes.FRAME_SIZE;
				
		//apenas para debug!
		double[][] frames_frequencies_and_amplitudes_no_window = new double[number_of_frames][Codes.FRAME_SIZE * 2];
		double[][] frames_frequencies_and_amplitudes_window = new double[number_of_frames][Codes.FRAME_SIZE * 2];

		//para comparação
		Map<Integer, ArrayList<Integer>> peaksPerFrameNoWindow = new HashMap<Integer,ArrayList<Integer>>(number_of_frames);
		Map<Integer, TreeSet<Integer>> peaksPerFrameWindow = new HashMap<Integer,TreeSet<Integer>>(number_of_frames);

		// Percorrem-se os segmentos de audio para obter os números complexos

		for (int frame = 0; frame < number_of_frames; frame++) 
		{
			System.out.println("[Frame " + frame + "] \n");

			//faz-se fft a cada frame para obter as amplitudes em números complexos
			double[] fftData1 = new double[Codes.FRAME_SIZE * 2];
			Complex[] fftData2 = new Complex[Codes.FRAME_SIZE];

			for (int i = 0; i < Codes.FRAME_SIZE; i++) {
				// copying audio data to the fft data buffer, imaginary part is
				// 0
				fftData1[2 * i] = audio[frame * Codes.FRAME_SIZE / Codes.HOP_RATIO + i];
				fftData1[2 * i + 1] = 0;
				
				fftData2[i] = new Complex(audio[frame * Codes.FRAME_SIZE / Codes.HOP_RATIO + i],0);

			}
			// frames_frequencies_and_amplitudes[frame] =
			// fft(complex_numbers_of_frame);
			DoubleFFT_1D fftDo = new DoubleFFT_1D(Codes.FRAME_SIZE);
			fftDo.realForwardFull(fftData1);
			frames_frequencies_and_amplitudes_no_window[frame] = fftData1;
			
			//Algoritmo Simples
			ArrayList<Integer> local_peaks_noWindow = _peak_finder.findLocalPeaks(fftData1);
			 //addFrameFingerprintsToRepository( _fingerprinter.getFingerprintsFromPeaks(local_peaks_noWindow), frame,  songID,  Codes.NO_WINDOW);
			 peaksPerFrameNoWindow.put(frame,  local_peaks_noWindow);
			
			 
			 
				writer.write("\n\n********* Picos SEM Hanning *********\n\n");

				OutputWritter.printDataUntilFrame(frame,
						local_peaks_noWindow, writer,
						true);
				


			 
			 
			 
			 success = _fingerprinter.matchFramePeaksToDatabase(local_peaks_noWindow, _fingerprints,frame);
			 //se houver sucesso na identificação, retorna-se imediatamente ao utilizador.
			 if(success != Codes.NO_MATCH){
				 System.out.println("Resultado encontrado SEM Hanning! ");
				 
					writer.close();
					
				 
				 
				 return success;
				 }
			 
			writer.write("[Frame " + frame + "] \n");
			//escreve frequencias para cada frame SEM WINDOWING
			//writer.write("\n  SEM Hanning : \n");

			OutputWritter.writeAmplitudesOnFile(writer,fftData1);

			
			//writer.write("\n  COM Hanning : \n");
/*
			Hanning hanning_window = new Hanning();
			//uses hanning
			double [] hanning_coefficients = hanning_window.getCoefficientsForWindow(Codes.FRAME_SIZE);
			//Aplica o coeficiente de hanning para aplicar a janela e converte de volta o array de fft de complexos para doubles (representado complexos)
			for(int pos_in_coefficients = 0 ; pos_in_coefficients < hanning_coefficients.length;pos_in_coefficients++ )
			
			{
				Complex hanning_coefficient = new Complex( hanning_coefficients[pos_in_coefficients],0);
				Complex afterHanning = fftData2[pos_in_coefficients].times(hanning_coefficient);
				
				fftData1[2* pos_in_coefficients] = afterHanning.real() ;
				fftData1[2* pos_in_coefficients + 1] = afterHanning.imag();

			}
				
			
			
			DoubleFFT_1D fftDo2 = new DoubleFFT_1D(Codes.FRAME_SIZE);
			fftDo2.realForwardFull(fftData1);
			frames_frequencies_and_amplitudes_window[frame] = fftData1;
			OutputWritter.writeAmplitudesOnFile(writer,fftData1);

			ArrayList<Integer> local_peaks_Hanning =_peak_finder.findLocalPeaks(fftData1);
			 //addFrameFingerprintsToRepository( _fingerprinter.getFingerprintsFromPeaks(local_peaks_noWindow), frame,  songID,  Codes.NO_WINDOW);
/*
				writer.write("\n\n********* Picos COM Hanning *********\n\n");

			 
				OutputWritter.printDataUntilFrame(frame,
						local_peaks_Hanning, writer,
						true);
		
			  	writer.flush();
			 

			 
			//escreve frequencias para cada frame COM WINDOWING
			//peaksPerFrameWindow.put(frame,local_peaks_Hanning);
			success = Codes.NO_MATCH;
			 //TODO: Repor success = _fingerprinter.matchFramePeaksToDatabase(local_peaks_Hanning, _fingerprintsHanning,frame);
			 //se houver sucesso na identificação, retorna-se imediatamente ao utilizador.
			 if(success != Codes.NO_MATCH){
				 System.out.println("Resultado encontrado COM Hanning! ");
				 
					writer.flush();
					writer.close();

				 return success;
				 }
 		*/
			
			
			
			//TODO: Activar!
			//Algoritmo mais sofisticado
			//peaksPerFrame.put(frame, AmplitudeMinPeakFinder.findLocalPeaks(fftData));

		}
		
		
		
		writer.flush();
		writer.close();

		return success;
		
		
	}
	
	public int get16bitPeaksPerFrame(double [] frame, FileWriter writer, int frame_number, int songID, boolean identify) throws IOException
	{

		// FFT tem como input nºs complexos
		
		//apenas para debug!
		//double[][] frames_frequencies_and_amplitudes_no_window = new double[number_of_frames][Codes.FRAME_SIZE * 2];
		//double[][] frames_frequencies_and_amplitudes_window = new double[number_of_frames][Codes.FRAME_SIZE * 2];

		//para comparação
		//Map<Integer, TreeSet<Integer>> peaksPerFrameNoWindow = new HashMap<Integer,TreeSet<Integer>>();
		//Map<Integer, TreeSet<Integer>> peaksPerFrameWindow = new HashMap<Integer,TreeSet<Integer>>();

		//faz-se fft a cada frame para obter as amplitudes em números complexos
		double[] fftData_no_window = new double[frame.length * 2]; // sem hanning
		//double[] amplitudes_after_hanning = new double[frame.length * 2]; // sem hanning

		Complex[] fftData_hanning_complex = new Complex[frame.length]; // para hanning
		
		// para cada amostra da frame
		for (int i = 0; i < frame.length; i++) {
			// copying audio data to the fft data buffer, imaginary part is
			// 0
			fftData_no_window[2 * i] = frame[ i];
			fftData_no_window[2 * i + 1] = 0;
			
			//fftData_hanning_complex[i] = new Complex(frame[i],0);

		}
		// frames_frequencies_and_amplitudes[frame] =
		// fft(complex_numbers_of_frame);
		
		//aplica fft às amostras obtidas - o input é em complexo mas retorna em real -> double
		DoubleFFT_1D fftDo = new DoubleFFT_1D(frame.length);
		fftDo.realForwardFull(fftData_no_window);
		//frames_frequencies_and_amplitudes_no_window[frame] = fftData1;
		
		//Algoritmo Simples
		 ArrayList<Integer> local_peaks_noWindow = (ArrayList<Integer>) _peak_finder.findLocalPeaks(fftData_no_window);
		 if(!identify)
			 addFrameFingerprintsToRepository( _fingerprinter.getFingerprintsFromPeaks(local_peaks_noWindow), frame_number,  songID,  Codes.NO_WINDOW);
		 //peaksPerFrameNoWindow.put(frame, (TreeSet<Double>) local_peaks_noWindow);
		
		writer.write("[Frame " + frame_number + "] \n");
		//escreve frequencias para cada frame SEM WINDOWING
		writer.write("\n  SEM Hanning : \n");

		OutputWritter.writeAmplitudesOnFile(writer,fftData_no_window);
		
		/*
		Hanning hanning_window = new Hanning();
		//uses hanning
		//TODO: Verificar se é preciso meter vector.lenght
		double [] hanning_coefficients = hanning_window.getCoefficientsForWindow(Codes.FRAME_SIZE);
		//Aplica o coeficiente de hanning para aplicar a janela e converte de volta o array de fft de complexos para doubles (representado complexos)
		for(int pos_in_coefficients = 0 ; pos_in_coefficients < hanning_coefficients.length;pos_in_coefficients++ )
		
		{
			Complex hanning_coefficient = new Complex( hanning_coefficients[pos_in_coefficients],0);
			Complex afterHanning = fftData_hanning_complex[pos_in_coefficients].times(hanning_coefficient);
			
			fftData_no_window[2* pos_in_coefficients] = afterHanning.real() ;
			fftData_no_window[2* pos_in_coefficients + 1] = afterHanning.imag();

		}
			
		
		
		DoubleFFT_1D fftDo2 = new DoubleFFT_1D(Codes.FRAME_SIZE);
		fftDo2.realForwardFull(fftData_no_window);
		//frames_frequencies_and_amplitudes_window[frame] = fftData1;
		
		writer.write("[Frame " + frame_number + "] \n");
		//escreve frequencias para cada frame SEM WINDOWING
		writer.write("\n  COM Hanning : \n");
*/
		
		OutputWritter.writeAmplitudesOnFile(writer,fftData_no_window);

		//ArrayList<Integer> local_peaks_Hanning =  _peak_finder.findLocalPeaks(fftData_no_window);
		
		 if(!identify){
			 addFrameFingerprintsToRepository( _fingerprinter.getFingerprintsFromPeaks(local_peaks_noWindow), frame_number,  songID,  Codes.NO_WINDOW);
		 }
			 else 
		 { 	// in case of a identification process
			 int success = _fingerprinter.matchFramePeaksToDatabase(local_peaks_noWindow, _fingerprints,frame_number);
			 //se houver sucesso na identificação, retorna-se imediatamente ao utilizador.
			 if(success != Codes.NO_MATCH)
			 {
				 music_winner = success;
				 System.out.println("* Resultado encontrado SEM Hanning! *");
					writer.write("\n\n********* Picos SEM Hanning *********\n\n");

					OutputWritter.printDataUntilFrame(frame_number,
							local_peaks_noWindow, writer,
							true);
					/*
					writer.write("\n\n********* Picos COM Hanning *********\n\n");

					OutputWritter.printDataUntilFrame(frame_number,
							local_peaks_Hanning, writer,
							true);
					writer.flush();
					
				 */
					writer.close();
					//_fingerprinter.getMatcher().printToScreenLastHits();
					_fingerprinter.getMatcher().clearResults();

				 
				 return success;
				 }
			 
			 //TODO:Voltar a meter!
			 /*
				success = _fingerprinter.matchFramePeaksToDatabase(local_peaks_Hanning, _fingerprints,frame_number);;
				 //TODO: Repor success = _fingerprinter.matchFramePeaksToDatabase(local_peaks_Hanning, _fingerprintsHanning,frame);
				 //se houver sucesso na identificação, retorna-se imediatamente ao utilizador.
				 if(success != Codes.NO_MATCH)
				 {
					 System.out.println("* Resultado encontrado COM Hanning! *");
					 
						writer.write("\n\n********* Picos SEM Hanning *********\n\n");

						OutputWritter.printDataUntilFrame(frame_number,
								local_peaks_noWindow, writer,
								true);
						
						writer.write("\n\n********* Picos COM Hanning *********\n\n");

						OutputWritter.printDataUntilFrame(frame_number,
								local_peaks_Hanning, writer,
								true);
						writer.flush();

						writer.flush();
						writer.close();
						_fingerprinter.getMatcher().clearResults();

						//_fingerprinter.getMatcher().printToScreenLastHits();

					 return success;
					 }
				 */


		 }
		//escreve frequencias para cada frame COM WINDOWING
		//peaksPerFrameWindow.put(frame_number,local_peaks_Hanning);
		
		
		
		
		
		//TODO: Activar!
		//Algoritmo mais sofisticado
		//peaksPerFrame.put(frame, AmplitudeMinPeakFinder.findLocalPeaks(fftData));
		writer.write("\n\n********* Picos SEM Hanning *********\n\n");

		OutputWritter.printDataUntilFrame(frame_number,
				local_peaks_noWindow, writer,
				true);
		/*
		writer.write("\n\n********* Picos COM Hanning *********\n\n");

		OutputWritter.printDataUntilFrame(frame_number,
				local_peaks_Hanning, writer,
				true);
				*/
		writer.flush();


		
		return Codes.NO_MATCH;
	}
	
	public void addSong(Song song) throws UnsupportedAudioFileException, IOException, WavFileException
	{
		int songID = _number_of_songs;
		song.setSongID(songID);
		
		FileWriter fp_debug_writer = new FileWriter(Codes.FINGERPRINTS_FOLDER + song.getName() +"-" + MainUtilities.getTimestampForFileName() + ".fpso");
		_fingerprinter.setFileWriter(fp_debug_writer);

		
		File original_results_file = new File(  Codes.ORIGINAL_DATA_FOLDER +song.getName() +"-" + MainUtilities.getTimestampForFileName() + ".orig");
		//original_results_file.createNewFile();
		FileWriter writer = new FileWriter(original_results_file);
		//TODO: Deveria ser depois mas para evitar problema com criação do metadata fica aqui
		 _music.put(songID, song);
		 _number_of_songs++;


		if(_bit_depth == 16)
		{
			System.out.println("\naddSong>> Info da música: " + song.getName());
			WavFile wavFile = WavFile.openWavFile(new File(song.getPath()));

			wavFile.display();
			System.out.println();
			//long number_of_samples = wavFile.getNumFrames();
			double[] frame = new double[Codes.FRAME_SIZE];
			int samples_read = 1;
			int frame_counter=0;
			System.out
			.println(">> Vou inserir a original "+ song.getName() + " na BD \n");

			while(samples_read > 0){
				
				samples_read = wavFile.readFrames(frame, Codes.FRAME_SIZE);
				//TODO:Manda ler os picos de cada frame
				get16bitPeaksPerFrame(frame,  writer,frame_counter,songID,false);
				frame_counter++;
			}
			writer.close();

		}
		else
		{
			
		byte[]audio = getAudioBytes(song.getPath());
		int audio_size = audio.length;
		int number_of_frames = audio_size / Codes.FRAME_SIZE;
		if(Codes.HOP_RATIO != 1)
			number_of_frames += (audio_size - Codes.FRAME_SIZE*Codes.HOP_RATIO) / Codes.FRAME_SIZE; // hop size
		System.out
		.println("********* Vou inserir a original "+ song.getName() + " na BD *********\n");

		getAllPeaksForEveryFrame(number_of_frames, audio,
				original_results_file, false, songID);

		}


		_fingerprinter.resetVariables();
		fp_debug_writer.flush();
		fp_debug_writer.close();
	}
	
	public int getNumberOfSongsInRepository()
	{
		return _number_of_songs;
	}
	
	private void insertFingerprint(int fingerprint, FingerprintMetaData metadata, int window)
	{
		int new_song_id = metadata.getSongID();
		//fingerprint já existe?
		if(_fingerprints.containsKey(fingerprint))
		{
			//para este fingerprint já existe uma associação com a música x?
			if(_fingerprints.get(fingerprint).containsKey(new_song_id))
			{ //se sim, então, adiciona-se apenas as frames associadas (é só uma em princípio)
				
				FingerprintMetaData current_metadata = _fingerprints.get(fingerprint).get(new_song_id);
				current_metadata.insertFrame(metadata.getFrames().first()); //TODO:em principio só tem uma! mudar caso contrário 
			
			}
			else // caso já exista o fingerprint mas não para aquela música em concreto
			{
				//adiciona-se directamente o songID e a metadata no segundo mapa
				if(window == Codes.NO_WINDOW)
				_fingerprints.get(fingerprint).put(new_song_id, metadata);
				else if(window == Codes.HANNING)
					_fingerprintsHanning.get(fingerprint).put(new_song_id, metadata);
				
			}
		}
		//caso o fingerprint não exista, então cria-se tudo do zero
		else{
			Map<Integer, FingerprintMetaData> metadata_map = new HashMap<Integer, FingerprintMetaData>();
			metadata_map.put(new_song_id, metadata);
			if(window == Codes.NO_WINDOW)
			_fingerprints.put(fingerprint, (HashMap<Integer, FingerprintMetaData>) metadata_map);
			else if(window == Codes.HANNING)
				_fingerprintsHanning.put(fingerprint, (HashMap<Integer, FingerprintMetaData>) metadata_map);

		}
	}
	
	
	
	private void addFrameFingerprintsToRepository(Iterable<Integer> iterable, int frame, int songID, int window)
	{
		Iterator<Integer> fingerprintIterator = iterable.iterator();
		while(fingerprintIterator.hasNext())
		{
			FingerprintMetaData metadata = new FingerprintMetaData(songID,_music.get(songID).getName());
			metadata.insertFrame(frame);
			insertFingerprint(fingerprintIterator.next(), metadata, window);
		}		
		
		
	}
	
	private  void getAllPeaksForEveryFrame(int number_of_frames,
			byte audio[], File results_file, boolean use_mean, int songID)
			throws IOException {


		results_file.createNewFile();
		FileWriter writer = new FileWriter(results_file);

		// FFT tem como input nºs complexos
		
		//apenas para debug!
		double[][] frames_frequencies_and_amplitudes_no_window = new double[number_of_frames][Codes.FRAME_SIZE * 2];
		double[][] frames_frequencies_and_amplitudes_window = new double[number_of_frames][Codes.FRAME_SIZE * 2];

		//para comparação
		Map<Integer, ArrayList<Integer>> peaksPerFrameNoWindow = new HashMap<Integer,ArrayList<Integer>>(number_of_frames);
		Map<Integer, TreeSet<Integer>> peaksPerFrameWindow = new HashMap<Integer,TreeSet<Integer>>(number_of_frames);

		// Percorrem-se os segmentos de audio para obter os números complexos

		for (int frame = 0; frame < number_of_frames; frame++) 
		{
			//System.out.println("[Frame " + frame + "] \n");

			//faz-se fft a cada frame para obter as amplitudes em números complexos
			double[] fftData1 = new double[Codes.FRAME_SIZE * 2];
			Complex[] fftData2 = new Complex[Codes.FRAME_SIZE];

			for (int i = 0; i < Codes.FRAME_SIZE; i++) {
				// copying audio data to the fft data buffer, imaginary part is
				// 0
				fftData1[2 * i] = audio[frame * Codes.FRAME_SIZE / Codes.HOP_RATIO + i];
				fftData1[2 * i + 1] = 0;
				
				fftData2[i] = new Complex(audio[frame * Codes.FRAME_SIZE / Codes.HOP_RATIO + i],0);

			}
			// frames_frequencies_and_amplitudes[frame] =
			// fft(complex_numbers_of_frame);
			DoubleFFT_1D fftDo = new DoubleFFT_1D(Codes.FRAME_SIZE);
			fftDo.realForwardFull(fftData1);
			frames_frequencies_and_amplitudes_no_window[frame] = fftData1;
			
			//Algoritmo Simples
			ArrayList<Integer> local_peaks_noWindow =  _peak_finder.findLocalPeaks(fftData1);
			 addFrameFingerprintsToRepository( _fingerprinter.getFingerprintsFromPeaks(local_peaks_noWindow), frame,  songID,  Codes.NO_WINDOW);
			 peaksPerFrameNoWindow.put(frame,  local_peaks_noWindow);
			
			writer.write("[Frame " + frame + "] \n");
			//escreve frequencias para cada frame SEM WINDOWING
			writer.write("\n  SEM Hanning : \n");

			OutputWritter.writeAmplitudesOnFile(writer,fftData1);

			writer.write("\n\n********* Picos SEM Hanning *********\n\n");

			OutputWritter.printDataUntilFrame(frame,
					local_peaks_noWindow, writer,
					true);
/*
			
			//writer.write("\n  COM Hanning : \n");

			Hanning hanning_window = new Hanning();
			//uses hanning
			double [] hanning_coefficients = hanning_window.getCoefficientsForWindow(Codes.FRAME_SIZE);
			//Aplica o coeficiente de hanning para aplicar a janela e converte de volta o array de fft de complexos para doubles (representado complexos)
			for(int pos_in_coefficients = 0 ; pos_in_coefficients < hanning_coefficients.length;pos_in_coefficients++ )
			
			{
				Complex hanning_coefficient = new Complex( hanning_coefficients[pos_in_coefficients],0);
				Complex afterHanning = fftData2[pos_in_coefficients].times(hanning_coefficient);
				
				fftData1[2* pos_in_coefficients] = afterHanning.real() ;
				fftData1[2* pos_in_coefficients + 1] = afterHanning.imag();

			}
				
			
			
			DoubleFFT_1D fftDo2 = new DoubleFFT_1D(Codes.FRAME_SIZE);
			fftDo2.realForwardFull(fftData1);
			frames_frequencies_and_amplitudes_window[frame] = fftData1;
			OutputWritter.writeAmplitudesOnFile(writer,fftData1);

			ArrayList<Integer> local_peaks_Hanning =  _peak_finder.findLocalPeaks(fftData1);
			 addFrameFingerprintsToRepository( _fingerprinter.getFingerprintsFromPeaks(local_peaks_noWindow), frame,  songID,  Codes.NO_WINDOW);

			//escreve frequencias para cada frame COM WINDOWING
			//peaksPerFrameWindow.put(frame,local_peaks_Hanning);
			
			writer.write("\n\n********* Picos COM Hanning *********\n\n");

			OutputWritter.printDataUntilFrame(frame,
					local_peaks_Hanning, writer,
					true);

			
			
			*/
			//TODO: Activar!
			//Algoritmo mais sofisticado
			//peaksPerFrame.put(frame, AmplitudeMinPeakFinder.findLocalPeaks(fftData));

		}
		
		
		

		
		
		// depois de se ter o fft de todas as frames/janelas

		// result is complex matrix obtained in previous step
		// obtem-se a amplitude para cada frequencia.
		if (use_mean) 
		{
			//	frames_frequencies_and_amplitudes = basic_low_pass_filter(frames_frequencies_and_amplitudes);
				// ja se tem o audio normalizado, agora pode-se ver dos picos
		}

		//TODO: TERMINAR! LIDAR COM OS PICOS DA FUNÇÃO CHAMADA
		
		
		writer.flush();
		writer.close();

	}
	
	private double convertFrameToSeconds(int frame){
		
		double time_per_frame = ((double)Codes.FRAME_SIZE/(double)Codes.SAMPLE_RATE)/(double)Codes.HOP_RATIO;
		return frame*time_per_frame;
		
	}

	@Override
	public String generateSuccessMessage() {
		String message = "";
		int sample_frame_recognition = _fingerprinter.getMatcher().getFrameHitted();
		int offset = _fingerprinter.getMatcher().getWinnerOffset();
		int original_first_frame = _fingerprinter.getMatcher().getFirstFrameOfOriginalCorrespondentToTheSample();
		double original_first_frame_time = convertFrameToSeconds(_fingerprinter.getMatcher().getFirstFrameOfOriginalCorrespondentToTheSample());
		double sample_time_needed_to_identification = convertFrameToSeconds(sample_frame_recognition);
		
		message+=(">Identificação feita!\n");
		message+=(">Música Reconhecida : " + _music.get(music_winner).getName() + " na frame da amostra: "
				+ sample_frame_recognition + " com o offset " + offset + "\n");
		message+=(">A amostra começa aproximadamente na frame " + original_first_frame + " da música original. ("
				+ (int)original_first_frame_time + " segundos) \n");
		message+=(">Identificação feita com " +  sample_time_needed_to_identification + " segundos da amostra!");
		//System.out.println("\n\t\t ***************************************************************\n");
	return message;
	}

	
	
	
	
	
}
