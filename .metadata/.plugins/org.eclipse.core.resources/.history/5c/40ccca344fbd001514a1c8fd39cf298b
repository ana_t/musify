package Hashing;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Entities.FingerprintMetaData;
import Matchers.Matcher;
import Utilities.Codes;
import Entities.PeakCombiner;

public class TwoPeaksFingerprinter implements Fingerprinter
{
	//for saving the old frame peaks. This because the repository only sends the
	//current frame peaks. Since this fingerprinter onl
	private ArrayList<Integer> _firstFramePeaks;	
	private Matcher _matcher;
	private int _mode;
	private int _frame;
	private FileWriter _writerOfFingerprintsForDebug;
	private static int _totalNumberOfCombinationsPossible = Codes.FREQUENCY_INTERVALS.length * Codes.FREQUENCY_INTERVALS.length;
	private static int _framesForFingerprint = 2;
	public TwoPeaksFingerprinter(Matcher matcher, int mode)
	{
		_firstFramePeaks = new ArrayList<Integer>();
		_matcher = matcher;
		_matcher.setNumberOfCombinationsPossiblePerGroupOfFramesAnalysis(_totalNumberOfCombinationsPossible);
		_matcher.setNumberOfFramesInvolved(_framesForFingerprint);
		_mode = mode;
		_frame = 0; // first frame
	}
	
	
	public void setFileWriter(FileWriter writer)
	{
		_writerOfFingerprintsForDebug = writer;
	}

	@Override
	public Iterable<Integer> getFingerprintsFromPeaks(
			Iterable<Integer> frame_peaks)
			{
		(ArrayList<Integer>) frame_peaks;
		if(!_firstFramePeaks.isEmpty() && (ArrayList<Integer>)frame_peaks.l)
		{
			_firstFramePeaks = new ArrayList<Integer>();
			return null;
		}

		if(_firstFramePeaks.isEmpty())
		{
			_firstFramePeaks = (ArrayList<Integer>) frame_peaks;
			return null;
		}
		// (aux) write fps to file
		if(_writerOfFingerprintsForDebug != null)
		{
			try 
			{
				_writerOfFingerprintsForDebug.write("\n\t Frame " + _frame);
				_writerOfFingerprintsForDebug.write("\n");

			} catch (IOException e) 
			{

				e.printStackTrace();
			}
		}

		//1) has to convert the peaks to fingerprints by the logic of this fingerprinter - getFingerprintsFromPeaks
		Iterable<Integer> composed_fingerprint = PeakCombiner.combineTwoFrameSets(_firstFramePeaks, frame_peaks, _mode, this, _writerOfFingerprintsForDebug, _frame);
		
		assert composed_fingerprint != null :  "TwoPeaksFingerprinter >>matchFramePeaksToDatabase >> composed_fingerprint can't be null ";
		
		//updates the frame - old frame for the fp generation
		_firstFramePeaks = (ArrayList<Integer>) frame_peaks;
		_frame++;
		
		return composed_fingerprint; 
		 
		
		
		
	}

	@Override
	public int matchFramePeaksToDatabase(
			Iterable<Integer> sample_fingerprints,
			Map<Integer, HashMap<Integer, FingerprintMetaData>> fingerprint_repository,
			int frame)
	{
		assert sample_fingerprints != null :  "TwoPeaksFingerprinter >>matchFramePeaksToDatabase >> sample_fingerprints can't be null ";

		//if it's the first frame, one has to wait for the second one to start making fingerprints
		if(_firstFramePeaks.isEmpty())
		{
			_firstFramePeaks = (ArrayList<Integer>) sample_fingerprints;
			return Codes.NO_MATCH;
		}
		//1) has to convert the peaks to fingerprints by the logic of this fingerprinter - getFingerprintsFromPeaks
		Iterable<Integer> composed_fingerprint = getFingerprintsFromPeaks(sample_fingerprints);
		
		assert composed_fingerprint != null :  "TwoPeaksFingerprinter >>matchFramePeaksToDatabase >> composed_fingerprint can't be null ";
		
		//2) has to look for matches in the repository
		return _matcher.getMatch(composed_fingerprint, fingerprint_repository, frame);
	}

	@Override
	public Matcher getMatcher() {

		return _matcher;
	}



	public int makeFingerprint(int frequency1, int frequency2) 
	{
		
		int digitsInPeak2 = String.valueOf(frequency2).length();
		int digitsInDelimiter = String.valueOf(Codes.FINGERPRINT_DELIMITER).length();
		
		int fingerprint = (int) (frequency1 * Math.pow(10, digitsInDelimiter + digitsInPeak2) + 
						Codes.FINGERPRINT_DELIMITER * Math.pow(10,digitsInPeak2) + frequency2);
		
		//debug print
		if(_writerOfFingerprintsForDebug != null)
		{
			try {
				_writerOfFingerprintsForDebug.write(" Fingerprint = " + fingerprint);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return fingerprint;
	}




	@Override
	public void resetVariables() 
	{
		_frame = 0;
		_firstFramePeaks = new ArrayList<Integer>();
	}


	@Override
	public FileWriter getDebugWriter() {
		return _writerOfFingerprintsForDebug;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}
	
}
