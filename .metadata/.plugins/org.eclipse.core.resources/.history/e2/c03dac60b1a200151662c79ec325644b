package ReporterTools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import Utilities.Codes;

public class SimpleReporter implements Reporter{
	
	FileWriter _report_writer;
	int _number_of_songs, _number_of_samples,_test_number, _number_of_fingerprints;
	String _storage_type;
	Test _test;
	
	public SimpleReporter(int number_of_songs, int number_of_samples, String storage_type, int number_of_fingerprints) throws IOException 
	{
		
		checkReportFileExistance();
		_report_writer = new FileWriter(Codes.REPORTS_FOLDER + getReportFileRelativePath());
		_storage_type = storage_type;
		_number_of_samples = number_of_samples;
		_number_of_songs = number_of_songs;
		_test_number = 1;
		_number_of_fingerprints = number_of_fingerprints;
	}
	
	
	
	@Override
	public void writeHeader() throws IOException 
	{
		_report_writer.write("\t ***\tReport\t***\n\n" );
		writeParameterValues();
		
		_report_writer.write("\n\n\t * Number of songs: " + _number_of_songs + "\n");
		_report_writer.write("\t * Number of samples: " + _number_of_samples+ "\n");
		_report_writer.write("\t * Number of fingerprints: " + _number_of_fingerprints+ "\n");
		_report_writer.write("\t * Type of storage: " + _storage_type+ "\n");
		_report_writer.write("\t * Minimum number of peaks for identification: " + Codes.MIN_HITS_TO_RESULT + "\n\n" );

	//	writeParameterValues();
		
	}

	@Override
	public void checkReportFileExistance() 
	{
		File reports_folder = new File (Codes.REPORTS_FOLDER);
			reports_folder.mkdir();
	}

	@Override
	public void writeTestHeader(String fingerprinterType, String matcherType, double[] repository_info ) throws IOException 
	{
		_report_writer.write(">> Test #" + _test_number + "\n");
		//_report_writer.write("\t * Average number of frames per fingerprint: " + repository_info[Codes.INDEX_AVERAGE_FRAME_WITH_SAME_FINGERPRINT]+ "\n" );
		//_report_writer.write("\t * Average number of songs per fingerprint: " + repository_info[Codes.INDEX_AVERAGE_SONG_WITH_SAME_FINGERPRINT ]+  "\n\n");

		_test_number++;
		_test = new SingleTest();
		
	}



	@Override
	public String getReportFileRelativePath() 
	{

		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minuts = cal.get(Calendar.MINUTE);
		
		return "Report-" + String.valueOf(dayOfMonth) + "-" + 		
		String.valueOf(month) +  "-" + String.valueOf(year) + "-" +
		String.valueOf(hour) +"h" + String.valueOf(minuts) 
		+ "m.txt";
		
	}



	@Override
	public void startTestMatching(String sampleName) {
		try {
			_report_writer.write("\n\n> Matching attempt with sample: " + sampleName );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		_test.startMatching();
	}



	@Override
	public 	void endTestMatching(int result, String identificationResultMessage)
	{
		if(Codes.NO_MATCH != result)
			addCorrectMatch();
		try {
			_report_writer.write(identificationResultMessage);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
	}



	@Override
	public void finishTest() throws IOException {
		_test.endMatching();

		_report_writer.write("\n> Test Results:\n");
		_report_writer.write("\t * Total Time of test = " + TimeUnit.MILLISECONDS.toSeconds(_test.getTotalTimeOfTest())+ "\n");
		_report_writer.write("\t * Average Time of matching = " + TimeUnit.MILLISECONDS.toSeconds(_test.getAverageTimePerMatching())+ "\n");
		_report_writer.write("\t * Number of correct answers = " + _test.getCorrectMatches()+ "\n");
		_report_writer.write("\t * Number of INcorrect answers = " + (_test.getMatches()-_test.getCorrectMatches()) + "\n\n");


	}



	@Override
	public void addCorrectMatch() {
		_test.incrementCorrectResults();
		
	}



	@Override
	public void finishReport() throws IOException {
		_report_writer.flush();
		_report_writer.close();
		
	}



	@Override
	public void writeParameterValues() throws IOException {
		
		_report_writer.write("\n\n\t* Program Parameters *\n");
		_report_writer.write("\t* Frame Size: " + Codes.FRAME_SIZE+ "\n");
		_report_writer.write("\t* Sample Rate: " + Codes.SAMPLE_RATE+ "\n");
		
		_report_writer.write("\n\t* Ranges: [" );
		for (int rangeIndex = Codes.LOWER_LIMIT; rangeIndex < Codes.UPPER_LIMIT ; rangeIndex++)
			_report_writer.write( Codes.FREQUENCY_INTERVALS[rangeIndex] + "," );

		_report_writer.write("]\n\t* Min hits to show result: " + Codes.MIN_HITS_TO_RESULT+ "\n");
		_report_writer.write("\t* Min Hits/frames ratio: " + Codes.MIN_HIT_RATIO_TO_RESULT + "\n\n");
		
	}
	
	
	

}
