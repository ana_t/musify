package com.musify.ana.musify;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import Utilities.AudioLiveAnalysis;
import Utilities.AudioLiveFrameReader;
import Utilities.Codes;
import Utilities.FrameReader;
import Utilities.LiveAnalysis;

import static android.media.MediaRecorder.AudioSource.DEFAULT;

public class MainActivity  extends AppCompatActivity  {

    ImageButton _bSettings, _bIdentify;
    int _iClicks_on_identify = 0;
    String _sIP;
    long startTime;
    static final long duration = 20; //segundos
    InetAddress _serverAddr;
    Context _context ;

    short[] audio_buffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _bSettings = (ImageButton) findViewById(R.id.settings_button);
        _bIdentify = (ImageButton) findViewById(R.id.identify_button);
        //_audioLiveAnalysis = new AudioLiveAnalysis(_recRecorder);
        _context = this.getApplicationContext();




        Bundle extras = getIntent().getExtras();
        if (extras == null)
        {
            _sIP = null;
        }
        else {
            _sIP = extras.getString("ip");
            Log.e("Main-IP",_sIP);

            try {
                _serverAddr = InetAddress.getByName(_sIP);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

        }

        Toast identify_toast = Toast.makeText(getBaseContext(), "Click on Mic for identification", Toast.LENGTH_LONG);
        identify_toast.show();


        //listeners


        _bSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //passa para activity de Settings
                Intent toSettings = new Intent(_context, Settings.class);

                if (_sIP != null)
                    toSettings.putExtra("ip", _sIP);

                startActivity(toSettings);

            }
        });

        _bIdentify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(_sIP == null){
                Toast identify_toast = Toast.makeText(getBaseContext(),"First set IP on settings!", Toast.LENGTH_LONG);
                identify_toast.show();
                }


                else if(_bIdentify.isEnabled())
                {

                    try {
                        identify();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    _iClicks_on_identify++; // var auxiliar para determinar se e para parar gravação

                }
            }
        });


    }

    private void identify() throws IOException, InterruptedException {
        //não deixa carregar enquanto está a existir gravação
        _bIdentify.setEnabled(false);
        _bIdentify.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);

        connectToServer connection = new connectToServer();
        connection.start();

        //mostra mensagem
        Toast identify_toast = Toast.makeText(getBaseContext(),"Identification started!", Toast.LENGTH_LONG);
        identify_toast.show();

        Toast remaining_time_toast = Toast.makeText(getBaseContext(),"Wait for a maximum of " + (duration - TimeUnit.MILLISECONDS.toSeconds((SystemClock.elapsedRealtime() - startTime))) + " seconds", Toast.LENGTH_LONG);
        //Log.e("TIME", String.valueOf(TimeUnit.MILLISECONDS.toSeconds(SystemClock.elapsedRealtime() - startTime)));
        remaining_time_toast.show();

        connection.join();
        _bIdentify.getBackground().clearColorFilter();
        _bIdentify.setEnabled(true);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class connectToServer extends Thread{

        @Override
        public void run()
        {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            Socket _socket;
            try{
                Log.e("Conexão" , "Antes");
                //Ligação ao servidor

                 _socket = new Socket(_serverAddr, Codes.SERVER_PORT);
                Log.e("Null?" , _socket.toString());
                startTime = SystemClock.elapsedRealtime();
                //fazer comunicação com o servidor e fazer gravação

                //sets recorder properties!
                AudioRecord recRecorder;
                LiveAnalysis audioLiveAnalysis;
                FrameReader frLiveFrameReader;

                int bufferSize = AudioRecord.getMinBufferSize(Codes.SAMPLING_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
                recRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, Codes.SAMPLING_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT,  bufferSize * Codes.TIMES_FRAME_SIZE_BUFFER);
                frLiveFrameReader = new AudioLiveFrameReader(_socket);
                audioLiveAnalysis = new AudioLiveAnalysis(recRecorder, frLiveFrameReader);
                //String result = "No identification found...";
                //manda gravar e depois recebe resposta do server
                Log.e("Recording" , "Começo");

                audioLiveAnalysis.start();

                //enquanto não termina a análise de áudio
                while(!frLiveFrameReader.isDone())
                {
                    //Log.e("isDone?" , String.valueOf(frLiveFrameReader.isDone()));

                    //espera

                }
                audioLiveAnalysis.stop();
                Log.e("Recording", "Fim");

                Intent toResults = new Intent(_context,Results.class);
                toResults.putExtra("result", frLiveFrameReader.getMessage());
                startActivity(toResults);

            } catch (Exception e) {
                System.err.println("Problemas com o Host...");
                // System.exit(1);
            }





        }



        }

}
