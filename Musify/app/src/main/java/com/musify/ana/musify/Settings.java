package com.musify.ana.musify;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Settings extends AppCompatActivity {

    String _ip;
    EditText _etIP;
    Button _bSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        _etIP = (EditText) findViewById(R.id.ip_textField);
        _bSave = (Button) findViewById(R.id.save_button);

        final Context context = this.getApplicationContext();

        Bundle extras = this.getIntent().getExtras();

        if(extras != null)
            _etIP.setText(extras.getString("ip"));
        else
        _etIP.setText("192.168.1.69");//"192.168.1.96");
        //listener
        _bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toMainActivity = new Intent(context, MainActivity.class);
                Log.e("Settings-IP",_etIP.getText().toString());
                toMainActivity.putExtra("ip", _etIP.getText().toString());
                startActivity(toMainActivity);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
