package Utilities;

/**
 * Created by ana on 14/08/15.
 */
public interface Codes {

    static int FRAME_SIZE = 4096;
    static int TIMES_FRAME_SIZE_BUFFER = 10;
    static int SAMPLING_RATE = 44100;
    static String NO_MATCH = "-3";
    static int SERVER_PORT = 8000;
    static int MIN_RECORDING_SECONDS = 20;

    static int MAX_FRAMES_TO_ANALYZE =  100; //TODO: REPOR! (MIN_RECORDING_SECONDS * SAMPLING_RATE)+ 1;


}
