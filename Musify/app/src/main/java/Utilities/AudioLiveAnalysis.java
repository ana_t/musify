package Utilities;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.provider.SyncStateContract;
import android.provider.SyncStateContract.Constants;
import android.util.Log;

import java.io.IOException;

import static android.provider.SyncStateContract.Constants.*;

/**
 * Created by ana on 14/08/15.
 */
public class AudioLiveAnalysis implements Runnable, LiveAnalysis{


    private AudioRecord _recRecorder;
    private FrameReader _frFrameReader;
    private Thread _thread = null;


    public AudioLiveAnalysis( AudioRecord recRecorder, FrameReader frameReader)
    {
        _recRecorder = recRecorder;
        _frFrameReader = frameReader;
    }

    public void start()
    {
        _recRecorder.startRecording();
        _thread = new Thread(this);
        _thread.start();
    }

    public void stop()  {
        _recRecorder.stop();
        _recRecorder.release();
        _thread = null;
    }


    @Override
    public void run() {

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        //short[] audio_buffer = new short[Codes.FRAME_SIZE * Codes.TIMES_FRAME_SIZE_BUFFER];
        int frame =0;
        int bufferSize = AudioRecord.getMinBufferSize(Codes.SAMPLING_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        Log.e("MinBuffSize", String.valueOf(bufferSize));

        while(_recRecorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING)
        {
            Log.e("Recording", "Nova-Frame");
            try {

            short[] audio_buffer = new short[Codes.FRAME_SIZE];
            _recRecorder.read(audio_buffer, 0, Codes.FRAME_SIZE);
                Log.e("Li-Recorder", "true");

                _frFrameReader.processFrame(audio_buffer, frame);
                Log.e("Passei processFrame ", "true");

                frame++;

            } catch (IOException e) {
                e.printStackTrace();
            }
            //short[] tempBuf = new short[Codes.FRAME_SIZE];
            //audioRecorder.read(tempBuf, 0, tempBuf.length);
            //iAudioReceiver.capturedAudioReceived(tempBuf, false);

        }
    }
}
