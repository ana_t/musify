package Utilities;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by ana on 14/08/15.
 */
public interface FrameReader {



    void processFrame(short [] frame, int frame_number) throws IOException; //faz pre-processamento a frame, comunica com server  e recebe a resposta
    boolean isDone();
    String getMessage();

}
