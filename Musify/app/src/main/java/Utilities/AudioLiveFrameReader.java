package Utilities;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by ana on 14/08/15.
 */
public class AudioLiveFrameReader implements FrameReader {


    private boolean _bDone;
    private String _sMessage;
    private Socket _socket;
    private DataOutputStream _doutstream;
    private BufferedReader _buffInStream;
    private int _iAnalyzed_frames;

    public AudioLiveFrameReader(Socket socket) throws IOException {
        _bDone = false;
        _sMessage = "No match found...";
        _socket = socket;
        _iAnalyzed_frames = 0;
        createSocketStreams();

    }

    @Override
    public void processFrame(short[] frame, int frame_number ) throws IOException {

        //TODO:

    //processa frame -> fft e picos ou só fft?

        //para já vou mandar apenas o array - convert para double para compatibilidade com os restantes ficheiros
       // _doutstream.writeDouble(Codes.FRAME_SIZE); //manda tamanho do array
        Log.e("Pframe-envio tamanho", "true");

        _doutstream.writeInt( frame.length);

        for (int sample_pos  = 0 ; sample_pos < frame.length ; sample_pos ++)
        {

            _doutstream.writeDouble((double) frame[sample_pos]);
        }
        Log.e("Enviei todo o array", "true");

        String server_output = "";
        String parsed_server_output= "";
        Log.e("analisar resposta srv", "true");

        while ((parsed_server_output = _buffInStream.readLine()) != null)
        {
            //System.out.println ("Server: " + inputLine);
            //out.println(inputLine);
            server_output+=parsed_server_output;
        }
        Log.e("terminei resposta srv", "true");

        boolean success =  !server_output.equalsIgnoreCase(Codes.NO_MATCH);
        if(!success || frame_number == Codes.MAX_FRAMES_TO_ANALYZE )
        {
            _doutstream.writeDouble((double) 1);
            if(success)
                setMessage(server_output);
            _bDone = true;
        }
        else if(  frame_number == Codes.MAX_FRAMES_TO_ANALYZE )


        _iAnalyzed_frames++;
    }

    @Override
    public boolean isDone() {
        return _bDone;
    }

    @Override
    public String getMessage() {
        return _sMessage;
    }

    private void setMessage(String message)
    {
         _sMessage = message;
    }

    private void createSocketStreams() throws IOException {
        _doutstream = new DataOutputStream(_socket.getOutputStream());
        _buffInStream = new BufferedReader(
                new InputStreamReader( _socket.getInputStream()));

    }

}
