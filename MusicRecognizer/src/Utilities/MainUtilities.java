package Utilities;

import java.util.Calendar;

public  class MainUtilities 
{

	/*
	 * 
	 * Obtem um sufixo para cada ficheiro de análise com base na hora e data da mesma
	 * 
	 * */
	public static String getTimestampForFileName() 
	{

		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minuts = cal.get(Calendar.MINUTE);
		
		return String.valueOf(dayOfMonth) + "-" + 		
		String.valueOf(month) +  "-" + String.valueOf(year) + "-" +
		String.valueOf(hour) +"h" + String.valueOf(minuts) 
		+ "m";
		
	}
	
}
