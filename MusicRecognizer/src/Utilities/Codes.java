package Utilities;

public interface Codes {

	
	 static final int[] FREQUENCY_INTERVALS = new int[] { 40,80,120,180,300,400,500};//, 350,400,450, 500,550, 600, 650, 700,750, 800,850, 900,950, 1000};//{ 400,800, 1200, 1800,
		//3000, 4000,5000 };//{ 40, 80, 120,180, 300 };	
	
	 //program parameters
	 static final int DEBUG = 0;
	 static final int PRODUCTION = 1;
	 static final int PROGRAM_MODE = PRODUCTION;
	 static final double MIN_AMPLITUDE = 1.5;
	 static final int MIN_BIN_INTERVAL_TO_PEAK = 2;
	 static final int MIN_HITS_TO_RESULT = 50;
	 static final double MIN_HIT_RATIO_TO_RESULT = 0.40;
	 static final int MIN_PEAK_MATCHES_PER_FRAME = 2;// FREQUENCY_INTERVALS.length/2;

	
	
	  // windowing
	  static final int HANNING = 2;
	  static final int NO_WINDOW = 1;
	
	  //Audio Analysis parameters
	 static final int FRAME_SIZE = 1024*4;
	 static final int SAMPLE_RATE = 44100;
	 static final int BIN_VALUE = SAMPLE_RATE / FRAME_SIZE;
	 static final int HOP_RATIO = 1;

	 //Last /First
	 static final int LAST_FREQUENCY_TO_ANALYZE = 1000;
	 static final int FIRST_FREQUENCY_TO_ANALYZE = 0;
	 static final int LAST_BIN_TO_ANALYZE = LAST_FREQUENCY_TO_ANALYZE/BIN_VALUE +1 ;
	 static final int FIRST_BIN_TO_ANALYZE = FIRST_FREQUENCY_TO_ANALYZE/BIN_VALUE ;
	 static final int LAST_FFT_POS_TO_ANALYZE = LAST_BIN_TO_ANALYZE*2 ;
	 static final int FIRST_FFT_POS_TO_ANALYZE = FIRST_BIN_TO_ANALYZE*2 ;

//40-80, 80-120, 120-180, 180-300.
	 
	 //Ranges
	 static final int LOWER_LIMIT = 0;
	 static final int UPPER_LIMIT = FREQUENCY_INTERVALS.length-1;
	 static final int FIRST_INTERVAL = 0;
	 static final int SECOND_INTERVAL = 1;
	 static final int THIRD_INTERVAL = 2;
	 static final int FOURTH_INTERVAL = 3;
	 
	 // indexes
	 static final int INDEX_AVERAGE_FRAME_WITH_SAME_FINGERPRINT = 0;
	 static final int INDEX_AVERAGE_SONG_WITH_SAME_FINGERPRINT = 1;

	 
	 //sizes
	 static final int SIZE_ARRAY_REPOSITORY_REPORT_INFO = 2;

	 static final int FINGERPRINT_DELIMITER = 999;
	 static final int COMBINER_ALL_COMBINATIONS = 1;
	 static final int COMBINER_COMBINATIONS_BETWEEN_THE_TWO_BEST_FROM_EACH_FRAME = 2;
	 static final int COMBINER_COMBINATIONS_BETWEEN_THE_THREE_BEST_FROM_EACH_FRAME = 3;


	 //Data storage
	 static final String RAM = "Main Memory";
	 static final String DB = "Database";	
	 
	 //Paths 
	 static final String REPORTS_FOLDER = "Reports/";
	 static final String FINGERPRINTS_FOLDER = "Fingerprints/";
	 static final String RESULTS_FOLDER = "Results/";

	 static final String NO_MATCH_MESSAGE = "no_match";
	 static final String ORIGINAL_DATA_FOLDER = "OriginalData/";
	 static final String SAMPLE_DATA_FOLDER = "SampleData/";
	 static final String PEAKS_FOLDER = "Peaks/";

	 
	 //Outputs
	 static final int NO_MATCH = -3;
	 static final String SUCCESSFULL_MATCH = "Identificação feita com SUCESSO!";
	 static final String UNSUCCESSFULL_MATCH = "Identificação feita incorretamente...";
	static final int MIN_FRAME_NUMBER_TO_POSITIVE_RESULT = 54;
	static final int MIN_DIFERENCE_TO_ORIGINAL_FRAME_IF_NEGATIVE_START_FRAME = -10;

	 


	
	
}
