package Hashing;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

import Entities.FingerprintMetaData;
import Matchers.Matcher;

public interface Fingerprinter {

	//int makeFingerprint(int frequency);
	int makeFingerprint(int frequency1, int frequency2);
	//void insertHashes(int songID, int frame, HashMap<Integer,Integer> database);
	Iterable<Integer> getFingerprintsFromPeaks(Iterable<Integer> frame_peaks);
	int matchFramePeaksToDatabase(Iterable<Integer> sample_fingerprints,  Map<Integer, HashMap<Integer, FingerprintMetaData>> fingerprint_repository, int frame);
	Matcher getMatcher();
	void resetVariables();
	FileWriter getDebugWriter();
	void setFileWriter(FileWriter writer);
	String getType();

}
