package Hashing;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import Entities.FingerprintMetaData;
import Matchers.Matcher;
import Utilities.Codes;

public class NoHashing implements Fingerprinter{

	private Matcher _matcher;
	
	public NoHashing(Matcher matcher)
	{
		_matcher = matcher;
	}
	public int makeFingerprint(int frequency) {
		// TODO Auto-generated method stub
		return frequency;
	}

	public int makeFingerprint(int frequency1, int frequency2) {
		return frequency1*10000 + frequency2;
	}

	@Override
	public Iterable<Integer> getFingerprintsFromPeaks(
			Iterable<Integer> frame_peaks) {
		return frame_peaks;
	}

	@Override
	public int matchFramePeaksToDatabase(
			Iterable<Integer> sample_fingerprints, // neste caso são os picos
			Map<Integer, HashMap<Integer, FingerprintMetaData>> fingerprint_repository, int frame) 
	{

		//vai utilizar o matcher para fazer o matching
		
		
		//como já tem fingerprints, basta mesmo chamar o matcher para ver se estes existem no 
		//repositório e fazer o match devido - de acordo com o matcher.
		//basicamente, este recolhe os matches, e o matcher identifica quais deve levar a sério, etc
		//etc até ter-se uma hipótese
		
		
		return _matcher.getMatch(sample_fingerprints, fingerprint_repository, frame);
		
			}
	@Override
	public Matcher getMatcher() {

		return _matcher;
	}
	@Override
	public void resetVariables() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public FileWriter getDebugWriter() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setFileWriter(FileWriter writer) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

}
/*
private static final int FUZ_FACTOR = 2;

private static long hash(long p1, long p2, long p3, long p4) {

	return (p4 - (p4 % FUZ_FACTOR)) * 100000000 + (p3 - (p3 % FUZ_FACTOR))
			* 100000 + (p2 - (p2 % FUZ_FACTOR)) * 100
			+ (p1 - (p1 % FUZ_FACTOR));
}
*/