package Hashing;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeSet;

import Utilities.Codes;
import Entities.FingerprintMetaData;
import Matchers.Matcher;

public class NPointFingerprinter implements Fingerprinter{

	private Matcher _matcher;
	private int _number_of_points_of_fingerprint;
	public NPointFingerprinter(Matcher matcher, int number_of_points_of_fingerprint)
	{
		_matcher = matcher;
		_number_of_points_of_fingerprint = number_of_points_of_fingerprint;
	}
	/*
	Para fazer deste método dinâmico, vamos ter que receber não os picos de uma frame, 
	Mas de n frames, consoante o número de pontos usado para o fringerprint. Assume-se que
	o número de frames enviado = número de picos no fingerprint, ou seja, há um pico escolhido
	por frame em cada fingerprint.

	*/
	@Override
	public Iterable<Integer> getFingerprintsFromPeaks(
			Iterable<Integer> frame_peaks) {
		/*
		//M ranges x N frames/picos por frame 
		int total_of_fingerprints = (int) Math.pow(Codes.FREQUENCY_INTERVALS.length, _number_of_points_of_fingerprint);

		List<ArrayList<Integer>> peaksPerFrame = new ArrayList<ArrayList<Integer>>();
		List <Integer> resultantFingerprints = new ArrayList<Integer>();
		GeneratePermutations(peaksPerFrame, resultantFingerprints, int depth, String current)
		
		
		
		
		// TODO Auto-generated method stub*/
		return null;
	}
	void GeneratePermutations(List<List<String>> Lists, List<String> result, int depth, String current)
	{
	    if(depth == Lists.size())
	    {
	       result.add(current);
	       return;
	     }

	    for(int i = 0; i < Lists.get(depth).size(); ++i)
	    {
	        GeneratePermutations(Lists, result, depth + 1, current + Lists.get(depth).get(i));
	    }
	}
	@Override
	public int matchFramePeaksToDatabase(
			Iterable<Integer> sample_fingerprints,
			Map<Integer, HashMap<Integer, FingerprintMetaData>> fingerprint_repository,
			int frame) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Matcher getMatcher() {
		return _matcher;
	}
	@Override
	public int makeFingerprint(int frequency1, int frequency2) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void resetVariables() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public FileWriter getDebugWriter() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setFileWriter(FileWriter writer) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}
	
}
