package Hashing;

import java.util.TreeSet;

public interface FingerprintData {

	int getSongID();
	TreeSet<Integer> getFrames();
	void insertFrame(int frame);
	String getSongName();
	
}
