package ReporterTools;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import Utilities.Codes;

public class SimpleReporter implements Reporter{
	
	FileWriter _report_writer;
	int _number_of_songs, _number_of_samples,_test_number, _number_of_fingerprints,
	_number_of_samples_from_phone, _number_of_samples_from_pc,
	_number_of_samples_from_phone_correct,_number_of_samples_from_pc_correct ;
	String _storage_type;
	Test _test;
	
	public SimpleReporter(int number_of_songs, int number_of_samples, String storage_type, int number_of_fingerprints) throws IOException 
	{
		
		checkReportFileExistance();
		_report_writer = new FileWriter(Codes.REPORTS_FOLDER + getReportFileRelativePath());
		_storage_type = storage_type;
		_number_of_samples = number_of_samples;
		_number_of_songs = number_of_songs;
		_test_number = 1;
		_number_of_fingerprints = number_of_fingerprints;
	}
	
	
	
	@Override
	public void writeHeader() throws IOException 
	{
		_report_writer.write("\t ***\tReport\t***\n\n" );
		writeParameterValues();
		
		_report_writer.write("\n\n\t * Number of songs: " + _number_of_songs + "\n");
		_report_writer.write("\t * Number of samples: " + _number_of_samples+ "\n");
		_report_writer.write("\t * Number of fingerprints: " + _number_of_fingerprints+ "\n");
		_report_writer.write("\t * Type of storage: " + _storage_type+ "\n");
		_report_writer.write("\t * Minimum number of peaks for identification: " + Codes.MIN_HITS_TO_RESULT + "\n\n" );
		_report_writer.flush();

	//	writeParameterValues();
		
	}

	@Override
	public void checkReportFileExistance() 
	{
		File reports_folder = new File (Codes.REPORTS_FOLDER);
			reports_folder.mkdir();
	}

	@Override
	public void writeTestHeader(String fingerprinterType, String matcherType, String peakFinderType, double[] repository_info ) throws IOException 
	{
		_report_writer.write(">> Test #" + _test_number + "\n");
		//_report_writer.write("\t * Average number of frames per fingerprint: " + repository_info[Codes.INDEX_AVERAGE_FRAME_WITH_SAME_FINGERPRINT]+ "\n" );
		//_report_writer.write("\t * Average number of songs per fingerprint: " + repository_info[Codes.INDEX_AVERAGE_SONG_WITH_SAME_FINGERPRINT ]+  "\n\n");
		_report_writer.write("> Peak Finder = " + peakFinderType + "\n");
		_report_writer.write("> Fingerprinter = " + fingerprinterType + "\n");
		_report_writer.write("> Matcher = " + matcherType + "\n\n");
		_report_writer.flush();

		_test_number++;
		_test = new SingleTest();
		
	}



	@Override
	public String getReportFileRelativePath() 
	{

		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minuts = cal.get(Calendar.MINUTE);
		
		return "Report-" + String.valueOf(dayOfMonth) + "-" + 		
		String.valueOf(month) +  "-" + String.valueOf(year) + "-" +
		String.valueOf(hour) +"h" + String.valueOf(minuts) 
		+ "m.txt";
		
	}



	@Override
	public void startTestMatching(String sampleName) {
		try {
			_report_writer.write("\n\n> Matching attempt with sample: " + sampleName + "\n >>" );
			_report_writer.flush();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		_test.startMatching();
	}



	@Override
	public 	void endTestMatching(String resultFromDB, String sampleName) throws IOException
	{
		compareResultWithCorrectAnswer(sampleName,resultFromDB) ;

		_test.endMatching();
	}

	@Override
	public void finishTest() throws IOException {
		_test.endMatching();
		_test.endTest();

		System.out.println("Test Duration = " + _test.getTotalTimeOfTest() + " em segundos:  "+ TimeUnit.MILLISECONDS.toSeconds(_test.getTotalTimeOfTest()));

		_report_writer.write("\n> Test Results:\n");
		_report_writer.write("\t * Total Time of test = " + TimeUnit.MILLISECONDS.toSeconds(_test.getTotalTimeOfTest())+ "\n");
		_report_writer.write("\t * Average Time of matching = " + TimeUnit.MILLISECONDS.toSeconds((long) _test.getAverageTimePerMatching())+ "\n");
		_report_writer.write("\t * Number of correct answers = " + _test.getCorrectMatches()+ "\n");
	
		_report_writer.write("\t * Number of INcorrect answers = " + (_test.getMatches()-_test.getCorrectMatches()) + "\n");
		_report_writer.write("\t * Number of correct answers of Phone Samples = " + _number_of_samples_from_phone_correct +"/"+ _number_of_samples_from_phone + "\n");
		_report_writer.write("\t * Number of correct answers of PC Samples= " + _number_of_samples_from_pc_correct +"/"+ _number_of_samples_from_pc + "\n\n");
	}


	@Override
	public void addCorrectMatch() {
		_test.incrementCorrectResults();
		
	}



	@Override
	public void finishReport() throws IOException {
		_report_writer.flush();
		_report_writer.close();
		
	}



	@Override
	public void writeParameterValues() throws IOException {
		
		_report_writer.write("\n\n\t* Program Parameters *\n");
		_report_writer.write("\t* Frame Size: " + Codes.FRAME_SIZE+ "\n");
		_report_writer.write("\t* Sample Rate: " + Codes.SAMPLE_RATE+ "\n");
		
		_report_writer.write("\t* Ranges: [" );
		for (int rangeIndex = Codes.LOWER_LIMIT; rangeIndex < Codes.UPPER_LIMIT ; rangeIndex++)
			_report_writer.write( Codes.FREQUENCY_INTERVALS[rangeIndex] + "," );

		_report_writer.write("]\n\t* Min hits to show result: " + Codes.MIN_HITS_TO_RESULT+ "\n");
		_report_writer.write("\t* Min Hits/frames ratio: " + Codes.MIN_HIT_RATIO_TO_RESULT + "\n\n");
		_report_writer.write("\t* Min Peaks Per Frame for Match: " + Codes.MIN_PEAK_MATCHES_PER_FRAME + "\n\n");
		_report_writer.flush();
	}



	@Override
	public void compareResultWithCorrectAnswer(String sampleName,
			String resultFromDatabase) throws IOException
	{
		//const
		String NO_MATCH_MADE = "";
		String DELIMITER = "-";
		String NO_ORIGINAL_IN_DB = "X";
		String HAS_ORIGINAL_IN_DB = "Y";
		String PHONE_RECORDING = "tlm";
		String PC_RECORDING = "pc";

		
		//data
		String[] sampleMetadata= sampleName.split(Pattern.quote(DELIMITER));
		int indexOfAvailabilityOfOriginalSongInDB = sampleMetadata.length-1;
		int indexOfTypeOfRecording = indexOfAvailabilityOfOriginalSongInDB-1;

		//takes out the extension part
		String sampleHasOriginalInDatabase = sampleMetadata[indexOfAvailabilityOfOriginalSongInDB].split(Pattern.quote("."))[0];
		String basicNameWithoutMetadata = sampleName.split(Pattern.quote("."))[0];
		
		String devicedUsedForRecording = sampleMetadata[indexOfTypeOfRecording];
		int lastIndexOfBasicName = basicNameWithoutMetadata.length() - devicedUsedForRecording.length() - 2 - sampleHasOriginalInDatabase.length();
		//- (2hífens- tamanho Y/X e tamanho device usado)
		basicNameWithoutMetadata =  basicNameWithoutMetadata.substring(0, lastIndexOfBasicName);
		int level_of_noise  = Character.getNumericValue(devicedUsedForRecording.charAt(devicedUsedForRecording.length()-1));
		devicedUsedForRecording = devicedUsedForRecording.substring(0, devicedUsedForRecording.length()-1);
		String resultFromDBWithoutExtension = "";
		if(resultFromDatabase.split(Pattern.quote(".")).length != 0)
			resultFromDBWithoutExtension = resultFromDatabase.split(Pattern.quote("."))[0];
		
		if(devicedUsedForRecording.equals(PHONE_RECORDING))
			_number_of_samples_from_phone++;
		else if(devicedUsedForRecording.equals(PC_RECORDING))
			_number_of_samples_from_pc++;

 
		//song was not identified by program, but one must check if the original was in the DB 
		if(	resultFromDBWithoutExtension.equals(NO_MATCH_MADE))
		{
			if( sampleHasOriginalInDatabase.equals(NO_ORIGINAL_IN_DB))
			{
				addCorrectMatch();
				if(devicedUsedForRecording.equals(PHONE_RECORDING))
					_number_of_samples_from_phone_correct++;
				else if(devicedUsedForRecording.equals(PC_RECORDING))
					_number_of_samples_from_pc_correct++;
				
				_report_writer.write("Correct Result! Sample " + sampleName + ", a recording made with " + devicedUsedForRecording + "\n");
				_report_writer.flush();

				return;

			}
			_report_writer.write("Wrong Result... Sample " + sampleName + ", a recording made with " + devicedUsedForRecording + "\n");
			_report_writer.flush();

				//else there was a wrong result and nothing has to be made
				return;
		}
		else // a match was made
		{
			if( sampleHasOriginalInDatabase.equals(HAS_ORIGINAL_IN_DB))
			{
				//result is correct!
				if(resultFromDBWithoutExtension.equals(basicNameWithoutMetadata))
				{
					addCorrectMatch();
					if(devicedUsedForRecording.equals(PHONE_RECORDING))
						_number_of_samples_from_phone_correct++;
					else if(devicedUsedForRecording.equals(PC_RECORDING))
						_number_of_samples_from_pc_correct++;
					_report_writer.write("Correct Result! Original :  " + resultFromDatabase + " and Sample " + sampleName + ", a recording made with " + devicedUsedForRecording + "\n");
					_report_writer.flush();

					return;
				}
				
			}
			
			_report_writer.write("Wrong Result... Sample " + sampleName + ", a recording made with " + devicedUsedForRecording + "\n");
			_report_writer.flush();

		}		
	}

}
