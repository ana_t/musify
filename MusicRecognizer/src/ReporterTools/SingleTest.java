package ReporterTools;


public class SingleTest implements Test
{
	
	private int _matching_processes = 0;
	private int _correct_matches = 0;
	private long _temporary_start_time = 0;
	private long _sum_matching_times = 0;
	private long _start_matching_time;
	private long _end_matching_time;

	
	public SingleTest()
	{
		_start_matching_time = System.currentTimeMillis();
		System.out.println("Start test @ "+ _start_matching_time);
		
	}
	
	
	@Override
	public void incrementMatchingProcesses() {

		_matching_processes++;		
	}

	@Override
	public void incrementCorrectResults() {
		_correct_matches++;
	}



	@Override
	public void startMatching() {
		incrementMatchingProcesses();
		_temporary_start_time = System.currentTimeMillis();
		
	}

	@Override
	public void endMatching() {
		_sum_matching_times+=( System.currentTimeMillis() - _temporary_start_time);
	}

	@Override
	public double getAverageTimePerMatching() {

		return (double)_sum_matching_times/(double)_matching_processes;
	}

	@Override
	public double percentageOfCorrectHits() {

		return (double)_correct_matches/(double)_matching_processes;
	}

	@Override
	public int getCorrectMatches() {

		return _correct_matches;
	}

	@Override
	public int getMatches() {

		return _matching_processes;
	}

	@Override
	public long getTotalTimeOfTest() {

		return _end_matching_time - _start_matching_time;
	}

	@Override
	public void endTest() {
		System.out.println("End test @ "+_end_matching_time);

		_end_matching_time = System.currentTimeMillis();
	}

}
