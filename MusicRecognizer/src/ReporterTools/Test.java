package ReporterTools;

public interface Test {
	
	void incrementMatchingProcesses();
	void incrementCorrectResults();
	void startMatching();
	void endMatching();
	void endTest();
	double getAverageTimePerMatching();
	double percentageOfCorrectHits();
	int getCorrectMatches();
	int getMatches();
	long getTotalTimeOfTest();
	

}
