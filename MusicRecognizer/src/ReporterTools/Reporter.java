package ReporterTools;

import java.io.IOException;

import Repository.Repository;

public interface Reporter {

	void writeHeader() throws IOException;
	void checkReportFileExistance();
	void writeTestHeader(String fingerprinterType, String matcherType, String peakFinderType, double[] repository_info ) throws IOException; 
	String getReportFileRelativePath();
	void startTestMatching(String sampleName);
	void endTestMatching(String resultFromDB, String sampleName) throws IOException;
	void finishTest() throws IOException;
	void addCorrectMatch();
	void finishReport() throws IOException;
	void writeParameterValues() throws IOException;
	void compareResultWithCorrectAnswer(String sampleName, String resultFromDatabase) throws IOException;
	
}
