package AudioTools.Readers;

public interface Reader {
	
	double[] readFileD(String path);
	short[] readFileS(String path);

}
