package AudioTools.PeakFinder;

import java.util.ArrayList;

public interface PeakFinder {

	ArrayList<Integer> findLocalPeaks(double[] fft);
	String getType();
}
