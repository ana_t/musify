package AudioTools.PeakFinder;

import java.util.ArrayList;

import AudioTools.BasicAudioFunctions;
import Utilities.Codes;

public class RangesPeakFinder implements PeakFinder{

	
	@Override
	public ArrayList<Integer> findLocalPeaks(double[] fft) {
		
		//picos locais
		ArrayList<Integer> localPeaks = new ArrayList<Integer>();

		//variáveis aux
		int div = Codes.FREQUENCY_INTERVALS[Codes.LOWER_LIMIT]/ Codes.BIN_VALUE;
		int remainer = Codes.FREQUENCY_INTERVALS[Codes.LOWER_LIMIT]% Codes.BIN_VALUE;
		//calcula o bin do primeiro range
		int bin = (remainer==0) ? div:(div+1);
		
		int range = 1;
		int upper_frequency = Codes.FREQUENCY_INTERVALS[range];
		int fft_pos;
		double max_amplitude = 0;
		int max_amplitude_bin_in_range = bin;

		while(true)
		{
			fft_pos = bin*2;
			int frequency = bin * Codes.BIN_VALUE;
			double amplitude = BasicAudioFunctions.getAmplitude(fft[fft_pos],fft[fft_pos + 1]);

			//caso ja esteja no proximo range
			if(frequency >= upper_frequency)
			{
				//guarda o maior da range anterior, e faz reset às variáveis auxiliares
				if(max_amplitude > Codes.MIN_AMPLITUDE)
					localPeaks.add(max_amplitude_bin_in_range * Codes.BIN_VALUE);
				max_amplitude_bin_in_range = bin;
				max_amplitude = amplitude;
				
				if(++range > Codes.UPPER_LIMIT)
					break;
				//range++;
				upper_frequency = Codes.FREQUENCY_INTERVALS[range];
				continue;
			}
			
			
			if(amplitude > max_amplitude)
			{
				max_amplitude = amplitude;
				max_amplitude_bin_in_range = bin;
				
			}
			//caso contrário segue para o bin seguinte
			
			
			
			bin++;
		}
		
		
		

		return localPeaks;
	}

	// Encontra onde está a frequencia de acordo com os limites definidos
	private static int getFrequencyIntervalIndex(int frequency) {
		int interval_lower_limit = 1;
		while (Codes.FREQUENCY_INTERVALS[interval_lower_limit] < frequency)
			interval_lower_limit++;
		return interval_lower_limit - 1;
	}

	@Override
	public String getType() 
	{
		return this.getClass().getSimpleName();
	}
}
