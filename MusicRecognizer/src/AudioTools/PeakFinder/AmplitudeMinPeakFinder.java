package AudioTools.PeakFinder;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.TreeSet;

import AudioTools.BasicAudioFunctions;
import Utilities.Codes;

public class AmplitudeMinPeakFinder implements PeakFinder{

	public ArrayList<Integer> findLocalPeaks(double[] fft) {
		ArrayList<Integer> localPeaks = new ArrayList<Integer>();
		
		int bin = Codes.MIN_BIN_INTERVAL_TO_PEAK ;
		int pos_in_fft = bin*2;
		//apenas a primeira metade do fft é aproveitavel , e como o fft tem tamanho FRAME_SIZE *2 por causa da representação
		//de complexos faz-se bin < FRAME_SIZE
		
		while(bin < Codes.LAST_BIN_TO_ANALYZE)
		{
			//System.out.println("Bin =" + bin + " pos="+ pos_in_fft);
			//ignoro os últimos 2 bins porque as altas frequências não serão importantes para o efeito
			if( pos_in_fft+4 >= Codes.FRAME_SIZE*2){
				break;
			}
			
			double max_amplitude = 0;
			int max_amplitude_bin = 0;
			//verifica qual o maior valor naquele intervalo
			for (int neighboor = - Codes.MIN_BIN_INTERVAL_TO_PEAK;  neighboor <  Codes.MIN_BIN_INTERVAL_TO_PEAK +1 ; neighboor++)
			{
			//	double real = fft[pos_in_fft + neighboor*2];
			//	double img = fft[pos_in_fft + neighboor*2 + 1];
				double amplitude = BasicAudioFunctions.getAmplitude(fft[pos_in_fft + neighboor*2],fft[pos_in_fft + neighboor*2 + 1]);
				if(amplitude > max_amplitude)
				{
					max_amplitude = amplitude;
					max_amplitude_bin = bin + neighboor;

				}
			}
			//se este bin é onde está o pico e se este é maior que 10
			if(max_amplitude_bin == bin && max_amplitude > Codes.MIN_AMPLITUDE)
			{
				int frequency = bin * Codes.BIN_VALUE;
				//System.out.println("Adicionei " + frequency + " à lista de pontos :D");
				localPeaks.add(frequency);
				//System.out.println("Adicionei pico!");
				
				bin +=  Codes.MIN_BIN_INTERVAL_TO_PEAK +1;
				pos_in_fft += ( Codes.MIN_BIN_INTERVAL_TO_PEAK +1)*2;
				continue;

			}
			
			//caso contrário salta para o maior bin, caso este seja a seguir do bin actual
			else if(max_amplitude_bin > bin )
			{
				pos_in_fft+= 2* (max_amplitude_bin - bin);
				bin+= (max_amplitude_bin - bin);
			}
			//como nao se guarda qual o bin mais intenso depois do actual, passa-se para o seguinte
			else
			{
				pos_in_fft+=2; //avança para o próximo número real i.e. do próximo bin
				bin++;

			}

		}
		return localPeaks;
		
	}

	@Override
	public String getType() 
	{
		return this.getClass().getSimpleName();
	}
}
