package AudioTools.PeakFinder;

import java.util.ArrayList;

import Utilities.Codes;
import AudioTools.BasicAudioFunctions;;
public class SimplePeakFinder implements PeakFinder{

	public ArrayList<Integer> findLocalPeaks(double[] fft) {
		
		ArrayList<Integer> localPeaks = new ArrayList<Integer>();
		
		int bin = Codes.MIN_BIN_INTERVAL_TO_PEAK ;
		int pos_in_fft = (Codes.MIN_BIN_INTERVAL_TO_PEAK )*2;
		//apenas a primeira metade do fft é aproveitavel , e como o fft tem tamanho FRAME_SIZE *2 por causa da representação
		//de complexos faz-se bin < FRAME_SIZE
		
		while(bin < Codes.LAST_BIN_TO_ANALYZE)
		{
			//System.out.println("Bin =" + bin + " pos="+ pos_in_fft);
			//ignoro os últimos 2 bins porque as altas frequências não serão importantes para o efeito
			if( pos_in_fft+4 >= Codes.FRAME_SIZE*2){
				break;
			}
			
			double max_amplitude = 0;
			int max_amplitude_bin = 0;
			//verifica qual o maior valor naquele intervalo
			for (int neighboor = - Codes.MIN_BIN_INTERVAL_TO_PEAK;  neighboor <  Codes.MIN_BIN_INTERVAL_TO_PEAK +1 ; neighboor++)
			{
			//	double real = fft[pos_in_fft + neighboor*2];
			//	double img = fft[pos_in_fft + neighboor*2 + 1];
				double amplitude = BasicAudioFunctions.getAmplitude(fft[pos_in_fft + neighboor*2],fft[pos_in_fft + neighboor*2 + 1]);
				if(amplitude > max_amplitude)
				{
					max_amplitude = amplitude;
					max_amplitude_bin = bin + neighboor;

				}
			}
			//se este bin é onde está o pico
			if(max_amplitude_bin == bin)
			{
				int frequency = bin * Codes.BIN_VALUE;
				//System.out.println("Adicionei " + frequency + " à lista de pontos :D");
				if(max_amplitude >=Codes.MIN_AMPLITUDE) //caso supere a amplitude min
					localPeaks.add(frequency);
				//System.out.println("Adicionei pico!");
				
				bin +=  Codes.MIN_BIN_INTERVAL_TO_PEAK +1;
				pos_in_fft += ( Codes.MIN_BIN_INTERVAL_TO_PEAK +1)*2;
				continue;

			}
			
			//caso contrário salta para o maior bin, caso este seja a seguir do bin actual
			else if(max_amplitude_bin > bin )
			{
				pos_in_fft+= 2* (max_amplitude_bin - bin);
				bin+= (max_amplitude_bin - bin);
			}
			//como nao se guarda qual o bin mais intenso depois do actual, passa-se para o seguinte
			else
			{
				pos_in_fft+=2; //avança para o próximo número real i.e. do próximo bin
				bin++;

			}

		}
		return localPeaks;
		/*
		for (int frame = 0; frame < frames_frequencies_and_amplitudes.length; frame++) 
		{
			// * 2 porque cada valor complexto ocupa 2 posições, uma para a
			// parte real e outra para a imaginária
			int frequency_pos = first_frequency_pos;
			//System.out.println("Bin Value = " + BIN_VALUE   + " Primeira Freq = " + frequency_pos/2 * BIN_VALUE + " Posição da primeira Freq = " + frequency_pos + " Posição da última Freq = " + last_frequency_pos + " última Freq  = " + (last_frequency_pos/2)*BIN_VALUE );
			 while( frequency_pos < last_frequency_pos) 
			 {
				// Obter a amplitude para a frequencia frequency para a
				// frame frame
				int frequency = frequency_pos/2 * BIN_VALUE;
				double amplitude = 10 * Math
						.log10(Math
								.sqrt(frames_frequencies_and_amplitudes[frame][frequency_pos]
										* frames_frequencies_and_amplitudes[frame][frequency_pos]
										+ frames_frequencies_and_amplitudes[frame][frequency_pos + 1]
										* frames_frequencies_and_amplitudes[frame][frequency_pos + 1]) + 1);

				// Descobre-se em que intervalo a frequencia se encontra
				int interval_index = getFrequencyIntervalIndex(frequency);
				// System.out.println("[FRAME "+ frame + "] Index: " +
				// interval_index + " frequency: "+ frequency +
				// " amplitude: "+ amplitude );
				writer.write("[FRAME " + frame + "] Index: "
						+ interval_index + " frequency: " + frequency
						+ " amplitude: " + amplitude + "\n");
				// Save the highest magnitude and corresponding frequency:
				if (amplitude > bestAmplitudesPerFrameAndFrequency[frame][interval_index] && (int) amplitude > MIN_AMPLITUDE) {
					frequenciesWithHigherAmplitudePerFrameAndInterval[frame][interval_index] = frequency;
					bestAmplitudesPerFrameAndFrequency[frame][interval_index] = amplitude;
				}
				frequency_pos += 2;
			}

		
	} 
*/
		
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

}
