package AudioTools.Windows;

public class Blackman implements Window{
	
	@Override
	public double[] getCoefficientsForWindow(int frame_size) {

        // generate nSamples window function values
        // for index values 0 .. nSamples - 1
        int last_usefull_info_in_frame = frame_size / 2;
        double ratio;
        double pi = Math.PI;
        double[] coefficients = new double[frame_size];


        ratio = pi / last_usefull_info_in_frame;
        for (int n = -last_usefull_info_in_frame; n < last_usefull_info_in_frame; n++)
        	coefficients[last_usefull_info_in_frame + n] = 0.42f + 0.5f * Math.cos(ratio) + 0.08f
                                * Math.cos(2 * n * ratio);
		
		return coefficients;
	}


}
