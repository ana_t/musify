package AudioTools.Windows;


public class Hanning implements Window{

	@Override
	public double[] getCoefficientsForWindow(int frame_size) {

        // generate nSamples window function values
        // for index values 0 .. nSamples - 1
        int last_usefull_info_in_frame = frame_size / 2;
        double ratio;
        double pi = Math.PI;
        double[] coefficients = new double[frame_size];

        ratio = pi / (last_usefull_info_in_frame + 1);
        for (int n = -last_usefull_info_in_frame; n < last_usefull_info_in_frame; n++)
        	coefficients[last_usefull_info_in_frame + n] = 0.5f + 0.5f * Math.cos(n * ratio);

		
		return coefficients;
	}
/*
    public double[] generate(int nSamples) {
        // generate nSamples window function values
        // for index values 0 .. nSamples - 1
        int m = nSamples / 2;
        double r;
        double pi = Math.PI;
        double[] w = new double[nSamples];
        switch (windowType) {
        case BARTLETT: // Bartlett (triangular) window
                for (int n = 0; n < nSamples; n++)
                        w[n] = 1.0f - Math.abs(n - m) / m;
                break;
        case HANNING: // Hanning window
                r = pi / (m + 1);
                for (int n = -m; n < m; n++)
                        w[m + n] = 0.5f + 0.5f * Math.cos(n * r);
                break;
        case HAMMING: // Hamming window
                r = pi / m;
                for (int n = -m; n < m; n++)
                        w[m + n] = 0.54f + 0.46f * Math.cos(n * r);
                break;
        case BLACKMAN: // Blackman window
                r = pi / m;
                for (int n = -m; n < m; n++)
                        w[m + n] = 0.42f + 0.5f * Math.cos(n * r) + 0.08f
                                        * Math.cos(2 * n * r);
                break;
        default: // Rectangular window function
                for (int n = 0; n < nSamples; n++)
                        w[n] = 1.0f;
        }
        return w;
}
}


	
	*/
	
	
}
