package AudioTools;

import Utilities.Codes;

public class BasicAudioFunctions implements AudioFunctions {

	public static double getAmplitude(double real, double imaginary){
		
		//double amplitude = //10 * Math
			//	.log10(Math
				Math.log10(Math.sqrt(real
								* real
								+ imaginary
								* imaginary)); //);
		
		double amplitude = Math.log(Math.sqrt(real
				* real
				+ imaginary
				* imaginary) + 1);		
		if(amplitude < 0)
		{
			System.out.println("real = " + real + "img =" + imaginary +  " amplitude = " + amplitude );
		}
		
		return amplitude;
		
		
	}
	

	private static double[][] basic_low_pass_filter(double frames_frequencies_and_amplitudes[][])
	{
		
		System.out.println("Vou usar a média");

		for (int frame = 0; frame < frames_frequencies_and_amplitudes.length; frame++) {
			int frequency_pos = Codes.FIRST_FFT_POS_TO_ANALYZE;
			//System.out.println("Bin Value = " + BIN_VALUE   + " Primeira Freq = " + frequency_pos/2 * BIN_VALUE + " Posição da primeira Freq = " + frequency_pos + " Posição da última Freq = " + last_frequency_pos + " última Freq  = " + (last_frequency_pos/2)*BIN_VALUE );
			int last_frequency_pos=Codes.LAST_FFT_POS_TO_ANALYZE;
			 while( frequency_pos < last_frequency_pos) 
			 {
				int frequency = frequency_pos/2 * Codes.BIN_VALUE;
				int bin_number = frequency_pos/2;

				int total = 5;
				double sum = Math
						.log(Math
								.sqrt(frames_frequencies_and_amplitudes[frame][frequency_pos]
										* frames_frequencies_and_amplitudes[frame][frequency_pos]
										+ frames_frequencies_and_amplitudes[frame][frequency_pos + 1]
										* frames_frequencies_and_amplitudes[frame][frequency_pos + 1]) + 1);
				double x1 = 0, x2 = 0, x3 = 0, x4 = 0;
				int aux_freq = -4;
				if (frequency_pos + aux_freq < 0) {
					total--;

				} else {
					aux_freq += frequency_pos;
					x1 = Math
							.log(Math
									.sqrt(frames_frequencies_and_amplitudes[frame][aux_freq]
											* frames_frequencies_and_amplitudes[frame][aux_freq]
											+ frames_frequencies_and_amplitudes[frame][aux_freq + 1]
											* frames_frequencies_and_amplitudes[frame][aux_freq + 1]) + 1);

				}
				aux_freq = -2;
				if (frequency_pos + aux_freq < 0)
					total--;
				else {
					aux_freq += frequency_pos;
					x2 = Math
							.log(Math
									.sqrt(frames_frequencies_and_amplitudes[frame][aux_freq]
											* frames_frequencies_and_amplitudes[frame][aux_freq]
											+ frames_frequencies_and_amplitudes[frame][aux_freq + 1]
											* frames_frequencies_and_amplitudes[frame][aux_freq + 1]) + 1);
				}
				aux_freq = 2;
				if (frequency_pos + aux_freq > Codes.FRAME_SIZE)
					total--;
				else {
					aux_freq += frequency_pos;
					x3 = Math
							.log(Math
									.sqrt(frames_frequencies_and_amplitudes[frame][aux_freq]
											* frames_frequencies_and_amplitudes[frame][aux_freq]
											+ frames_frequencies_and_amplitudes[frame][aux_freq + 1]
											* frames_frequencies_and_amplitudes[frame][aux_freq + 1]) + 1);
				}
				aux_freq = 4;
				if (frequency_pos + aux_freq > Codes.FRAME_SIZE)
					total--;
				else {
					aux_freq += frequency_pos;
					x4 = Math
							.log(Math
									.sqrt(frames_frequencies_and_amplitudes[frame][aux_freq]
											* frames_frequencies_and_amplitudes[frame][aux_freq]
											+ frames_frequencies_and_amplitudes[frame][aux_freq + 1]
											* frames_frequencies_and_amplitudes[frame][aux_freq + 1]) + 1);

				}
				sum += x1 + x2 + x3 + x4;

				frames_frequencies_and_amplitudes[frame][frequency_pos] = sum/total;
				frequency_pos += 2;

			}
		}
		return frames_frequencies_and_amplitudes;

	}

}
