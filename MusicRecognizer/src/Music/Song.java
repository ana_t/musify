package Music;


public class Song implements Music{
	
	private String _name;
	private String _artist; //mudar para múltiplos artistas
	private String _album;
	private int _song_id;
	private int _year;
	private String _path;

	//construtor preliminar apenas para testar o programa
	public Song(String path)
	{
		_path = path;
		String[] path_splitted = path.split("/");
		_name = path_splitted[path_splitted.length-1];
//		_song_id = songID;
	}

	@Override
	public int getSongID() {

		return _song_id;
	}

	@Override
	public String getName() {

		return _name;
	}

	@Override
	public String getArtist() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getYear() {

		return _year;
	}

	@Override
	public String getAlbum() {

		return _album;
	}

	@Override
	public String getPath() {

		return _path;
	}

	@Override
	public void setSongID(int songID) {
		_song_id = songID;
		
	}

}
