package Music;

public interface Music {

	int getSongID();
	String getName();
	String getArtist();
	int getYear();
	String getAlbum();
	String getPath();
	void setSongID(int songID);
	
}
