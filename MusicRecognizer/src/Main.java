//java imports
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Random;











//AudioTools
import AudioTools.PeakFinder.AmplitudeMinPeakFinder;
import AudioTools.PeakFinder.PeakFinder;
import AudioTools.PeakFinder.RangesPeakFinder;
import AudioTools.PeakFinder.RangesPeakFinderWithFilter;
import AudioTools.PeakFinder.SimplePeakFinder;
import AudioTools.Readers.WavFileException;
//Hashing
import Hashing.Fingerprinter;
import Hashing.NoHashing;
import Hashing.TwoPeaksFingerprinter;
import Matchers.BasicIntersectionMatcher;
import Matchers.FingerprintBasicMatcher;
//import Matchers.FingerprintBasicMatcher;
import Music.Song;
import ReporterTools.Reporter;
import ReporterTools.SimpleReporter;
import Repository.MusicRepository;
import Utilities.Codes;

import javax.sound.sampled.UnsupportedAudioFileException;

public class Main {

	/*
	 * radd
	 * /Users/ana/Teses/Ana/Original/1minuto/Boa_vibe2-Boss_AC-Ritmo_Amor_e_Palavras
	 * -2005.wav ridentify
	 * /Users/ana/Copy/SAMPLes/Boa_vibe_acompanha_original-Boss_AC-RAP-2005.wav
	 * radd
	 * /Users/ana/Copy/SAMPLes/Boa_vibe_acompanha_original-Boss_AC-RAP-2005.wav
	 */

	private static String originalsFolder = "/Users/ana/Teses/Ana/Original/1minuto/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav"; // "/Users/ana/Teses/Ana/Original/1minuto/Boa_vibe2-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav";
	private static String samplesFolder = "/Users/ana/Copy/SAMPLes/Ruido_injectado/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav";// "/Users/ana/Copy/SAMPLes/Boa_vibe_acompanha_original-Boss_AC-RAP-2005.wav";
	private static MusicRepository repository;
	private static int bit_depth;
    private ServerSocket serverSocket;		
    private static final int SERVERPORT = 8000;
    private static boolean isRunning = true;
    private static Reporter reporter;
    private static String music_result_match;
    
/*
 * Input : Folder de originais, folder de amostras [opcional]
 * 
 * 
 * */

	public static void main(String[] args)
			throws UnsupportedAudioFileException, IOException, WavFileException {
		
		//create all dirs if needed
		createDirectoryIfNeeded(Codes.REPORTS_FOLDER);
		createDirectoryIfNeeded(Codes.RESULTS_FOLDER);

		if(Codes.PROGRAM_MODE == Codes.DEBUG)
		{
			createDirectoryIfNeeded(Codes.FINGERPRINTS_FOLDER);
			createDirectoryIfNeeded(Codes.ORIGINAL_DATA_FOLDER);
			createDirectoryIfNeeded(Codes.SAMPLE_DATA_FOLDER);
			createDirectoryIfNeeded(Codes.PEAKS_FOLDER);			
		}
		
		PrintStream out = new PrintStream(new FileOutputStream(Codes.RESULTS_FOLDER + getResultsFileRelativePath()));
		//System.setOut(out);
		
		originalsFolder = args[0];

		if (args.length != 1) {
			samplesFolder = args[1];
			// verifica se é de 8 ou 16 bits
			bit_depth = Integer.parseInt(args[2]);

			if (bit_depth != 8 && bit_depth != 16)
			{
				System.out
						.println("Erro, bit depth não aceite (usar 8 ou 16 bit por sample)");
				return;
			}
		}
		// TODO:Trocar conforme gosto -> verificar se é para multiplicar log
		// define tipo de matching e hashing
		Fingerprinter fingerprinter =// new NoHashing( new BasicIntersectionMatcher());
		new TwoPeaksFingerprinter(new FingerprintBasicMatcher(), Codes.COMBINER_ALL_COMBINATIONS);
		// TODO:Trocar conforme gosto -> verificar se é para multiplicar log
		PeakFinder peak_finder = new RangesPeakFinderWithFilter();//new RangesPeakFinder();
		// new AmplitudeMinPeakFinder();
		//new SimplePeakFinder();

		repository = new MusicRepository(fingerprinter, bit_depth, peak_finder);
		// constroi Base de dados com base numa directoria.
		constructDataBaseFromFolder(originalsFolder);
		//-1 .DS
		reporter = new SimpleReporter(repository.getNumberOfSongsInRepository(),new File(samplesFolder).list().length -1, Codes.RAM,repository.getSize());
		reporter.writeHeader();
		reporter.writeTestHeader(fingerprinter.getType(), fingerprinter.getMatcher().getType(),peak_finder.getType(), null );

		
		
		System.out.println("Número de fingerprints actual do Repositório : "
				+ repository.getSize());
		System.out.println("Número de Músicas actual do Repositório : "
				+ repository.getNumberOfSongsInRepository());
		if (args.length != 1) {
			System.out
					.println("\n\n \t ************************ Início da fase de Identificação ************************ \n\n");
			identifyEverySample(samplesFolder);

		} 
		else 
		{
			//lança o servidor
			launchServer();

		}
		// Para quando se faz escolha aleatória
		/*
		 * String sample_path = getRandomSamplePath(samplesFolder);
		 * System.out.println
		 * ("A amostra escolhida aleatoriamente para reconhecimento é :" +
		 * sample_path); repository.identify(sample_path); System.out.println(
		 * "A amostra escolhida aleatoriamente para reconhecimento foi :" +
		 * sample_path);
		 */

		// Para fazer análise de todas as amostras
		System.out.println("Terminei");

	}


	private static void identifyEverySample(String samplesFolder)
			throws IOException 
	{

		Files.walk(Paths.get(samplesFolder))
				.forEach(
						filePath -> {
							if (Files.isRegularFile(filePath)
									&& filePath
											.toAbsolutePath()
											.toString()
											.equalsIgnoreCase(
													samplesFolder
															+ filePath
																	.getFileName())
									&& !filePath.getFileName().toString()
											.equalsIgnoreCase(".DS_Store")) 
							{
								Song sample = new Song(samplesFolder
										+ filePath.getFileName().toString());
								try {
									System.out
											.println("\n >>  A amostra escolhida para reconhecimento é :"
													+ sample.getPath() + "**");
									
									
									reporter.startTestMatching( sample.getPath());
									
									long tStart = System.currentTimeMillis();
									int result = repository.identify(sample.getPath(),
											filePath.getFileName().toString());
									
									String identificationResultMessage = "NO match found in database..."; 
									String dbSongName = "";
									if( result != Codes.NO_MATCH)
									{
										identificationResultMessage = repository.generateSuccessMessage();
										dbSongName = repository.getMusicNameFromMusicID(result);
									}	
									reporter.endTestMatching( dbSongName,sample.getName());
									
									long tEnd = System.currentTimeMillis();
									long tDelta = tEnd - tStart;
									double elapsedSeconds = tDelta / 1000.0;
									System.out
											.println("Identificação (acima) levou "
													+ elapsedSeconds
													+ " segundos\n\n");
									// System.out.println("A amostra escolhida aleatoriamente para reconhecimento foi :"
									// + original.getPath());
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						});
		//no fim do teste, escrevem-se as conclusões do report
		reporter.finishTest();
		reporter.finishReport();

	}

	private static String getRandomSamplePath(String samples_path) {

		File samples_folder = new File(samples_path);
		File[] samples = samples_folder.listFiles();
		int number_of_samples = samples.length;
		Random random_number_generator = new Random();
		int index = random_number_generator.nextInt(number_of_samples);
		// System.out.println("Sample>> Comparação entre " +
		// samples[index].getAbsolutePath() + " e " + samples_path +"/"
		// +".DS_Store" + "resultado = " +
		// samples[index].getAbsolutePath().equalsIgnoreCase( samples_path +"/"
		// +".DS_Store"));
		if (samples[index].getAbsolutePath().equalsIgnoreCase(
				samples_path + ".DS_Store")
				|| samples[index].isDirectory())
			return getRandomSamplePath(samples_path);

		return samples[index].getAbsolutePath();
	}

	private static void constructDataBaseFromFolder(String originals_path)
			throws IOException {

		Files.walk(Paths.get(originals_path)).forEach(
				filePath -> {

					if (Files.isRegularFile(filePath)
							&& !filePath.getFileName().toString()
									.equalsIgnoreCase(".DS_Store")) {
						Song original = new Song(originals_path  + filePath.getFileName() );
						try {
							System.out.println("Vou tentar adicionar música "+ originals_path + filePath.getFileName());

							//System.out.println("Adicionei música");
							repository.addSong(original);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				});

	}
	
	
	
	public static void launchServer() throws IOException
	{
        ServerSocket serverSocket = new ServerSocket(SERVERPORT);
	    Socket client_socket = null;
	    System.out.println ("À espera de conexão.......");
	    int iteration = 1;

		while(true)
		{

			
			client_socket = serverSocket.accept(); 
		    DataInputStream in = new DataInputStream(client_socket.getInputStream());

			System.out.println("Conexão #" + iteration + " feita!");
			FileWriter writer = new FileWriter("Android_identification" + iteration +".txt");

	             
			//para escrever para o cliente
			PrintWriter out = new PrintWriter(client_socket.getOutputStream(), 
                        true); 
			int frame_number = 0;
			//boolean is_running = true;
			
			while (true) 
			{
			    int length = in.readInt();
			    
			    if(length == 1)
			    	break;
			    
			    double[] frame = new double[length];
			    
			    for(int sample_pos = 0; sample_pos < length; sample_pos++)
			    {
			    	frame[ sample_pos ] = in.readDouble();
			    }
			    System.out.println("> Recebi frame");
				int result = repository.get16bitPeaksPerFrame(frame,  writer,frame_number,Codes.NO_MATCH,true);
			    System.out.println("> Analisei  frame");

				if(result != Codes.NO_MATCH)
				{
					out.print(repository.generateSuccessMessage());
					break;
				}
				
					
				//responde ao cliente com a resposta
				out.print(Codes.NO_MATCH);


	  
				frame_number++;
	     }
			writer.flush();
			writer.close();
			System.out.println("Conexão #" + iteration + " terminada!");

			iteration++;
	        
		}

    }
	
	
	private static void createDirectoryIfNeeded(String directoryName)
	{
		
		File theDirToTest = new File(directoryName);
		if (!theDirToTest.exists()){
			theDirToTest.mkdir();
			
		}

	}
	
	public static String getResultsFileRelativePath() 
	{

		Calendar cal = Calendar.getInstance();
		int dayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) + 1;
		int year = cal.get(Calendar.YEAR);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minuts = cal.get(Calendar.MINUTE);
		
		return "Results-" + String.valueOf(dayOfMonth) + "-" + 		
		String.valueOf(month) +  "-" + String.valueOf(year) + "-" +
		String.valueOf(hour) +"h" + String.valueOf(minuts) 
		+ "m.txt";
		
	}

	

}
