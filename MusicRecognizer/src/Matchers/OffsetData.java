package Matchers;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class OffsetData implements Offset{
	
	
	private int _offset;
	private int _songID;
	
	public OffsetData(int offset , int songID){
		
		_offset = offset;
		_songID = songID;
		
	}
	

	@Override
	public int getSongID() {
		// TODO Auto-generated method stub
		return _songID;
	}

	@Override
	public int getOffset() {
		// TODO Auto-generated method stub
		return _offset;
	}

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
            append(_offset).
            append(_songID).
            toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
       if (!(obj instanceof OffsetData))
            return false;
        if (obj == this)
            return true;

        OffsetData rhs = (OffsetData) obj;
        return new EqualsBuilder().
            // if deriving: appendSuper(super.equals(obj)).
            append(_offset, rhs._offset).
            append(_songID, rhs._songID).
            isEquals();
    }}
