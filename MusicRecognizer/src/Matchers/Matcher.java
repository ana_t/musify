package Matchers;

import java.util.HashMap;
import java.util.Map;

import Entities.FingerprintMetaData;

public interface Matcher {
	
	int getMatch(Iterable<Integer> sample_fingerprints,
			Map<Integer, HashMap<Integer, FingerprintMetaData>> fingerprint_repository, int frame);
	void printToScreenLastHits();
	void printToFileLastHits();
	void clearResults();
	int getFrameHitted();
	int getWinnerOffset();
	//int getHitsOfWinnerOffset();
	int getFirstFrameOfOriginalCorrespondentToTheSample();
	String getType();
	int getNumberOfCombinationsPossiblePerGroupOfFramesAnalysis();
	void setNumberOfCombinationsPossiblePerGroupOfFramesAnalysis(int combinations);
	void setNumberOfFramesInvolved(int frames);
}
