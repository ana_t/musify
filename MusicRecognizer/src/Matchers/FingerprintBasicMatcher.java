package Matchers;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import Entities.FingerprintMetaData;
import Utilities.Codes;

public class FingerprintBasicMatcher implements Matcher
{

	private Map<Offset, Integer> _offset_hits_per_song;
	private int _hitted_frame;
	private int _winner_offset;
	private int _first_original_frame;
	private int _combinations;
	private int _min_number_of_combinations_to_match;
	private int _second_degree_min_number_of_combinations_to_match;
	private int _framesInvolved;



	public FingerprintBasicMatcher()
	{
		_offset_hits_per_song = new HashMap<Offset, Integer>();
		//hitted_frame = Codes.NO_MATCH;
	}

	@Override
	public void clearResults()
	{
		_offset_hits_per_song = new HashMap<Offset, Integer>();
	}

	@Override
	public int getMatch(
			Iterable<Integer> sample_fingerprints,
			Map<Integer, HashMap<Integer, FingerprintMetaData>> fingerprint_repository, int frame) 
	{
		_hitted_frame = Codes.NO_MATCH;
		_winner_offset = Codes.NO_MATCH;
		boolean min_peaks_combination_was_found = false;
		boolean second_degree_peaks_combination_was_found = false;

		//guarda os dados de offsets - quantas vezes calhou no mesmo offset
		Map<Offset, Integer> frame_offset_hits = new HashMap<Offset, Integer>();
		Iterator<Integer> fingerprint_iterator = sample_fingerprints.iterator();
		Map<Offset, Boolean> offset_has_min_number_of_combos = new HashMap<Offset, Boolean>();
		Map<Offset, Boolean> offset_has_second_degree_of_combos = new HashMap<Offset, Boolean>();

		//paRa cada fingerprint vai procurá-lo na bds e preencher os dados do offset
		while(fingerprint_iterator.hasNext())			
		{
			int fingerprint = fingerprint_iterator.next();
			//se a fingerprint foi encontrada
			if(fingerprint_repository.containsKey(fingerprint))
			{
				// para cada música, tem que se actualizar o frameHits
				HashMap<Integer, FingerprintMetaData> hitted_songs_for_current_fingerprint = fingerprint_repository.get(fingerprint);

				Set<Entry<Integer, FingerprintMetaData>> fingerprintMetaData_per_hitted_song_for_fp = hitted_songs_for_current_fingerprint.entrySet();

				Iterator<Entry<Integer, FingerprintMetaData>> hittedSongAndFpMetaData_iterator = fingerprintMetaData_per_hitted_song_for_fp.iterator();				
				
				//navega pelas músicas que têm aquele fingerprint e depois procura as frames onde isso acontece
				while(hittedSongAndFpMetaData_iterator.hasNext())
				{
					Entry<Integer, FingerprintMetaData> entry = hittedSongAndFpMetaData_iterator.next();
					int songID = entry.getKey();
					FingerprintMetaData fpMetaData = entry.getValue();
					TreeSet<Integer> frames = fpMetaData.getFrames();
					//para cada hitted frame, procura actualiza o contador de ocorrências
					//de offsets

					Iterator<Integer> hitted_frame_iterator = frames.iterator();

					while (hitted_frame_iterator.hasNext())
					{
						int original_frame = hitted_frame_iterator.next();
						int offset = original_frame-frame;

						//procura no mapa de hits
						//se existe

						Offset offsetSample = new OffsetData(offset,songID);
						//procura se offset e songID já existem no mapa
						if(_offset_hits_per_song.containsKey(offsetSample))
						{
							int hits = _offset_hits_per_song.get(offsetSample);

							//TODO: Meter controlo de frame original!
							if( frame_offset_hits.get(offsetSample) != null)
							{
								//#fingerprints que correspondem à mesma frame (mesma frame da mesma música)
								int frequency_on_frame = frame_offset_hits.get(offsetSample);
								//para a mesma música e offset
								frequency_on_frame++;
								frame_offset_hits.put(offsetSample, frequency_on_frame);
								
								if(offset_has_second_degree_of_combos.get(offsetSample) == null)
									second_degree_peaks_combination_was_found = false;
								else
									second_degree_peaks_combination_was_found = true;
								
								if(offset_has_min_number_of_combos.get(offsetSample) == null)
									min_peaks_combination_was_found = false;
								else
									min_peaks_combination_was_found = true;
								
								if(!min_peaks_combination_was_found && frequency_on_frame > _min_number_of_combinations_to_match)
								{//adiciona caso haja 3 pontos por frame para a mesma musica e mesmo offset
									// se já existe, actualiza contador
									hits ++;
									offset_has_min_number_of_combos.put(offsetSample, true);
									_offset_hits_per_song.put(offsetSample,hits);
								}
								else if(min_peaks_combination_was_found && !second_degree_peaks_combination_was_found && frequency_on_frame > _min_number_of_combinations_to_match)
								{//adiciona caso haja 3 pontos por frame para a mesma musica e mesmo offset
									// se já existe, actualiza contador
									hits ++;
									offset_has_second_degree_of_combos.put(offsetSample, true);
									_offset_hits_per_song.put(offsetSample,hits);
								}
							} // actualiza o valor de hits
							else frame_offset_hits.put(offsetSample,1); // actualiza o valor de hits							
							
							//se o numero de ocorrencias do offset for de numero razoável, considera-se
							//que a musica foi identificada
							double ratio = (double)hits/ (double)frame;

							/*		Se encontrou match! 	*/
							if(hits >= Codes.MIN_HITS_TO_RESULT &&  frame > Codes.MIN_FRAME_NUMBER_TO_POSITIVE_RESULT)		
	// ratio>= Codes.MIN_HIT_RATIO_TO_RESULT &&								)
							{
								if(original_frame - frame < 0 && original_frame - frame < Codes.MIN_DIFERENCE_TO_ORIGINAL_FRAME_IF_NEGATIVE_START_FRAME)
									continue;
								_hitted_frame = frame;
								_winner_offset = offset;
								_first_original_frame = original_frame - frame;
								if( _first_original_frame < 0)
									_first_original_frame = 0;
								return songID;
							}
							//so mete de houver 2+ hits na mesma frame para o mesmo offset 


						}
						else //caso não exista ainda,adiciona
						{
							_offset_hits_per_song.put(offsetSample, 1);
							frame_offset_hits.put(offsetSample,1); // actualiza o valor de hits

						}
					}
				}	
			}
			// caso não contenha, segue para o próximo fingerprint, ou sai do método
		}
		//printToScreenLastHits();
		return Codes.NO_MATCH;
	}

	public void printToScreenLastHits(){

		Set<Entry<Offset, Integer>> map_entries = _offset_hits_per_song.entrySet();

		Iterator<Entry<Offset, Integer>> map_iterator = map_entries.iterator();

		System.out.println("Matches made: ");

		while(map_iterator.hasNext())
		{
			Entry<Offset, Integer> entry = map_iterator.next();

			System.out.println("\n Para a música songID= " + entry.getKey().getSongID() + " e offset="+
					entry.getKey().getOffset() + " Com hits = " + entry.getValue());

		}
		//_frame_hits_per_song = new HashMap<Integer, HashMap<Integer, Integer>>();



	}


	@Override
	public int getFrameHitted() {

		return _hitted_frame;
	}

	@Override
	public int getWinnerOffset() {

		return _winner_offset;
	}

	@Override
	public void printToFileLastHits() {
		// TODO Auto-generated method stub

	}

	@Override
	public int getFirstFrameOfOriginalCorrespondentToTheSample() {

		return _first_original_frame;
	}

	@Override
	public String getType() {
		return this.getClass().getSimpleName();
	}

	@Override
	public int getNumberOfCombinationsPossiblePerGroupOfFramesAnalysis() 
	{
		return _combinations;

	}

	@Override
	public void setNumberOfCombinationsPossiblePerGroupOfFramesAnalysis(
			int combinations) 
	{
		_combinations = combinations;		
	}

	@Override
	public void setNumberOfFramesInvolved(int frames) {
		_framesInvolved = frames;
		_min_number_of_combinations_to_match = (int) Math.pow(Codes.MIN_PEAK_MATCHES_PER_FRAME, _framesInvolved); // 2 frames devia ser dinâmico.
		_second_degree_min_number_of_combinations_to_match = (int) Math.pow(Codes.MIN_PEAK_MATCHES_PER_FRAME + 1, _framesInvolved); // 2 frames devia ser dinâmico.
	}

}
