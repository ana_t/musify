package Repository;

import java.io.FileWriter;
import java.io.IOException;

public interface Repository {
	
	
	int getSize();
	int get16bitPeaksPerFrame(double [] frame, FileWriter writer, int frame_number, int songID, boolean identify) throws IOException ;
	String generateSuccessMessage();
	String getMusicNameFromMusicID(int songID);
	

}
