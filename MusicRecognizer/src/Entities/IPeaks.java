package Entities;

public interface IPeaks {

	double getAmplitude();
	//int getFrequency();
	int getBin();
	int getFrequency();
}
