package Entities;

import Utilities.Codes;

public class Peak implements IPeaks {

	int _bin, _frequency;
	double _amplitude;
	
	public Peak(int frequency, double amplitude)
	{
		_frequency = frequency;
		_bin = frequency/Codes.BIN_VALUE;
		_amplitude = amplitude;
	}
	
	@Override
	public double getAmplitude() {

		return _amplitude;
	}

	@Override
	public int getBin() {

		return _bin;
	}

	@Override
	public int getFrequency() {

		return _frequency;
	}

}
