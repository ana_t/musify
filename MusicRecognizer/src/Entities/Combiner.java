package Entities;

public interface Combiner {

	Iterable<Integer> combineTwoFrameSets(Iterable<Integer> frame1, Iterable<Integer>frame2); 	
	
}
