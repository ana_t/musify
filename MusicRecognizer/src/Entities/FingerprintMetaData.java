package Entities;

import java.util.TreeSet;

import Hashing.FingerprintData;

public class FingerprintMetaData implements FingerprintData{
	

	private int _songID;
	private String _songName;
	private TreeSet<Integer> _frames;
	
	
	public FingerprintMetaData(int songID, String song_name) {

		_songID = songID;
		_frames = new TreeSet<Integer>();
		_songName = song_name;
		
	}
	
	
	@Override
	public int getSongID() {

		return _songID;
	}

	@Override
	public TreeSet<Integer> getFrames() {

		return _frames;
	}

	@Override
	public void insertFrame(int frame) {
		
		_frames.add(frame);		
	}


	@Override
	public String getSongName() {
		return _songName;
	}
	
	
	
	
	

}
