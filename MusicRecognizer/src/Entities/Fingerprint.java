package Entities;

public class Fingerprint implements IFingerprint {

	int _peaksInvolved, _fingerprint;
	
	public Fingerprint(int fingerprint, int number_of_peaks)
	{
		_peaksInvolved = number_of_peaks;
		_fingerprint = fingerprint;
	}
	
	@Override
	public int getFingerprint() {

		return _fingerprint;
	}

	@Override
	public int getNumberOfPeaksInFingerprint() {

		return _peaksInvolved;
	}
	

	
}
