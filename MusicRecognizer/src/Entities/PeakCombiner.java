package Entities;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import Hashing.Fingerprinter;
import Utilities.Codes;

public class PeakCombiner
{



	public static Iterable<Integer> combineTwoFrameSets(Iterable<Integer> frame1, Iterable<Integer>frame2, int mode, Fingerprinter fingerprinter, FileWriter debug_writer,int  first_frame) 
	{
		assert 	(mode == Codes.COMBINER_ALL_COMBINATIONS ||mode == Codes.COMBINER_COMBINATIONS_BETWEEN_THE_THREE_BEST_FROM_EACH_FRAME || mode == Codes.COMBINER_COMBINATIONS_BETWEEN_THE_TWO_BEST_FROM_EACH_FRAME)
 : "PeakCombiner combineTwoFrameSets >> not a viable mode set(" + mode + ")";

		assert 	(fingerprinter !=null)
 : "PeakCombiner combineTwoFrameSets >> Fingerprinter can't be null";

		if(mode == Codes.COMBINER_ALL_COMBINATIONS)
			return combineMode1TwoFrameSets(frame1,frame2,fingerprinter, debug_writer, first_frame);
		
		if(mode == Codes.COMBINER_COMBINATIONS_BETWEEN_THE_TWO_BEST_FROM_EACH_FRAME)
			return combineModeWithNBestTwoFrameSets((ArrayList<Integer>)frame1,(ArrayList<Integer>)frame2,2);

		//if(_mode == Codes.COMBINER_COMBINATIONS_BETWEEN_THE_THREE_BEST_FROM_EACH_FRAME)
		else	
			return combineModeWithNBestTwoFrameSets((ArrayList<Integer>)frame1,(ArrayList<Integer>)frame2,3);
	}
	
	/*
	 * All vs All 
	 */
	private static Iterable<Integer>combineMode1TwoFrameSets(Iterable<Integer> frame1, Iterable<Integer>frame2, Fingerprinter fingerprinter, FileWriter debug_writer, int first_frame)
	{
		ArrayList<Integer>fingerprints = new ArrayList<Integer>();
		//gives an index for debug purposes
		int peakOfFrame1 = 1;
		int peakOfFrame2 = Codes.FREQUENCY_INTERVALS.length; //-1+1

		if(debug_writer != null)
			printPeaksOnDebugFile( frame1, frame2, debug_writer, first_frame);
		//for each peak in frame 1
		for(Integer peak1 : frame1)
		{
			
			for(Integer peak2 : frame2)
			{
				if(debug_writer != null)
				{
					try 
					{
						//podia imprimir-se o fp aqui...
						debug_writer.write("\n Peak" + peakOfFrame1 + "xPeak" + peakOfFrame2);
					} 	catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				int result_fingerprint = fingerprinter.makeFingerprint(peak1, peak2);
				fingerprints.add(result_fingerprint);
				peakOfFrame2++;


			}
			
		peakOfFrame1++;
		peakOfFrame2 = Codes.FREQUENCY_INTERVALS.length;

		}
		return fingerprints;
	}
	/*
	 *The two best of first frame vs 2 best of the second frame
	 */
	private static Iterable<Integer>combineModeWithNBestTwoFrameSets(ArrayList<Integer> frame1, ArrayList<Integer>frame2, int nBest)
	{
		assert 	(nBest >frame1.size() && nBest >frame2.size()): "PeakCombiner >>combineModeWithNBestTwoFrameSets >> nBest is bigger than number of peaks in each frame.";
		//to get the n best in each frame, then 
		Collections.sort(frame1); 
		Collections.sort(frame2); 

		//arrayList.get(arrayList.size() - 1); //gets the last item, largest for an ascending sort

		return null;
	}
	
	private static void printPeaksOnDebugFile(Iterable<Integer> frame1, Iterable<Integer>frame2, FileWriter debug_writer, int first_frame )
	{
		int peakIndex = 1;
		int secondFrame = first_frame + 1;
		try 
		{
			debug_writer.write("\n#Peak\t| Peak Value |\t#Frame" );
		
			for(Integer peak1 : frame1)
			{
				debug_writer.write("\n"+ peakIndex + "\t| " + peak1 + "\t\t| " + first_frame  );
				peakIndex++;
			}
			debug_writer.write("\n_______________________________________________");

			for(Integer peak2 : frame2)
			{
				debug_writer.write("\n"+ peakIndex + "\t| " + peak2 + "\t\t| "+ secondFrame  );
				peakIndex++;
			}
			debug_writer.write("\n");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}






}
