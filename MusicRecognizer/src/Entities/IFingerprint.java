package Entities;

public interface IFingerprint {
	
	int getFingerprint();
	int getNumberOfPeaksInFingerprint();
	

}
