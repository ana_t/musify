	 ***	Report	***



	* Program Parameters *
	* Frame Size: 4096
	* Sample Rate: 44100
	* Ranges: [40,80,120,180,300,400,]
	* Min hits to show result: 1000
	* Min Hits/frames ratio: 1.0

	* Min Peaks Per Frame for Match: 2



	 * Number of songs: 15
	 * Number of samples: 23
	 * Number of fingerprints: 46
	 * Type of storage: Main Memory
	 * Minimum number of peaks for identification: 1000

>> Test #1
> Fingerprinter = NoHashing
> Matcher = BasicIntersectionMatcher



> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/adele_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005-2.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/boss2_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/boss_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Chandelier(Piano_Version)-Sia-unknown-2015-2.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Chandelier(Piano_Version)-Sia-unknown-2015.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Delilah-Florence_And_The_Machine-How_Big_Blue_And_Beautiful-2015-2.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Delilah-Florence_And_The_Machine-How_Big_Blue_And_Beautiful-2015.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/ed-thinking2_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/ed-thinking_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/ed-you2_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/ed-you_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Elastic_Heart-Sia-unknown-2015-2.wav
 >>>Identificação feita!
>Música Reconhecida : Boa_vibe-Boss_AC-Ritmo_Amor_e_Palavras-2005.wav na frame da amostra: 290 com o offset 0
>A amostra começa aproximadamente na frame 0 da música original. (0 segundos) 
>Identificação feita com 26.93514739229025 segundos da amostra!

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Elastic_Heart-Sia-unknown-2015.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/hideaway_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/opera_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/royals_tlm.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Stay_with_Me-Sam_Smith-Unknown-2015 .wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Stay_with_Me-Sam_Smith-Unknown-2015-2.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Take_Me_To_Church-Hozier-unknown-2015-2.wav
 >>NO match found in database...

> Matching attempt with sample: /Users/Ana/Copy/SAMPLes/16bits/Jan-16/Take_Me_To_Church-Hozier-unknown-2015.wav
 >>NO match found in database...
> Test Results:
	 * Total Time of test = 32
	 * Average Time of matching = 1
	 * Number of correct answers = 1
	 * Number of INcorrect answers = 21

